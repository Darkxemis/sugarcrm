{
	"properties": [

	{

		"title":"Retorno de Inversión de la Campaña"
,
		"subtitle":"Cantidad en €"
,
		"type":"bar chart"
,
		"legend":"on"
,
		"labels":"value"
,
		"thousands":""

	}

	],

	"label": [

		"Ingresos"
,
		"Inversión"
,
		"Presupuesto"
,
		"Ingresos Esperados"

	],

	"color": [

		"#8c2b2b"
,
		"#468c2b"
,
		"#2b5d8c"
,
		"#cd5200"
,
		"#e6bf00"
,
		"#7f3acd"
,
		"#00a9b8"
,
		"#572323"
,
		"#004d00"
,
		"#000087"
,
		"#e48d30"
,
		"#9fba09"
,
		"#560066"
,
		"#009f92"
,
		"#b36262"
,
		"#38795c"
,
		"#3D3D99"
,
		"#99623d"
,
		"#998a3d"
,
		"#994e78"
,
		"#3d6899"
,
		"#CC0000"
,
		"#00CC00"
,
		"#0000CC"
,
		"#cc5200"
,
		"#ccaa00"
,
		"#6600cc"
,
		"#005fcc"

	],

	"values": [

	{

		"label": [

			"Ingresos"

		],

		"values": [

			0

		],

		"valuelabels": [

			"€0.00"

		],

		"links": [

			""

		]

	}
,
	{

		"label": [

			"Inversión"

		],

		"values": [

			700

		],

		"valuelabels": [

			"€700.00"

		],

		"links": [

			""

		]

	}
,
	{

		"label": [

			"Presupuesto"

		],

		"values": [

			500

		],

		"valuelabels": [

			"€500.00"

		],

		"links": [

			""

		]

	}
,
	{

		"label": [

			"Ingresos Esperados"

		],

		"values": [

			3000

		],

		"valuelabels": [

			"€3,000.00"

		],

		"links": [

			""

		]

	}

	]

}