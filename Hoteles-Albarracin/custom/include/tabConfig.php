<?php
// created: 2016-02-26 17:59:30
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Opportunities',
      3 => 'Leads',
      4 => 'Contacts',
    ),
  ),
  'LBL_TABGROUP_MARKETING' => 
  array (
    'label' => 'LBL_TABGROUP_MARKETING',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Contacts',
      2 => 'Leads',
      3 => 'Campaigns',
      4 => 'Prospects',
      5 => 'ProspectLists',
      6 => 'Cases',
      7 => 'Emails',
    ),
  ),
  'LBL_TABGROUP_SUPPORT' => 
  array (
    'label' => 'LBL_TABGROUP_SUPPORT',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Contacts',
      2 => 'Bugs',
    ),
  ),
  'LBL_TABGROUP_ACTIVITIES' => 
  array (
    'label' => 'LBL_TABGROUP_ACTIVITIES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Calendar',
      2 => 'Calls',
      3 => 'Meetings',
      4 => 'Tasks',
      5 => 'Notes',
    ),
  ),
);