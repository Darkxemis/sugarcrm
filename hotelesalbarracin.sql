-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2016 a las 15:28:03
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hotelesalbarracin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts`
--

CREATE TABLE `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `annual_revenue` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `phone_office` varchar(100) DEFAULT NULL,
  `phone_alternate` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ownership` varchar(100) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sic_code` varchar(10) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `account_type`, `industry`, `annual_revenue`, `phone_fax`, `billing_address_street`, `billing_address_city`, `billing_address_state`, `billing_address_postalcode`, `billing_address_country`, `rating`, `phone_office`, `phone_alternate`, `website`, `ownership`, `employees`, `ticker_symbol`, `shipping_address_street`, `shipping_address_city`, `shipping_address_state`, `shipping_address_postalcode`, `shipping_address_country`, `parent_id`, `sic_code`, `campaign_id`) VALUES
('7fe7189d-6ab1-6354-73c7-56cf64525f61', 'Jose Miguel Jaimez Díaz', '2016-02-25 20:30:57', '2016-02-26 13:39:17', '1', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Customer', ' ', '15000', NULL, 'C/ galicia nº13', NULL, 'Granada', '18230', 'España', NULL, NULL, NULL, 'http://', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
('8241cc33-5104-18df-a97b-56cf594cb6a6', 'Mi empresa', '2016-02-25 19:42:11', '2016-02-26 13:39:50', '1', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, '816c0648-ca93-1d95-11f9-56cf45208e84', 'Investor', 'Other', '30.000.00€', NULL, 'AV constitución Nº46', 'Granada', 'Granada', '18020', 'España', NULL, '958473108', NULL, 'http://', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
('a9f6a964-b67a-e5cd-809b-56d04ab03144', 'jose_marketing', '2016-02-26 12:50:28', '2016-02-26 13:38:54', '1', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', NULL, 0, '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Competitor', ' ', '20.000.00€', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'df781430-6d7e-2794-5764-56d0380c6338');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_audit`
--

CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accounts_audit`
--

INSERT INTO `accounts_audit` (`id`, `parent_id`, `date_created`, `created_by`, `field_name`, `data_type`, `before_value_string`, `after_value_string`, `before_value_text`, `after_value_text`) VALUES
('bf491f73-01d9-90ee-347e-56cf5c0ad39c', '8241cc33-5104-18df-a97b-56cf594cb6a6', '2016-02-25 19:55:52', '816c0648-ca93-1d95-11f9-56cf45208e84', 'name', 'name', 'Albarracín Gallego Teresa', 'Mi empresa', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_bugs`
--

CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_cases`
--

CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_contacts`
--

CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accounts_contacts`
--

INSERT INTO `accounts_contacts` (`id`, `contact_id`, `account_id`, `date_modified`, `deleted`) VALUES
('59205344-cfc0-8691-a9f8-56cf5c800302', '572c47d0-be2b-c421-e235-56cf5c83e9f9', '8241cc33-5104-18df-a97b-56cf594cb6a6', '2016-02-25 19:58:22', 0),
('802598f5-5093-4b48-d46b-56cf6463de9b', '7fa89531-289c-bb16-72d6-56cf64ceb37a', '7fe7189d-6ab1-6354-73c7-56cf64525f61', '2016-02-25 20:30:57', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_cstm`
--

CREATE TABLE `accounts_cstm` (
  `id_c` char(36) NOT NULL,
  `nif_c` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accounts_cstm`
--

INSERT INTO `accounts_cstm` (`id_c`, `nif_c`) VALUES
('7fe7189d-6ab1-6354-73c7-56cf64525f61', '88888888X'),
('8241cc33-5104-18df-a97b-56cf594cb6a6', '77777777P'),
('a9f6a964-b67a-e5cd-809b-56d04ab03144', '9999999G');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_opportunities`
--

CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accounts_opportunities`
--

INSERT INTO `accounts_opportunities` (`id`, `opportunity_id`, `account_id`, `date_modified`, `deleted`) VALUES
('14278b44-2710-748e-6901-56d04bbc7c7b', '10fafcd7-13da-d3cc-e185-56d04b8f4d19', 'a9f6a964-b67a-e5cd-809b-56d04ab03144', '2016-02-26 12:56:54', 0),
('ce00b9c3-625a-bf1c-38fb-56cf5d9a0795', 'cc0cbc57-0f93-b561-06c0-56cf5d092089', '8241cc33-5104-18df-a97b-56cf594cb6a6', '2016-02-25 20:02:39', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_actions`
--

CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acl_actions`
--

INSERT INTO `acl_actions` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `category`, `acltype`, `aclaccess`, `deleted`) VALUES
('14d7d43b-77b6-8a09-9a24-56cdec1003fc', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Campaigns', 'module', 89, 0),
('14d7d88a-fa01-441c-9662-56cdeca4425c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Campaigns', 'module', 90, 0),
('15741114-c0ba-f279-f588-56cdec6c559e', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Campaigns', 'module', 90, 0),
('1574161f-a535-97bf-1027-56cdec1f47df', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Campaigns', 'module', 90, 0),
('16105230-442a-bc29-a26c-56cdec269d7b', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Campaigns', 'module', 90, 0),
('16105372-05d8-6637-957f-56cdec995194', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Campaigns', 'module', 90, 0),
('161055c3-53ad-2977-e398-56cdeca68276', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Campaigns', 'module', 90, 0),
('16ac9f31-61dd-9c39-297c-56cdec83d86a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Campaigns', 'module', 90, 0),
('1f3809f6-3f4b-976c-9082-56cdec06ec59', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'ProspectLists', 'module', 90, 0),
('1f380b24-e398-3dfe-6a32-56cdec6af30f', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'ProspectLists', 'module', 89, 0),
('1f380c32-6f35-9c96-7981-56cdec3ab498', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'ProspectLists', 'module', 90, 0),
('1fd4471d-8cbb-5ccb-d584-56cdecd557fa', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'ProspectLists', 'module', 90, 0),
('1fd44c1f-5607-4bb9-997c-56cdecb26b5c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'ProspectLists', 'module', 90, 0),
('2070a70f-c9c9-ab77-c082-56cdece04ff7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'ProspectLists', 'module', 90, 0),
('2070a78f-b40c-1737-a5f2-56cdecc438a4', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'ProspectLists', 'module', 90, 0),
('2070afcc-be95-e14b-d080-56cdec232675', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'ProspectLists', 'module', 90, 0),
('27c3af65-f8b3-f474-db93-56cdec82403d', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Prospects', 'module', 89, 0),
('285fe893-829f-b7a4-a583-56cdec73f288', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Prospects', 'module', 90, 0),
('285fee4b-2ddc-7359-ba0b-56cdec8e730f', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Prospects', 'module', 90, 0),
('28fc23e1-d4d9-6fc3-ccc0-56cdec490bf9', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Prospects', 'module', 90, 0),
('28fc27f9-9adf-75d2-e441-56cdece77527', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Prospects', 'module', 90, 0),
('28fc2df4-b650-8fea-c43d-56cdec731309', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Prospects', 'module', 90, 0),
('29986b57-1968-52e4-ba83-56cdeca4a423', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Prospects', 'module', 90, 0),
('29986c60-e3d6-6515-9d8c-56cdec824907', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Prospects', 'module', 90, 0),
('304f30cf-87d1-c037-7d60-56cdec576eda', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'EmailMarketing', 'module', 89, 0),
('304f382d-f9f7-2448-b7ce-56cdece78a77', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'EmailMarketing', 'module', 90, 0),
('304f3ad2-a50f-737e-9249-56cdeccb5054', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'EmailMarketing', 'module', 90, 0),
('30eb783c-9d05-33df-b503-56cdeca5349a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'EmailMarketing', 'module', 90, 0),
('30eb7b8d-a6ca-567d-f4b6-56cdec01bc25', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'EmailMarketing', 'module', 90, 0),
('3187bbfc-1742-bd35-bbb1-56cdecdac7a0', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'EmailMarketing', 'module', 90, 0),
('3187bd0c-33b2-ed29-a61d-56cdeca594e8', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'EmailMarketing', 'module', 90, 0),
('3187bdfd-73e6-159e-a382-56cdecc9fd90', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'EmailMarketing', 'module', 90, 0),
('324802fb-647c-25e9-31fa-56cdec8595a0', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Project', 'module', 90, 0),
('32480a66-369c-b9f3-37c4-56cdec67a037', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Project', 'module', 89, 0),
('3c0c0cdf-edc7-db23-2bed-56cdeca05ddc', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Project', 'module', 90, 0),
('3c0c0e50-e656-4a31-431c-56cdecb5d3ce', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Project', 'module', 90, 0),
('45d00175-dabc-5afb-12aa-56cdeca2a2f9', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Project', 'module', 90, 0),
('45d00440-85b7-a568-9be1-56cdecaf1630', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Project', 'module', 90, 0),
('45d00526-254f-d1ed-d381-56cdece7c5ca', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Project', 'module', 90, 0),
('49f1ce69-e8f3-e233-b782-56cdec21e0b1', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Contacts', 'module', 89, 0),
('4a8e0639-626b-12ce-08fe-56cdec954e44', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Contacts', 'module', 90, 0),
('4a8e0dae-d9c0-2783-efb2-56cdec27840a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Contacts', 'module', 90, 0),
('4a8e0e6f-521b-6725-2f03-56cdec16693f', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Contacts', 'module', 90, 0),
('4b2a421a-be3d-707e-72de-56cdec430fd7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Contacts', 'module', 90, 0),
('4b2a46b0-2eb6-74a5-093c-56cdec3750e0', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Contacts', 'module', 90, 0),
('4bc6834e-6298-e541-f38d-56cdec229512', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Contacts', 'module', 90, 0),
('4bc68a82-35cc-f64a-0885-56cdec753737', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Contacts', 'module', 90, 0),
('4f9401f8-ffe0-dea4-b1c8-56cdec1f9371', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Project', 'module', 90, 0),
('56c3176d-c92d-a626-64d0-56cdec1ba43c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Accounts', 'module', 89, 0),
('575f52be-23af-2bf5-0f40-56cdec3f84ba', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Accounts', 'module', 90, 0),
('575f5bbe-ff39-3f83-c3ee-56cdec0527d4', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Accounts', 'module', 90, 0),
('57fb9077-0dc4-09c2-54c1-56cdec2fcb4c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Accounts', 'module', 90, 0),
('57fb9ba5-7b0a-93fd-b4f4-56cdecee24bc', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Accounts', 'module', 90, 0),
('57fb9ccb-84f3-d718-856f-56cdec603da9', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Accounts', 'module', 90, 0),
('5897ddc0-2c9c-84b5-5107-56cdec1c9617', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Accounts', 'module', 90, 0),
('5897de46-e3f6-f0ab-3dcb-56cdec4c4391', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Accounts', 'module', 90, 0),
('62f8229b-c83f-c6a1-654a-56cdecd2033e', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Opportunities', 'module', 90, 0),
('62f8295c-c54e-8af3-d827-56cdec5d9852', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Opportunities', 'module', 90, 0),
('62f82b7b-4792-15a8-bf69-56cdec585597', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Opportunities', 'module', 89, 0),
('63946767-0fda-3b44-6e6e-56cdecc3696a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Opportunities', 'module', 90, 0),
('6394693d-9e98-501b-751e-56cdecee05cb', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Opportunities', 'module', 90, 0),
('6430a33a-a196-e742-efb4-56cdecf1eb53', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Opportunities', 'module', 90, 0),
('6430ab5e-cb6b-4a78-9de1-56cdecfebdb7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Opportunities', 'module', 90, 0),
('6430ac46-3c8f-b466-ea73-56cdecd0bcf7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Opportunities', 'module', 90, 0),
('6cbc22f5-7a2f-4e90-3165-56cdec1896b7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'EmailTemplates', 'module', 89, 0),
('6cbc2b1f-00f3-fe68-f7db-56cdec812648', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'EmailTemplates', 'module', 90, 0),
('6d5860ec-f1f6-c318-bd2e-56cdecf7f1eb', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'EmailTemplates', 'module', 90, 0),
('6d58677b-5905-bdb7-8895-56cdec11c6b1', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'EmailTemplates', 'module', 90, 0),
('6d586c6f-e01b-aba4-5070-56cdec3432ed', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'EmailTemplates', 'module', 90, 0),
('6df4a3a3-2995-3d7b-ccee-56cdecf9ca3b', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'EmailTemplates', 'module', 90, 0),
('6df4a567-ad97-5790-63d3-56cdecb17770', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'EmailTemplates', 'module', 90, 0),
('6df4ac58-4604-b27a-029c-56cdec973aa5', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'EmailTemplates', 'module', 90, 0),
('7372f9c8-8489-9a4b-8955-56cdec03f542', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Notes', 'module', 89, 0),
('740f39cb-a788-caa1-70ce-56cdec0a0ecd', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Notes', 'module', 90, 0),
('740f3c61-8fe5-c695-ffe1-56cdec5ba0a1', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Notes', 'module', 90, 0),
('74ab7213-b309-c53d-48e1-56cdec683548', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Notes', 'module', 90, 0),
('74ab75d9-4028-9914-de0c-56cdec48f0d6', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Notes', 'module', 90, 0),
('74ab7acb-68ac-e950-4a8e-56cdecd118a6', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Notes', 'module', 90, 0),
('7547bbf9-bfe0-3c3c-05ae-56cdec37b9ae', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Notes', 'module', 90, 0),
('7547be18-77e1-e56c-05a9-56cdec9bef04', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Notes', 'module', 90, 0),
('7b623381-cad8-8d7a-818b-56cdec046596', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Calls', 'module', 89, 0),
('7bfe63ab-0e84-0127-b9f4-56cdec9295a2', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Calls', 'module', 90, 0),
('7bfe6725-ea46-5799-b738-56cdec39e037', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Calls', 'module', 90, 0),
('7bfe6b78-374d-6105-6777-56cdecded90d', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Calls', 'module', 90, 0),
('7c9ababb-1318-a321-18b5-56cdec7d2e9c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Calls', 'module', 90, 0),
('7c9abce8-46cd-915e-166c-56cdecca8263', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Calls', 'module', 90, 0),
('7d36f412-d040-8c97-8b71-56cdecd0d604', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Calls', 'module', 90, 0),
('7d36f6ea-fe4f-ce5b-a5c7-56cdec262879', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Calls', 'module', 90, 0),
('86fb06a2-9e3e-8847-7bde-56cdecdd001b', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Emails', 'module', 89, 0),
('86fb0d3d-47d0-a74b-d5ac-56cdec657b74', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Emails', 'module', 90, 0),
('8797447a-0e90-4df7-f8bf-56cdec706357', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Emails', 'module', 90, 0),
('87974d95-9641-0845-1b34-56cdecc0c155', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Emails', 'module', 90, 0),
('88338565-c80c-c96f-0d2a-56cdec96bcee', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Emails', 'module', 90, 0),
('88338e1e-725f-1999-7426-56cdece13725', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Emails', 'module', 90, 0),
('88cfc781-5782-a5dd-6f44-56cdec7729d2', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Emails', 'module', 90, 0),
('88cfcd98-2984-fe4b-4bd3-56cdec5b83d0', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Emails', 'module', 90, 0),
('90bf169b-1ea3-b5cf-b3c2-56cdec5257ff', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Meetings', 'module', 90, 0),
('90bf17fb-6828-4428-d5e5-56cdecf34e6a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Meetings', 'module', 89, 0),
('915b52ef-3944-7936-0b0e-56cdec8b743a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Meetings', 'module', 90, 0),
('915b565f-cf3e-1886-8215-56cdec12693c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Meetings', 'module', 90, 0),
('91f796f2-156f-8bfb-9113-56cdeca68acc', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Meetings', 'module', 90, 0),
('91f79a4a-2720-1170-d8e7-56cdec6f9022', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Meetings', 'module', 90, 0),
('9293d41e-f105-cead-f873-56cdec8df346', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Meetings', 'module', 90, 0),
('9293de9f-2f65-a7c1-dac5-56cdec9a96a5', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Meetings', 'module', 90, 0),
('994a959e-c38d-05df-e9d0-56cdec8259db', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Tasks', 'module', 89, 0),
('99e6d4e5-cf83-331b-1b19-56cdec3e3eeb', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Tasks', 'module', 90, 0),
('99e6da36-ca46-28f6-f1e4-56cdecd6f1dd', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Tasks', 'module', 90, 0),
('9a831103-6445-c601-8931-56cdec4da2e7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Tasks', 'module', 90, 0),
('9a8313f7-a165-62a3-f6a5-56cdec303cd7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Tasks', 'module', 90, 0),
('9a8316ce-b8e2-4c44-59d3-56cdeccfe6b9', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Tasks', 'module', 90, 0),
('9b1f50c5-2826-e023-1491-56cdec27b9f5', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Tasks', 'module', 90, 0),
('9b1f562d-5de9-2609-254e-56cdec5443d1', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Tasks', 'module', 90, 0),
('a77806cd-4d6b-bad0-b455-56cdec8cbb90', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'ProjectTask', 'module', 89, 0),
('b13c01f3-727f-a8ac-3975-56cdec6b8327', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'ProjectTask', 'module', 90, 0),
('b13c026c-8f9f-1d3d-f50d-56cdecef0f6e', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'ProjectTask', 'module', 90, 0),
('b13c091e-2d88-078a-5b6a-56cdec4d8d4c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'ProjectTask', 'module', 90, 0),
('bb000781-30ec-0bca-d199-56cdec0ff3cc', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'ProjectTask', 'module', 90, 0),
('bb000bd2-c5ff-2522-802d-56cdec74f84c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'ProjectTask', 'module', 90, 0),
('bb000eb1-49b9-0800-baf8-56cdec19c21f', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'ProjectTask', 'module', 90, 0),
('bcb1326c-8c49-f763-4b49-56cdec336058', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Documents', 'module', 90, 0),
('bcb133f6-2022-85f7-2045-56cdece12226', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Documents', 'module', 89, 0),
('bd4d771c-7a3c-7478-2a64-56cdec83b099', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Documents', 'module', 90, 0),
('bd4d7fc1-5d4e-2f1b-d53c-56cdece3986c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Documents', 'module', 90, 0),
('bde9b83c-4cf2-944f-e5fd-56cdecfac197', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Documents', 'module', 90, 0),
('bde9b8a6-c214-96ba-40a1-56cdec3419ef', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Documents', 'module', 90, 0),
('be85f05a-05c0-2a69-3c75-56cdecba2645', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Documents', 'module', 90, 0),
('be85f260-ec1c-19b4-b81b-56cdeccd7955', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Documents', 'module', 90, 0),
('c4c40319-10b7-08fb-9deb-56cdec41d432', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'ProjectTask', 'module', 90, 0),
('c8e55289-8bc4-4192-6441-56cdec1ffb83', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Leads', 'module', 90, 0),
('c8e55aba-4b03-83da-b59b-56cdec0a88f1', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Leads', 'module', 89, 0),
('c9819733-4303-cb23-3a72-56cdec805c84', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Leads', 'module', 90, 0),
('c981995d-8db2-0f62-0f99-56cdeccabcd0', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Leads', 'module', 90, 0),
('ca1dd328-840e-1e2d-f0f7-56cdec69ae4f', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Leads', 'module', 90, 0),
('ca1dd3c9-9c4b-16d1-767f-56cdec8731fb', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Leads', 'module', 90, 0),
('caba132b-2823-c890-27d2-56cdecd4f7d9', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Leads', 'module', 90, 0),
('caba15d7-bf6f-5958-d81d-56cdec956a8a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Leads', 'module', 90, 0),
('d5b6a7b4-4694-2e88-c790-56cdec370884', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Cases', 'module', 89, 0),
('d652e639-4b43-72a3-6628-56cdecf18c0c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Cases', 'module', 90, 0),
('d652ee7e-fc21-18e6-19ab-56cdec5f692b', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Cases', 'module', 90, 0),
('d6ef256b-9418-e055-298a-56cdec9c3813', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Cases', 'module', 90, 0),
('d6ef2dec-b837-67fa-4eeb-56cdecf90424', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Cases', 'module', 90, 0),
('d78b69ca-5084-d4a2-d59d-56cdec23ebbf', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Cases', 'module', 90, 0),
('d78b6b83-d047-3d4d-cb9d-56cdecd9c661', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Cases', 'module', 90, 0),
('d827a740-6cd2-ffb0-c481-56cdecd4fa85', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Cases', 'module', 90, 0),
('e0b32578-0066-4644-fa90-56cdec75394c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Bugs', 'module', 89, 0),
('e14f62f8-917e-164a-7b57-56cdeccdee72', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Bugs', 'module', 90, 0),
('e14f6cb1-fe3a-4bab-730a-56cdec435f9c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Bugs', 'module', 90, 0),
('e1506e58-17e1-437b-25ac-56cdec0f9c2c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'EAPM', 'module', 90, 0),
('e1506ec4-5d5e-2550-998f-56cdec917e71', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'EAPM', 'module', 89, 0),
('e1eba0de-8968-1b9c-d799-56cdecd2d7ee', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Bugs', 'module', 90, 0),
('e1eba26c-36af-481d-087d-56cdec0e733a', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Bugs', 'module', 90, 0),
('e1eba702-72ea-a559-f5ad-56cdec52097d', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Bugs', 'module', 90, 0),
('e1eca01c-122c-21af-6d20-56cdec7c1ae4', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'EAPM', 'module', 90, 0),
('e1ecaafe-21b0-35b7-840f-56cdec8a90cc', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'EAPM', 'module', 90, 0),
('e287e328-68b8-4d89-86a5-56cdecd9a49f', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Bugs', 'module', 90, 0),
('e287e56d-45fd-70aa-9afe-56cdec72ea3c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Bugs', 'module', 90, 0),
('e288e39e-c594-4b2a-2053-56cdec35ad38', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'EAPM', 'module', 90, 0),
('e288eb94-e337-dfe0-14ec-56cdecfd037c', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'EAPM', 'module', 90, 0),
('e288eedf-37da-cc95-25a8-56cdec9b60a7', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'EAPM', 'module', 90, 0),
('e3252f6e-fa05-fcad-4b0a-56cdec2cb0ce', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'EAPM', 'module', 90, 0),
('eb137510-9122-dc7f-dc52-56cdec1b4447', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'access', 'Users', 'module', 89, 0),
('ebafb5de-91b6-f242-f540-56cdec47a534', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'view', 'Users', 'module', 90, 0),
('ebafbd26-fd57-7dee-630a-56cdec7b0d76', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'list', 'Users', 'module', 90, 0),
('ec4bf8c4-c626-7561-b86f-56cdeccbeba4', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'edit', 'Users', 'module', 90, 0),
('ec4bf949-217f-6d22-3e05-56cdec70a08d', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'delete', 'Users', 'module', 90, 0),
('ece8310c-0e54-c676-9312-56cdecf88635', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'export', 'Users', 'module', 90, 0),
('ece8333f-8fec-7910-7ad5-56cdec454cea', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'import', 'Users', 'module', 90, 0),
('ed84704c-f23a-7772-4e22-56cdece73bec', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '', 'massupdate', 'Users', 'module', 90, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_roles`
--

CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acl_roles`
--

INSERT INTO `acl_roles` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `description`, `deleted`) VALUES
('55effc16-809c-3ff4-f464-56cf528b4049', '2016-02-25 19:13:11', '2016-02-25 19:22:15', '1', '1', 'Recepción', 'Encargados principalmente de la recepción y de las incidencias principales de Hoteles Albarracín.', 0),
('85f6e014-4622-795d-bde1-56cf4a829665', '2016-02-25 18:38:14', '2016-02-25 19:01:22', '1', '1', 'Comerciales', 'Este es el área de comerciales, principalmente encargados del comercio y distribución de noches de hotel y ofertas de fin de semana.', 0),
('a235c595-b3e5-f72b-b2db-56cee0123ce6', '2016-02-25 11:08:27', '2016-02-26 10:48:11', '1', '1', 'Marketing', 'Encargados principalmente de las promociones y distribución publicitaria de Hoteles Albarracín.', 0),
('e900e1cc-5f05-d544-b881-56cf44eeee58', '2016-02-25 18:15:24', '2016-02-25 18:21:30', '1', '1', 'Jefatura', 'Solo permisos para altos cargos de la empresa.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_roles_actions`
--

CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acl_roles_actions`
--

INSERT INTO `acl_roles_actions` (`id`, `role_id`, `action_id`, `access_override`, `date_modified`, `deleted`) VALUES
('12dd0b4e-2550-59e6-9533-56cf4f9b0d07', '85f6e014-4622-795d-bde1-56cf4a829665', '304f30cf-87d1-c037-7d60-56cdec576eda', -98, '2016-02-25 19:03:12', 0),
('12dd0d37-5fef-e320-078b-56cf4f945f9a', '85f6e014-4622-795d-bde1-56cf4a829665', '30eb783c-9d05-33df-b503-56cdeca5349a', 0, '2016-02-25 19:03:12', 0),
('16c50126-3714-84e8-7672-56cf4f9be283', '85f6e014-4622-795d-bde1-56cf4a829665', '30eb7b8d-a6ca-567d-f4b6-56cdec01bc25', 0, '2016-02-25 19:03:12', 0),
('1aae0117-c00d-2547-ee3c-56cf4fdcec78', '85f6e014-4622-795d-bde1-56cf4a829665', '3187bbfc-1742-bd35-bbb1-56cdecdac7a0', 0, '2016-02-25 19:03:12', 0),
('1aae0f92-8189-7ea9-e0a7-56cf4fc153fe', '85f6e014-4622-795d-bde1-56cf4a829665', '3187bdfd-73e6-159e-a382-56cdecc9fd90', 0, '2016-02-25 19:03:12', 0),
('1e9605b7-6acb-ee64-ec13-56cf4f95a4b2', '85f6e014-4622-795d-bde1-56cf4a829665', '304f382d-f9f7-2448-b7ce-56cdece78a77', 0, '2016-02-25 19:03:12', 0),
('1e960d50-266a-e897-411d-56cf4fe29830', '85f6e014-4622-795d-bde1-56cf4a829665', '3187bd0c-33b2-ed29-a61d-56cdeca594e8', 0, '2016-02-25 19:03:12', 0),
('1f56e197-6b2b-6526-92cd-56cf44112c17', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '14d7d43b-77b6-8a09-9a24-56cdec1003fc', 89, '2016-02-25 18:21:35', 0),
('20126cda-91cc-19ef-c888-56cf443d20c6', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '16105230-442a-bc29-a26c-56cdec269d7b', 0, '2016-02-25 18:21:35', 0),
('2050e5d3-5bb8-a1ef-dea3-56cf440318e0', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '15741114-c0ba-f279-f588-56cdec6c559e', 0, '2016-02-25 18:21:35', 0),
('208f66b9-6b06-77ca-e603-56cf44b59d0e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '161055c3-53ad-2977-e398-56cdeca68276', 0, '2016-02-25 18:21:35', 0),
('20cdeeb2-f67f-938e-f40e-56cf44bce742', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '16105372-05d8-6637-957f-56cdec995194', 0, '2016-02-25 18:21:35', 0),
('210c6ba5-8491-5026-775d-56cf44bf1917', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '1574161f-a535-97bf-1027-56cdec1f47df', 0, '2016-02-25 18:21:35', 0),
('214ae3e9-3479-628b-e777-56cf44757a82', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '14d7d88a-fa01-441c-9662-56cdeca4425c', 0, '2016-02-25 18:21:35', 0),
('214ae803-6b0a-c2a9-9ad4-56cf445ff163', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '16ac9f31-61dd-9c39-297c-56cdec83d86a', 0, '2016-02-25 18:21:35', 0),
('218963f0-cc7a-bac3-aa11-56cf4498e41f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd5b6a7b4-4694-2e88-c790-56cdec370884', 89, '2016-02-25 18:21:35', 0),
('21c7f955-a4b6-a157-4f78-56cf443fab58', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd6ef2dec-b837-67fa-4eeb-56cdecf90424', 0, '2016-02-25 18:21:35', 0),
('2206748c-ded0-e0c3-6b1e-56cf44c9fc5b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd6ef256b-9418-e055-298a-56cdec9c3813', 0, '2016-02-25 18:21:35', 0),
('2244f9bf-05e6-a25c-2136-56cf44e58afd', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd78b69ca-5084-d4a2-d59d-56cdec23ebbf', 0, '2016-02-25 18:21:35', 0),
('22837cd0-409f-8c36-ba24-56cf44001e23', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd78b6b83-d047-3d4d-cb9d-56cdecd9c661', 0, '2016-02-25 18:21:35', 0),
('22c1fd8c-8d76-1084-4bc7-56cf4443a517', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd652ee7e-fc21-18e6-19ab-56cdec5f692b', 0, '2016-02-25 18:21:35', 0),
('23007040-1989-062f-bad9-56cf44c128c8', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd827a740-6cd2-ffb0-c481-56cdecd4fa85', 0, '2016-02-25 18:21:35', 0),
('23007836-8ea5-aa08-683c-56cf4446c1c7', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'd652e639-4b43-72a3-6628-56cdecf18c0c', 0, '2016-02-25 18:21:35', 0),
('233ef270-7b96-0557-58d6-56cf4409cbce', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'c8e55aba-4b03-83da-b59b-56cdec0a88f1', 89, '2016-02-25 18:21:35', 0),
('237d7d48-23f9-2c71-835a-56cf44b79e28', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'ca1dd328-840e-1e2d-f0f7-56cdec69ae4f', 0, '2016-02-25 18:21:35', 0),
('23bbf3f5-e3b1-0bfc-ffce-56cf44f592b6', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'c981995d-8db2-0f62-0f99-56cdeccabcd0', 0, '2016-02-25 18:21:35', 0),
('23fa78ff-be8b-6700-69c3-56cf440505fb', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'caba132b-2823-c890-27d2-56cdecd4f7d9', 0, '2016-02-25 18:21:35', 0),
('2438f48a-e056-a403-3e3f-56cf44c37212', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'ca1dd3c9-9c4b-16d1-767f-56cdec8731fb', 0, '2016-02-25 18:21:35', 0),
('247771d2-11f1-31eb-cb5a-56cf44dc247c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'c9819733-4303-cb23-3a72-56cdec805c84', 0, '2016-02-25 18:21:35', 0),
('24b5f7f5-ab68-2b8c-8e53-56cf44a3987e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'caba15d7-bf6f-5958-d81d-56cdec956a8a', 0, '2016-02-25 18:21:35', 0),
('24f47196-e772-cbb0-07c9-56cf44a7fecb', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'c8e55289-8bc4-4192-6441-56cdec1ffb83', 0, '2016-02-25 18:21:35', 0),
('2532fb08-4f10-482e-dddd-56cf44e31e27', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '49f1ce69-e8f3-e233-b782-56cdec21e0b1', 89, '2016-02-25 18:21:35', 0),
('2532fe7b-1ab4-915c-643c-56cf4444985f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4b2a46b0-2eb6-74a5-093c-56cdec3750e0', 0, '2016-02-25 18:21:35', 0),
('25717cc7-8de0-c3ef-31f3-56cf449a760e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4a8e0639-626b-12ce-08fe-56cdec954e44', 0, '2016-02-25 18:21:35', 0),
('25aff549-f1ea-57da-373a-56cf44e51e9b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4bc68a82-35cc-f64a-0885-56cdec753737', 0, '2016-02-25 18:21:35', 0),
('25ee8ddd-fee8-149f-46cd-56cf448af0b6', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4b2a421a-be3d-707e-72de-56cdec430fd7', 0, '2016-02-25 18:21:35', 0),
('262d015f-de0d-3204-75b7-56cf442c293a', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4a8e0e6f-521b-6725-2f03-56cdec16693f', 0, '2016-02-25 18:21:35', 0),
('26660241-b5b6-152a-3fe8-56cf4f3d1810', '85f6e014-4622-795d-bde1-56cf4a829665', '7372f9c8-8489-9a4b-8955-56cdec03f542', 89, '2016-02-25 19:03:12', 0),
('266607e9-6268-b572-8989-56cf4f5737a5', '85f6e014-4622-795d-bde1-56cf4a829665', '304f3ad2-a50f-737e-9249-56cdeccb5054', 0, '2016-02-25 19:03:12', 0),
('266b8135-52b7-c0ae-ffda-56cf44ed78fb', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4bc6834e-6298-e541-f38d-56cdec229512', 0, '2016-02-25 18:21:35', 0),
('26aa0046-35cf-2663-5ae4-56cf4491e4c6', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4a8e0dae-d9c0-2783-efb2-56cdec27840a', 0, '2016-02-25 18:21:35', 0),
('26e88111-fa26-65fa-664a-56cf4473c7a9', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '57fb9077-0dc4-09c2-54c1-56cdec2fcb4c', 0, '2016-02-25 18:21:35', 0),
('26e88509-df68-4b72-c0dc-56cf44382525', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '56c3176d-c92d-a626-64d0-56cdec1ba43c', 89, '2016-02-25 18:21:35', 0),
('2727088d-0c4f-85c6-a6fd-56cf44e6d3b5', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '57fb9ccb-84f3-d718-856f-56cdec603da9', 0, '2016-02-25 18:21:35', 0),
('2765821c-6f67-6910-fbc5-56cf440ee29f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '5897de46-e3f6-f0ab-3dcb-56cdec4c4391', 0, '2016-02-25 18:21:35', 0),
('276589c6-8d8b-e76c-dc88-56cf44738243', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '57fb9ba5-7b0a-93fd-b4f4-56cdecee24bc', 0, '2016-02-25 18:21:35', 0),
('27a400e0-b43a-8c35-465f-56cf44bbff2d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '575f5bbe-ff39-3f83-c3ee-56cdec0527d4', 0, '2016-02-25 18:21:35', 0),
('27e28555-d0f9-34e0-f2f7-56cf44f2e011', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '5897ddc0-2c9c-84b5-5107-56cdec1c9617', 0, '2016-02-25 18:21:35', 0),
('27e28c24-36ac-b85e-3cc9-56cf44f1733a', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '575f52be-23af-2bf5-0f40-56cdec3f84ba', 0, '2016-02-25 18:21:35', 0),
('282108fc-0e1a-4ddb-4efb-56cf442c52d0', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bcb133f6-2022-85f7-2045-56cdece12226', 89, '2016-02-25 18:21:35', 0),
('285f8c79-4d51-78b9-7f96-56cf44b09637', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bde9b83c-4cf2-944f-e5fd-56cdecfac197', 0, '2016-02-25 18:21:35', 0),
('28dc831d-a2be-3594-c43a-56cf444f6f22', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'be85f260-ec1c-19b4-b81b-56cdeccd7955', 0, '2016-02-25 18:21:35', 0),
('28dc8e27-db18-a881-0ace-56cf44c44b32', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bd4d7fc1-5d4e-2f1b-d53c-56cdece3986c', 0, '2016-02-25 18:21:35', 0),
('291b07e8-198b-c638-36ba-56cf442f3d52', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bde9b8a6-c214-96ba-40a1-56cdec3419ef', 0, '2016-02-25 18:21:35', 0),
('2959876e-6481-6fdb-afe4-56cf446f799a', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bd4d771c-7a3c-7478-2a64-56cdec83b099', 0, '2016-02-25 18:21:35', 0),
('299804b3-8e46-b708-6fb3-56cf44494e8b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'be85f05a-05c0-2a69-3c75-56cdecba2645', 0, '2016-02-25 18:21:35', 0),
('29d68884-29db-83ca-137b-56cf44cf9c02', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bcb1326c-8c49-f763-4b49-56cdec336058', 0, '2016-02-25 18:21:35', 0),
('2a1505b5-ed99-e014-7e12-56cf4461f980', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1506ec4-5d5e-2550-998f-56cdec917e71', 89, '2016-02-25 18:21:35', 0),
('2a4e04d8-f9df-3b53-ec58-56cf4fd2fe96', '85f6e014-4622-795d-bde1-56cf4a829665', '74ab7213-b309-c53d-48e1-56cdec683548', 0, '2016-02-25 19:03:12', 0),
('2a5396d3-2900-0eb6-6c76-56cf4434aafb', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e288eb94-e337-dfe0-14ec-56cdecfd037c', 0, '2016-02-25 18:21:35', 0),
('2a5398b6-478c-8d32-db92-56cf441c2451', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1ecaafe-21b0-35b7-840f-56cdec8a90cc', 0, '2016-02-25 18:21:35', 0),
('2a921682-b8aa-12f2-bd2d-56cf4424d3ff', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e288e39e-c594-4b2a-2053-56cdec35ad38', 0, '2016-02-25 18:21:35', 0),
('2b0f1339-8682-267e-eae2-56cf44ec5fdb', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e288eedf-37da-cc95-25a8-56cdec9b60a7', 0, '2016-02-25 18:21:35', 0),
('2b0f1608-8846-d187-3f47-56cf44892d58', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1eca01c-122c-21af-6d20-56cdec7c1ae4', 0, '2016-02-25 18:21:35', 0),
('2b4d9f73-2875-461d-62b2-56cf4423a780', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e3252f6e-fa05-fcad-4b0a-56cdec2cb0ce', 0, '2016-02-25 18:21:35', 0),
('2b8c1047-eba0-d184-6f9f-56cf44cec2d6', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1506e58-17e1-437b-25ac-56cdec0f9c2c', 0, '2016-02-25 18:21:35', 0),
('2bca99c3-6182-be18-e0ed-56cf4487f7f8', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '86fb06a2-9e3e-8847-7bde-56cdecdd001b', 89, '2016-02-25 18:21:35', 0),
('2c091a49-60ec-2feb-2591-56cf44c14d98', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '88338565-c80c-c96f-0d2a-56cdec96bcee', 0, '2016-02-25 18:21:35', 0),
('2c091bcc-1580-a558-b67e-56cf44326e52', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '87974d95-9641-0845-1b34-56cdecc0c155', 0, '2016-02-25 18:21:35', 0),
('2c479348-3733-0da3-f6d2-56cf44707db0', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '88cfcd98-2984-fe4b-4bd3-56cdec5b83d0', 0, '2016-02-25 18:21:35', 0),
('2c861484-829c-4472-b1ef-56cf447daec1', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '88338e1e-725f-1999-7426-56cdece13725', 0, '2016-02-25 18:21:35', 0),
('2cc4909d-9b51-64e2-3411-56cf44583a2d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '8797447a-0e90-4df7-f8bf-56cdec706357', 0, '2016-02-25 18:21:35', 0),
('2d031eb8-98a1-b0ce-1af9-56cf44bc70ec', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '88cfc781-5782-a5dd-6f44-56cdec7729d2', 0, '2016-02-25 18:21:35', 0),
('2d419052-7f4b-0219-36c3-56cf4457562a', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e0b32578-0066-4644-fa90-56cdec75394c', 89, '2016-02-25 18:21:35', 0),
('2d419c0e-c259-b673-3a49-56cf44e8a4a1', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '86fb0d3d-47d0-a74b-d5ac-56cdec657b74', 0, '2016-02-25 18:21:35', 0),
('2d80170b-0bbd-b299-4351-56cf4475c01e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1eba702-72ea-a559-f5ad-56cdec52097d', 0, '2016-02-25 18:21:35', 0),
('2dbe90dc-cbbd-ec1e-7a26-56cf44fbe8ad', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e287e328-68b8-4d89-86a5-56cdecd9a49f', 0, '2016-02-25 18:21:35', 0),
('2dbe9378-daad-edbd-6267-56cf4488c95b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1eba0de-8968-1b9c-d799-56cdecd2d7ee', 0, '2016-02-25 18:21:35', 0),
('2dfd1c3e-9218-4fb9-02d0-56cf44dc437f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e1eba26c-36af-481d-087d-56cdec0e733a', 0, '2016-02-25 18:21:35', 0),
('2e360bd0-4d85-65b8-85d7-56cf4f74754d', '85f6e014-4622-795d-bde1-56cf4a829665', '74ab7acb-68ac-e950-4a8e-56cdecd118a6', 0, '2016-02-25 19:03:12', 0),
('2e3b9206-f154-4c6d-6b83-56cf44dd0dff', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e14f62f8-917e-164a-7b57-56cdeccdee72', 0, '2016-02-25 18:21:35', 0),
('2e7a293f-856e-6f30-0c90-56cf44768fcd', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e14f6cb1-fe3a-4bab-730a-56cdec435f9c', 0, '2016-02-25 18:21:35', 0),
('2e7a2bf9-fea3-3958-ad03-56cf44f3bd76', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'e287e56d-45fd-70aa-9afe-56cdec72ea3c', 0, '2016-02-25 18:21:35', 0),
('2eb8a1c2-4fc9-6f44-4a76-56cf44f3542c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '1f380b24-e398-3dfe-6a32-56cdec6af30f', 89, '2016-02-25 18:21:35', 0),
('2ef7225a-5445-1828-bc87-56cf4430601b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '1fd44c1f-5607-4bb9-997c-56cdecb26b5c', 0, '2016-02-25 18:21:35', 0),
('2ef7235b-8991-7e9c-f987-56cf4400b3c3', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '1fd4471d-8cbb-5ccb-d584-56cdecd557fa', 0, '2016-02-25 18:21:35', 0),
('2f35a032-6d02-9c9a-0668-56cf4481daa5', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '2070afcc-be95-e14b-d080-56cdec232675', 0, '2016-02-25 18:21:35', 0),
('2f35a633-8f31-6ed9-1a7f-56cf44853a5b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '2070a78f-b40c-1737-a5f2-56cdecc438a4', 0, '2016-02-25 18:21:35', 0),
('2f74225b-96ff-ccb8-086e-56cf444031a3', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '1f3809f6-3f4b-976c-9082-56cdec06ec59', 0, '2016-02-25 18:21:35', 0),
('2fb2a04a-d1c8-87b3-906d-56cf4416314d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '1f380c32-6f35-9c96-7981-56cdec3ab498', 0, '2016-02-25 18:21:35', 0),
('2fb2aaf2-1480-1eaa-ae72-56cf442becfc', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '2070a70f-c9c9-ab77-c082-56cdece04ff7', 0, '2016-02-25 18:21:35', 0),
('2ff123f4-7efc-d06a-0c61-56cf44faf5a6', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7b623381-cad8-8d7a-818b-56cdec046596', 89, '2016-02-25 18:21:35', 0),
('302fa75f-c583-4152-0483-56cf44d5637c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7c9ababb-1318-a321-18b5-56cdec7d2e9c', 0, '2016-02-25 18:21:35', 0),
('302fa8fc-b6d1-3d0b-f971-56cf446dd19c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7bfe63ab-0e84-0127-b9f4-56cdec9295a2', 0, '2016-02-25 18:21:35', 0),
('306e2e17-6550-39eb-f66a-56cf444f18f4', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7d36f412-d040-8c97-8b71-56cdecd0d604', 0, '2016-02-25 18:21:35', 0),
('30acac9d-fa86-9e33-9881-56cf44ab56f2', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7c9abce8-46cd-915e-166c-56cdecca8263', 0, '2016-02-25 18:21:35', 0),
('30eb29e7-709e-4949-996b-56cf44d5ceae', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7bfe6b78-374d-6105-6777-56cdecded90d', 0, '2016-02-25 18:21:35', 0),
('30eb2aa6-a23b-6445-f88c-56cf440d486a', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7d36f6ea-fe4f-ce5b-a5c7-56cdec262879', 0, '2016-02-25 18:21:35', 0),
('3129a901-05d3-d29f-c334-56cf44616740', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7bfe6725-ea46-5799-b738-56cdec39e037', 0, '2016-02-25 18:21:35', 0),
('3129ae67-e792-ed0f-504d-56cf448e8934', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '304f30cf-87d1-c037-7d60-56cdec576eda', 89, '2016-02-25 18:21:35', 0),
('31682d60-b7a1-03a2-4844-56cf44a1a61f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '30eb783c-9d05-33df-b503-56cdeca5349a', 0, '2016-02-25 18:21:35', 0),
('31a6a592-bd49-95ac-7265-56cf44a33626', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '30eb7b8d-a6ca-567d-f4b6-56cdec01bc25', 0, '2016-02-25 18:21:35', 0),
('31e5298a-ad9a-9c86-4b7d-56cf44f49d5d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '3187bdfd-73e6-159e-a382-56cdecc9fd90', 0, '2016-02-25 18:21:35', 0),
('321e0a7b-34f0-2b02-2dfb-56cf4faa052d', '85f6e014-4622-795d-bde1-56cf4a829665', '74ab75d9-4028-9914-de0c-56cdec48f0d6', 0, '2016-02-25 19:03:12', 0),
('321e0bbf-328e-9919-78c1-56cf4fb8d908', '85f6e014-4622-795d-bde1-56cf4a829665', '7547bbf9-bfe0-3c3c-05ae-56cdec37b9ae', 0, '2016-02-25 19:03:12', 0),
('3223a8a8-f790-52ac-7d8c-56cf445ea9aa', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '3187bbfc-1742-bd35-bbb1-56cdecdac7a0', 0, '2016-02-25 18:21:35', 0),
('32622319-46aa-39bf-cfaa-56cf44822bf5', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '3187bd0c-33b2-ed29-a61d-56cdeca594e8', 0, '2016-02-25 18:21:35', 0),
('32622fe9-518a-2e0a-3476-56cf44b71a6f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '304f382d-f9f7-2448-b7ce-56cdece78a77', 0, '2016-02-25 18:21:35', 0),
('32a0a61d-cde9-0089-c4e9-56cf44134587', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '304f3ad2-a50f-737e-9249-56cdeccb5054', 0, '2016-02-25 18:21:35', 0),
('32df324b-5497-2826-5097-56cf4474b17d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '74ab7213-b309-c53d-48e1-56cdec683548', 0, '2016-02-25 18:21:35', 0),
('32df32dc-3986-4383-3442-56cf4422ff65', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7372f9c8-8489-9a4b-8955-56cdec03f542', 89, '2016-02-25 18:21:35', 0),
('331db4ba-ab19-ffb3-ae69-56cf4423a443', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7547bbf9-bfe0-3c3c-05ae-56cdec37b9ae', 0, '2016-02-25 18:21:35', 0),
('331dbea3-a921-a310-4ed0-56cf4420a806', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '74ab7acb-68ac-e950-4a8e-56cdecd118a6', 0, '2016-02-25 18:21:35', 0),
('335c3269-f574-1f10-3116-56cf441ae6f0', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '74ab75d9-4028-9914-de0c-56cdec48f0d6', 0, '2016-02-25 18:21:35', 0),
('339ab508-692c-65f8-48f5-56cf44be5a87', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '740f39cb-a788-caa1-70ce-56cdec0a0ecd', 0, '2016-02-25 18:21:35', 0),
('339ab5b8-5942-2067-ead3-56cf44180a7c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '7547be18-77e1-e56c-05a9-56cdec9bef04', 0, '2016-02-25 18:21:35', 0),
('33d002e8-e102-d201-cddb-56cf4f7d631d', '85f6e014-4622-795d-bde1-56cf4a829665', '7d36f412-d040-8c97-8b71-56cdecd0d604', 0, '2016-02-25 19:03:12', 0),
('33d00f50-3509-369d-0fa4-56cf4f2847a5', '85f6e014-4622-795d-bde1-56cf4a829665', '7bfe63ab-0e84-0127-b9f4-56cdec9295a2', 0, '2016-02-25 19:03:12', 0),
('33d93aef-b91f-71cd-6b25-56cf44fc9c8b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '740f3c61-8fe5-c695-ffe1-56cdec5ba0a1', 0, '2016-02-25 18:21:35', 0),
('3417b541-a67a-ea04-8945-56cf446469be', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6394693d-9e98-501b-751e-56cdecee05cb', 0, '2016-02-25 18:21:35', 0),
('3417bad3-f199-1b44-070e-56cf44897ab7', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '62f82b7b-4792-15a8-bf69-56cdec585597', 89, '2016-02-25 18:21:35', 0),
('34563644-d686-3ad8-5d1c-56cf4428c956', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '63946767-0fda-3b44-6e6e-56cdecc3696a', 0, '2016-02-25 18:21:35', 0),
('3494b49c-131a-4224-a936-56cf446c31d7', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6430ab5e-cb6b-4a78-9de1-56cdecfebdb7', 0, '2016-02-25 18:21:35', 0),
('3494bfcf-873d-4b09-d50f-56cf44379d6a', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6430ac46-3c8f-b466-ea73-56cdecd0bcf7', 0, '2016-02-25 18:21:35', 0),
('34d33d3d-fe66-919a-f8ec-56cf44261392', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '62f8229b-c83f-c6a1-654a-56cdecd2033e', 0, '2016-02-25 18:21:35', 0),
('3511b2e2-175a-79c1-7780-56cf44ac3418', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6430a33a-a196-e742-efb4-56cdecf1eb53', 0, '2016-02-25 18:21:35', 0),
('355035a6-b65a-bc74-75e7-56cf445f1b51', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '62f8295c-c54e-8af3-d827-56cdec5d9852', 0, '2016-02-25 18:21:35', 0),
('35503db7-e0c4-7975-08ba-56cf44bc329f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6cbc22f5-7a2f-4e90-3165-56cdec1896b7', 89, '2016-02-25 18:21:35', 0),
('358eb73e-0b26-b0f1-f8e0-56cf44fe1738', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6d5860ec-f1f6-c318-bd2e-56cdecf7f1eb', 0, '2016-02-25 18:21:35', 0),
('35cd3dba-3b66-b20d-f796-56cf44f9428d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6df4a3a3-2995-3d7b-ccee-56cdecf9ca3b', 0, '2016-02-25 18:21:35', 0),
('35cd3e0d-ed55-80e9-8a1a-56cf445e3be9', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6d58677b-5905-bdb7-8895-56cdec11c6b1', 0, '2016-02-25 18:21:35', 0),
('360603d7-cc85-1901-b407-56cf4f9917c7', '85f6e014-4622-795d-bde1-56cf4a829665', '740f39cb-a788-caa1-70ce-56cdec0a0ecd', 0, '2016-02-25 19:03:12', 0),
('360bb58b-228b-f31b-d8c8-56cf44b3f8f0', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6df4a567-ad97-5790-63d3-56cdecb17770', 0, '2016-02-25 18:21:35', 0),
('364a3e4d-d2fc-2246-b7e7-56cf442ff175', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6d586c6f-e01b-aba4-5070-56cdec3432ed', 0, '2016-02-25 18:21:35', 0),
('3688ba62-3ea0-051a-ad32-56cf445ce9e1', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6df4ac58-4604-b27a-029c-56cdec973aa5', 0, '2016-02-25 18:21:35', 0),
('3688baea-c32f-58ef-9f48-56cf445c210d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '6cbc2b1f-00f3-fe68-f7db-56cdec812648', 0, '2016-02-25 18:21:35', 0),
('36c739ad-7710-179b-5382-56cf44ad7fe3', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '32480a66-369c-b9f3-37c4-56cdec67a037', 89, '2016-02-25 18:21:35', 0),
('3705c2b4-9fb4-28e5-0812-56cf44f9ffb1', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '45d00526-254f-d1ed-d381-56cdece7c5ca', 0, '2016-02-25 18:21:35', 0),
('374441b2-62b3-f00f-62fd-56cf446fb19e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '3c0c0cdf-edc7-db23-2bed-56cdeca05ddc', 0, '2016-02-25 18:21:35', 0),
('374449de-478a-6cfa-d7c6-56cf4448e344', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '45d00175-dabc-5afb-12aa-56cdeca2a2f9', 0, '2016-02-25 18:21:35', 0),
('3782c807-09fe-9e8b-cff1-56cf44e9c61c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '45d00440-85b7-a568-9be1-56cdecaf1630', 0, '2016-02-25 18:21:35', 0),
('37c14c92-d8ec-150d-11d5-56cf44df8be2', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '4f9401f8-ffe0-dea4-b1c8-56cdec1f9371', 0, '2016-02-25 18:21:35', 0),
('37c14e1e-4400-1260-7f34-56cf44beb19c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '3c0c0e50-e656-4a31-431c-56cdecb5d3ce', 0, '2016-02-25 18:21:35', 0),
('37ffcab8-0eba-8f19-9268-56cf44a9340c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '324802fb-647c-25e9-31fa-56cdec8595a0', 0, '2016-02-25 18:21:35', 0),
('383e46e6-a3e2-0779-1020-56cf44c39057', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '27c3af65-f8b3-f474-db93-56cdec82403d', 89, '2016-02-25 18:21:35', 0),
('383e4ebc-9ea4-701b-133b-56cf44b818b9', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '28fc2df4-b650-8fea-c43d-56cdec731309', 0, '2016-02-25 18:21:35', 0),
('387cc61e-798b-e07f-b8e7-56cf44acf89e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '28fc27f9-9adf-75d2-e441-56cdece77527', 0, '2016-02-25 18:21:35', 0),
('387ccba8-7a86-6110-3534-56cf44516e23', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '29986b57-1968-52e4-ba83-56cdeca4a423', 0, '2016-02-25 18:21:35', 0),
('38bb4718-c213-b17f-1216-56cf4460729e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '28fc23e1-d4d9-6fc3-ccc0-56cdec490bf9', 0, '2016-02-25 18:21:35', 0),
('38f9c85e-8255-66b1-6279-56cf442800f7', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '285fee4b-2ddc-7359-ba0b-56cdec8e730f', 0, '2016-02-25 18:21:35', 0),
('38f9ce6d-9647-052c-a74d-56cf44fb5b8d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '29986c60-e3d6-6515-9d8c-56cdec824907', 0, '2016-02-25 18:21:35', 0),
('3938470b-da3e-6c97-84bd-56cf442e14ab', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '285fe893-829f-b7a4-a583-56cdec73f288', 0, '2016-02-25 18:21:35', 0),
('3976c86f-9832-0bf4-f7fa-56cf448adf4e', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '90bf17fb-6828-4428-d5e5-56cdecf34e6a', 89, '2016-02-25 18:21:35', 0),
('39b5464f-289a-cc80-27bf-56cf44dccd58', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '915b565f-cf3e-1886-8215-56cdec12693c', 0, '2016-02-25 18:21:35', 0),
('39b549a0-2d7d-a1b3-bafe-56cf4478ee5d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '91f796f2-156f-8bfb-9113-56cdeca68acc', 0, '2016-02-25 18:21:35', 0),
('39ee0fad-8f7f-f3eb-30bb-56cf4f9894f6', '85f6e014-4622-795d-bde1-56cf4a829665', '7547be18-77e1-e56c-05a9-56cdec9bef04', 0, '2016-02-25 19:03:12', 0),
('39f3cb31-0792-7239-86d2-56cf4409800b', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9293d41e-f105-cead-f873-56cdec8df346', 0, '2016-02-25 18:21:35', 0),
('3a324ce5-1298-e860-2776-56cf446462df', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '91f79a4a-2720-1170-d8e7-56cdec6f9022', 0, '2016-02-25 18:21:35', 0),
('3a70c218-4a56-cb76-9bc8-56cf4422bd75', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '915b52ef-3944-7936-0b0e-56cdec8b743a', 0, '2016-02-25 18:21:35', 0),
('3a70cee2-ea2f-5412-2886-56cf4467d007', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9293de9f-2f65-a7c1-dac5-56cdec9a96a5', 0, '2016-02-25 18:21:35', 0),
('3aaf4983-f7f9-ab1e-559d-56cf44485d00', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '90bf169b-1ea3-b5cf-b3c2-56cdec5257ff', 0, '2016-02-25 18:21:35', 0),
('3aedc934-4735-58a9-2524-56cf44d92ff3', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '994a959e-c38d-05df-e9d0-56cdec8259db', 89, '2016-02-25 18:21:35', 0),
('3b2c47ae-d85c-e007-2b08-56cf4462bc69', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9a8313f7-a165-62a3-f6a5-56cdec303cd7', 0, '2016-02-25 18:21:35', 0),
('3b2c4d6d-fcf8-ef96-a9f2-56cf44bd2ae3', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9a8316ce-b8e2-4c44-59d3-56cdeccfe6b9', 0, '2016-02-25 18:21:35', 0),
('3b6ad7a7-8b25-3389-2198-56cf44d69bdd', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9a831103-6445-c601-8931-56cdec4da2e7', 0, '2016-02-25 18:21:35', 0),
('3b6ad95f-c325-78cb-75aa-56cf4492986c', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9b1f562d-5de9-2609-254e-56cdec5443d1', 0, '2016-02-25 18:21:35', 0),
('3ba95c0d-d978-2727-35b8-56cf44437b5f', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '99e6da36-ca46-28f6-f1e4-56cdecd6f1dd', 0, '2016-02-25 18:21:35', 0),
('3be7d337-270e-4d4b-bcad-56cf441853bd', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '9b1f50c5-2826-e023-1491-56cdec27b9f5', 0, '2016-02-25 18:21:35', 0),
('3be7d838-771b-35cf-c91e-56cf4497b648', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '99e6d4e5-cf83-331b-1b19-56cdec3e3eeb', 0, '2016-02-25 18:21:35', 0),
('3c265973-074c-d1ef-c64c-56cf44dc1aa4', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'a77806cd-4d6b-bad0-b455-56cdec8cbb90', 89, '2016-02-25 18:21:35', 0),
('3c64dd08-aff4-28ff-b28c-56cf44601dcb', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'b13c091e-2d88-078a-5b6a-56cdec4d8d4c', 0, '2016-02-25 18:21:35', 0),
('3c64df3b-fa15-a4c1-8768-56cf44532b64', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bb000bd2-c5ff-2522-802d-56cdec74f84c', 0, '2016-02-25 18:21:35', 0),
('3ca35722-b200-6b7d-b0d5-56cf44660162', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bb000eb1-49b9-0800-baf8-56cdec19c21f', 0, '2016-02-25 18:21:35', 0),
('3ce1d81f-e1b9-a022-8e88-56cf44c3cfa4', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'b13c026c-8f9f-1d3d-f50d-56cdecef0f6e', 0, '2016-02-25 18:21:35', 0),
('3ce1d881-0b92-3afe-279d-56cf44b66f74', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'bb000781-30ec-0bca-d199-56cdec0ff3cc', 0, '2016-02-25 18:21:35', 0),
('3d205d5a-f38e-d7cd-44ac-56cf44d3e8c3', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'c4c40319-10b7-08fb-9deb-56cdec41d432', 0, '2016-02-25 18:21:35', 0),
('3d5ed030-35e9-3b41-a4da-56cf4455a36d', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'b13c01f3-727f-a8ac-3975-56cdec6b8327', 0, '2016-02-25 18:21:35', 0),
('41be049c-35d6-e6c3-8c3c-56cf4fa7d14c', '85f6e014-4622-795d-bde1-56cf4a829665', '740f3c61-8fe5-c695-ffe1-56cdec5ba0a1', 0, '2016-02-25 19:03:12', 0),
('41be0954-f66b-9ae3-50aa-56cf4fe6895c', '85f6e014-4622-795d-bde1-56cf4a829665', '62f82b7b-4792-15a8-bf69-56cdec585597', 89, '2016-02-25 19:03:12', 0),
('45a6051b-b204-9314-aaa4-56cf4f8ec323', '85f6e014-4622-795d-bde1-56cf4a829665', '6394693d-9e98-501b-751e-56cdecee05cb', 0, '2016-02-25 19:03:12', 0),
('481cd260-2981-df06-b5b4-56cf52b532dd', '55effc16-809c-3ff4-f464-56cf528b4049', '14d7d43b-77b6-8a09-9a24-56cdec1003fc', -98, '2016-02-25 19:22:19', 0),
('48d85c2f-dc32-895e-8683-56cf5293f0b4', '55effc16-809c-3ff4-f464-56cf528b4049', '16105230-442a-bc29-a26c-56cdec269d7b', 0, '2016-02-25 19:22:19', 0),
('4955559e-d93d-fee9-c1a0-56cf525b3a05', '55effc16-809c-3ff4-f464-56cf528b4049', '15741114-c0ba-f279-f588-56cdec6c559e', 0, '2016-02-25 19:22:19', 0),
('498e055e-6ff2-6eee-b549-56cf4f687179', '85f6e014-4622-795d-bde1-56cf4a829665', '63946767-0fda-3b44-6e6e-56cdecc3696a', 0, '2016-02-25 19:03:12', 0),
('498e0aeb-a8b6-616e-e717-56cf4f11fcdc', '85f6e014-4622-795d-bde1-56cf4a829665', '6430ac46-3c8f-b466-ea73-56cdecd0bcf7', 0, '2016-02-25 19:03:12', 0),
('4993ddfc-2a37-4dcb-cb50-56cf527c9c54', '55effc16-809c-3ff4-f464-56cf528b4049', '161055c3-53ad-2977-e398-56cdeca68276', 0, '2016-02-25 19:22:19', 0),
('49d25d21-8a39-c182-dd28-56cf5250ac05', '55effc16-809c-3ff4-f464-56cf528b4049', '16105372-05d8-6637-957f-56cdec995194', 0, '2016-02-25 19:22:19', 0),
('4a4f5176-d178-e381-8ad6-56cf52264ac0', '55effc16-809c-3ff4-f464-56cf528b4049', '1574161f-a535-97bf-1027-56cdec1f47df', 0, '2016-02-25 19:22:19', 0),
('4a4f5931-6490-2aea-e51b-56cf523ba497', '55effc16-809c-3ff4-f464-56cf528b4049', '16ac9f31-61dd-9c39-297c-56cdec83d86a', 0, '2016-02-25 19:22:19', 0),
('4a8ddd69-8f38-6076-7d7d-56cf52e3490c', '55effc16-809c-3ff4-f464-56cf528b4049', '14d7d88a-fa01-441c-9662-56cdeca4425c', 0, '2016-02-25 19:22:19', 0),
('4acc59f2-0d1c-6950-dd99-56cf52a36d67', '55effc16-809c-3ff4-f464-56cf528b4049', 'd5b6a7b4-4694-2e88-c790-56cdec370884', -98, '2016-02-25 19:22:19', 0),
('4b0ad6cd-0e0d-eeed-44a3-56cf52071ff3', '55effc16-809c-3ff4-f464-56cf528b4049', 'd6ef2dec-b837-67fa-4eeb-56cdecf90424', 0, '2016-02-25 19:22:19', 0),
('4b49523a-48d8-650e-3e08-56cf52c291f1', '55effc16-809c-3ff4-f464-56cf528b4049', 'd6ef256b-9418-e055-298a-56cdec9c3813', 0, '2016-02-25 19:22:19', 0),
('4b87e2de-611e-ada9-94a9-56cf52042912', '55effc16-809c-3ff4-f464-56cf528b4049', 'd78b69ca-5084-d4a2-d59d-56cdec23ebbf', 0, '2016-02-25 19:22:19', 0),
('4bc6695e-d306-c174-86dc-56cf520902ef', '55effc16-809c-3ff4-f464-56cf528b4049', 'd78b6b83-d047-3d4d-cb9d-56cdecd9c661', 0, '2016-02-25 19:22:19', 0),
('4bc66f75-de79-c542-e74f-56cf522ff088', '55effc16-809c-3ff4-f464-56cf528b4049', 'd652ee7e-fc21-18e6-19ab-56cdec5f692b', 0, '2016-02-25 19:22:19', 0),
('4c04e162-583d-ccff-3761-56cf52bee2c0', '55effc16-809c-3ff4-f464-56cf528b4049', 'd827a740-6cd2-ffb0-c481-56cdecd4fa85', 0, '2016-02-25 19:22:19', 0),
('4c4362f8-9a8f-11b7-d620-56cf52e603f3', '55effc16-809c-3ff4-f464-56cf528b4049', 'd652e639-4b43-72a3-6628-56cdecf18c0c', 0, '2016-02-25 19:22:19', 0),
('4c81e656-2ca7-c3a0-57d5-56cf5206f5b7', '55effc16-809c-3ff4-f464-56cf528b4049', 'c8e55aba-4b03-83da-b59b-56cdec0a88f1', -98, '2016-02-25 19:22:19', 0),
('4cc06252-3561-9b7f-a0da-56cf52be02c9', '55effc16-809c-3ff4-f464-56cf528b4049', 'ca1dd328-840e-1e2d-f0f7-56cdec69ae4f', 0, '2016-02-25 19:22:19', 0),
('4cfee409-4918-5fc7-6636-56cf52ebf30a', '55effc16-809c-3ff4-f464-56cf528b4049', 'c981995d-8db2-0f62-0f99-56cdeccabcd0', 0, '2016-02-25 19:22:19', 0),
('4d3d63dc-a12d-f9f6-1a42-56cf52c764f3', '55effc16-809c-3ff4-f464-56cf528b4049', 'caba132b-2823-c890-27d2-56cdecd4f7d9', 0, '2016-02-25 19:22:19', 0),
('4d3d6d0a-bec7-1f68-669c-56cf523928b8', '55effc16-809c-3ff4-f464-56cf528b4049', 'ca1dd3c9-9c4b-16d1-767f-56cdec8731fb', 0, '2016-02-25 19:22:19', 0),
('4d760362-3d1b-3e03-ce2a-56cf4f6f5665', '85f6e014-4622-795d-bde1-56cf4a829665', '6430ab5e-cb6b-4a78-9de1-56cdecfebdb7', 0, '2016-02-25 19:03:12', 0),
('4d7609c0-8aea-8575-4e65-56cf4fe767cb', '85f6e014-4622-795d-bde1-56cf4a829665', '62f8229b-c83f-c6a1-654a-56cdecd2033e', 0, '2016-02-25 19:03:12', 0),
('4d7bef7a-02a0-547c-69a9-56cf527e0644', '55effc16-809c-3ff4-f464-56cf528b4049', 'c9819733-4303-cb23-3a72-56cdec805c84', 0, '2016-02-25 19:22:19', 0),
('4dba6dc8-018e-a7a3-2469-56cf52b8ea6c', '55effc16-809c-3ff4-f464-56cf528b4049', 'c8e55289-8bc4-4192-6441-56cdec1ffb83', 0, '2016-02-25 19:22:19', 0),
('4dba6ed8-106b-8153-2241-56cf527f4c6f', '55effc16-809c-3ff4-f464-56cf528b4049', 'caba15d7-bf6f-5958-d81d-56cdec956a8a', 0, '2016-02-25 19:22:19', 0),
('4df8e298-a517-57da-2b2c-56cf526a8f37', '55effc16-809c-3ff4-f464-56cf528b4049', '49f1ce69-e8f3-e233-b782-56cdec21e0b1', 89, '2016-02-25 19:22:19', 0),
('4e376e1b-eb94-9301-5924-56cf52e72fe5', '55effc16-809c-3ff4-f464-56cf528b4049', '4b2a46b0-2eb6-74a5-093c-56cdec3750e0', 0, '2016-02-25 19:22:19', 0),
('4e75e768-5fae-3335-ce43-56cf521ca97b', '55effc16-809c-3ff4-f464-56cf528b4049', '4a8e0639-626b-12ce-08fe-56cdec954e44', 0, '2016-02-25 19:22:19', 0),
('4eb46170-01b6-3d7e-d426-56cf521ef445', '55effc16-809c-3ff4-f464-56cf528b4049', '4bc68a82-35cc-f64a-0885-56cdec753737', 0, '2016-02-25 19:22:19', 0),
('4ef2e617-5489-11f4-9bc1-56cf521dc7d5', '55effc16-809c-3ff4-f464-56cf528b4049', '4b2a421a-be3d-707e-72de-56cdec430fd7', 0, '2016-02-25 19:22:19', 0),
('4f316f31-74b4-80ae-8d0a-56cf521ff461', '55effc16-809c-3ff4-f464-56cf528b4049', '4a8e0e6f-521b-6725-2f03-56cdec16693f', 0, '2016-02-25 19:22:19', 0),
('4f6feb9f-0345-f4f6-0480-56cf52d56ce5', '55effc16-809c-3ff4-f464-56cf528b4049', '4bc6834e-6298-e541-f38d-56cdec229512', 0, '2016-02-25 19:22:19', 0),
('4fae7472-ef71-98f0-4ca6-56cf52e73782', '55effc16-809c-3ff4-f464-56cf528b4049', '56c3176d-c92d-a626-64d0-56cdec1ba43c', -98, '2016-02-25 19:22:19', 0),
('4fae7926-2305-0611-41db-56cf52a54258', '55effc16-809c-3ff4-f464-56cf528b4049', '4a8e0dae-d9c0-2783-efb2-56cdec27840a', 0, '2016-02-25 19:22:19', 0),
('4fecf6ff-398f-2d57-422e-56cf52a27828', '55effc16-809c-3ff4-f464-56cf528b4049', '57fb9077-0dc4-09c2-54c1-56cdec2fcb4c', 0, '2016-02-25 19:22:19', 0),
('502b7e1a-c966-5d19-d446-56cf5297fa93', '55effc16-809c-3ff4-f464-56cf528b4049', '57fb9ccb-84f3-d718-856f-56cdec603da9', 0, '2016-02-25 19:22:19', 0),
('5069f245-da1f-cc39-c347-56cf527aea24', '55effc16-809c-3ff4-f464-56cf528b4049', '5897de46-e3f6-f0ab-3dcb-56cdec4c4391', 0, '2016-02-25 19:22:19', 0),
('5069f378-fa0b-4c73-7d52-56cf5242b07a', '55effc16-809c-3ff4-f464-56cf528b4049', '57fb9ba5-7b0a-93fd-b4f4-56cdecee24bc', 0, '2016-02-25 19:22:19', 0),
('50a871e0-0ece-3522-0309-56cf52dfdbb9', '55effc16-809c-3ff4-f464-56cf528b4049', '575f5bbe-ff39-3f83-c3ee-56cdec0527d4', 0, '2016-02-25 19:22:19', 0),
('50e6f53c-9e73-5874-51e8-56cf52d7b23c', '55effc16-809c-3ff4-f464-56cf528b4049', '5897ddc0-2c9c-84b5-5107-56cdec1c9617', 0, '2016-02-25 19:22:19', 0),
('512576ee-49a6-0e48-6a67-56cf52f124a5', '55effc16-809c-3ff4-f464-56cf528b4049', '575f52be-23af-2bf5-0f40-56cdec3f84ba', 0, '2016-02-25 19:22:19', 0),
('515e09d1-4fa0-86fc-c5ef-56cf4f393a19', '85f6e014-4622-795d-bde1-56cf4a829665', '6430a33a-a196-e742-efb4-56cdecf1eb53', 0, '2016-02-25 19:03:12', 0),
('5163f8e1-ea4f-74f9-44ad-56cf5240b017', '55effc16-809c-3ff4-f464-56cf528b4049', 'bde9b83c-4cf2-944f-e5fd-56cdecfac197', 0, '2016-02-25 19:22:19', 0),
('5163fde0-d40a-521e-19e6-56cf52e90a51', '55effc16-809c-3ff4-f464-56cf528b4049', 'bcb133f6-2022-85f7-2045-56cdece12226', -98, '2016-02-25 19:22:19', 0),
('51a27ad0-2678-7c3e-11f2-56cf522b87f0', '55effc16-809c-3ff4-f464-56cf528b4049', 'bd4d7fc1-5d4e-2f1b-d53c-56cdece3986c', 0, '2016-02-25 19:22:19', 0),
('51e0f068-5484-2479-8529-56cf5279904d', '55effc16-809c-3ff4-f464-56cf528b4049', 'be85f260-ec1c-19b4-b81b-56cdeccd7955', 0, '2016-02-25 19:22:19', 0),
('521f73af-3927-a5bf-3772-56cf5232162e', '55effc16-809c-3ff4-f464-56cf528b4049', 'bd4d771c-7a3c-7478-2a64-56cdec83b099', 0, '2016-02-25 19:22:19', 0),
('521f7d91-5a36-ed92-33a3-56cf5238e747', '55effc16-809c-3ff4-f464-56cf528b4049', 'bde9b8a6-c214-96ba-40a1-56cdec3419ef', 0, '2016-02-25 19:22:19', 0),
('525df371-e1b0-fd90-5127-56cf52b57aa1', '55effc16-809c-3ff4-f464-56cf528b4049', 'be85f05a-05c0-2a69-3c75-56cdecba2645', 0, '2016-02-25 19:22:19', 0),
('529c78f4-c434-bc9d-dd60-56cf52d66eae', '55effc16-809c-3ff4-f464-56cf528b4049', 'bcb1326c-8c49-f763-4b49-56cdec336058', 0, '2016-02-25 19:22:19', 0),
('52daf323-71d4-0059-48e9-56cf528a08e5', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1506ec4-5d5e-2550-998f-56cdec917e71', -98, '2016-02-25 19:22:19', 0),
('53197f35-1a07-ed30-94dc-56cf527907a3', '55effc16-809c-3ff4-f464-56cf528b4049', 'e288eb94-e337-dfe0-14ec-56cdecfd037c', 0, '2016-02-25 19:22:19', 0),
('5357fc84-afe9-616d-e9bd-56cf52d785d8', '55effc16-809c-3ff4-f464-56cf528b4049', 'e288e39e-c594-4b2a-2053-56cdec35ad38', 0, '2016-02-25 19:22:19', 0),
('5357fe0d-c51a-22e6-c864-56cf52cf7356', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1ecaafe-21b0-35b7-840f-56cdec8a90cc', 0, '2016-02-25 19:22:19', 0),
('539678cf-5ca4-52b2-483d-56cf52bb1f41', '55effc16-809c-3ff4-f464-56cf528b4049', 'e288eedf-37da-cc95-25a8-56cdec9b60a7', 0, '2016-02-25 19:22:19', 0),
('53d4fc51-15d9-042b-87b2-56cf529c3065', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1eca01c-122c-21af-6d20-56cdec7c1ae4', 0, '2016-02-25 19:22:19', 0),
('53d4fd19-d583-457c-aad8-56cf52172e0f', '55effc16-809c-3ff4-f464-56cf528b4049', 'e3252f6e-fa05-fcad-4b0a-56cdec2cb0ce', 0, '2016-02-25 19:22:19', 0),
('5413814b-3e38-b919-9dcc-56cf52b59e56', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1506e58-17e1-437b-25ac-56cdec0f9c2c', 0, '2016-02-25 19:22:19', 0),
('5452003c-8046-6c07-6c26-56cf522d353c', '55effc16-809c-3ff4-f464-56cf528b4049', '86fb06a2-9e3e-8847-7bde-56cdecdd001b', -98, '2016-02-25 19:22:19', 0),
('5490850c-1142-bba5-0fb3-56cf52cf33d7', '55effc16-809c-3ff4-f464-56cf528b4049', '88338565-c80c-c96f-0d2a-56cdec96bcee', 0, '2016-02-25 19:22:19', 0),
('54cf07fa-b2d6-a863-9cdc-56cf529013bd', '55effc16-809c-3ff4-f464-56cf528b4049', '87974d95-9641-0845-1b34-56cdecc0c155', 0, '2016-02-25 19:22:19', 0),
('550d841e-3fe9-34f8-8d0e-56cf520af165', '55effc16-809c-3ff4-f464-56cf528b4049', '88cfcd98-2984-fe4b-4bd3-56cdec5b83d0', 0, '2016-02-25 19:22:19', 0),
('55460572-3e09-9a68-b55b-56cf4f57700c', '85f6e014-4622-795d-bde1-56cf4a829665', '6cbc22f5-7a2f-4e90-3165-56cdec1896b7', -98, '2016-02-25 19:03:12', 0),
('55460a7f-e8ac-3cbf-efbf-56cf4f0cdc51', '85f6e014-4622-795d-bde1-56cf4a829665', '62f8295c-c54e-8af3-d827-56cdec5d9852', 0, '2016-02-25 19:03:12', 0),
('554c0c1c-42aa-65c6-f1f3-56cf52e61918', '55effc16-809c-3ff4-f464-56cf528b4049', '88338e1e-725f-1999-7426-56cdece13725', 0, '2016-02-25 19:22:19', 0),
('558a83ec-abef-2c5b-8908-56cf529c7e2b', '55effc16-809c-3ff4-f464-56cf528b4049', '8797447a-0e90-4df7-f8bf-56cdec706357', 0, '2016-02-25 19:22:19', 0),
('558a8aec-b101-4d40-229e-56cf52cdca89', '55effc16-809c-3ff4-f464-56cf528b4049', '88cfc781-5782-a5dd-6f44-56cdec7729d2', 0, '2016-02-25 19:22:19', 0),
('55c90a78-6355-447b-c554-56cf521c1654', '55effc16-809c-3ff4-f464-56cf528b4049', '86fb0d3d-47d0-a74b-d5ac-56cdec657b74', 0, '2016-02-25 19:22:19', 0),
('56078009-a96c-6cad-caed-56cf525d4a84', '55effc16-809c-3ff4-f464-56cf528b4049', 'e0b32578-0066-4644-fa90-56cdec75394c', 89, '2016-02-25 19:22:19', 0),
('5646034a-08d9-cc61-f8fd-56cf529aac0b', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1eba702-72ea-a559-f5ad-56cdec52097d', 0, '2016-02-25 19:22:19', 0),
('56460fc0-d451-6230-7a79-56cf525310b5', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1eba0de-8968-1b9c-d799-56cdecd2d7ee', 0, '2016-02-25 19:22:19', 0),
('56848781-d975-61bd-7150-56cf5240ebb6', '55effc16-809c-3ff4-f464-56cf528b4049', 'e1eba26c-36af-481d-087d-56cdec0e733a', 0, '2016-02-25 19:22:19', 0),
('568487d2-777f-fc31-bdb6-56cf520dceb7', '55effc16-809c-3ff4-f464-56cf528b4049', 'e287e328-68b8-4d89-86a5-56cdecd9a49f', 0, '2016-02-25 19:22:19', 0),
('56c30ab3-c1e6-5251-d96e-56cf5227701c', '55effc16-809c-3ff4-f464-56cf528b4049', 'e14f62f8-917e-164a-7b57-56cdeccdee72', 0, '2016-02-25 19:22:19', 0),
('57018038-d934-0bc4-3e50-56cf52822dbf', '55effc16-809c-3ff4-f464-56cf528b4049', 'e287e56d-45fd-70aa-9afe-56cdec72ea3c', 0, '2016-02-25 19:22:19', 0),
('570188ea-02c6-ae7a-84e7-56cf52f568c7', '55effc16-809c-3ff4-f464-56cf528b4049', 'e14f6cb1-fe3a-4bab-730a-56cdec435f9c', 0, '2016-02-25 19:22:19', 0),
('57400c39-619e-a3e6-5a44-56cf5288583f', '55effc16-809c-3ff4-f464-56cf528b4049', '1fd4471d-8cbb-5ccb-d584-56cdecd557fa', 0, '2016-02-25 19:22:19', 0),
('57400d07-d9fa-68bd-a8be-56cf524d289b', '55effc16-809c-3ff4-f464-56cf528b4049', '1f380b24-e398-3dfe-6a32-56cdec6af30f', -98, '2016-02-25 19:22:19', 0),
('577e8aa6-40ae-53f0-27e9-56cf5254497a', '55effc16-809c-3ff4-f464-56cf528b4049', '2070a78f-b40c-1737-a5f2-56cdecc438a4', 0, '2016-02-25 19:22:19', 0),
('577e8b33-a3ed-9c67-a0dd-56cf5241c080', '55effc16-809c-3ff4-f464-56cf528b4049', '1fd44c1f-5607-4bb9-997c-56cdecb26b5c', 0, '2016-02-25 19:22:19', 0),
('57bd0b9a-913c-7f47-804a-56cf52f171d3', '55effc16-809c-3ff4-f464-56cf528b4049', '2070afcc-be95-e14b-d080-56cdec232675', 0, '2016-02-25 19:22:19', 0),
('57fb8114-401d-3d29-0710-56cf52faa492', '55effc16-809c-3ff4-f464-56cf528b4049', '2070a70f-c9c9-ab77-c082-56cdece04ff7', 0, '2016-02-25 19:22:19', 0),
('57fb8be1-cad2-8b31-7ea7-56cf52a03603', '55effc16-809c-3ff4-f464-56cf528b4049', '1f3809f6-3f4b-976c-9082-56cdec06ec59', 0, '2016-02-25 19:22:19', 0),
('583a12ac-91b0-9de4-53da-56cf522c8554', '55effc16-809c-3ff4-f464-56cf528b4049', '1f380c32-6f35-9c96-7981-56cdec3ab498', 0, '2016-02-25 19:22:19', 0),
('583a1c4f-0092-731e-a075-56cf52e4d608', '55effc16-809c-3ff4-f464-56cf528b4049', '7b623381-cad8-8d7a-818b-56cdec046596', 89, '2016-02-25 19:22:19', 0),
('587891ed-7872-43c0-43c1-56cf5221c919', '55effc16-809c-3ff4-f464-56cf528b4049', '7bfe63ab-0e84-0127-b9f4-56cdec9295a2', 0, '2016-02-25 19:22:19', 0),
('58789db6-91fc-5031-d527-56cf5246c78e', '55effc16-809c-3ff4-f464-56cf528b4049', '7c9ababb-1318-a321-18b5-56cdec7d2e9c', 0, '2016-02-25 19:22:19', 0),
('58b71c7b-a0ca-a0e9-4e18-56cf52a86d0e', '55effc16-809c-3ff4-f464-56cf528b4049', '7d36f412-d040-8c97-8b71-56cdecd0d604', 0, '2016-02-25 19:22:19', 0),
('58f59857-da41-3348-49d5-56cf52bd36a6', '55effc16-809c-3ff4-f464-56cf528b4049', '7bfe6b78-374d-6105-6777-56cdecded90d', 0, '2016-02-25 19:22:19', 0),
('58f59abd-d1e8-a1b8-08ff-56cf52362192', '55effc16-809c-3ff4-f464-56cf528b4049', '7c9abce8-46cd-915e-166c-56cdecca8263', 0, '2016-02-25 19:22:19', 0),
('592e021d-8cee-312c-dfc8-56cf4f85a35f', '85f6e014-4622-795d-bde1-56cf4a829665', '6d5860ec-f1f6-c318-bd2e-56cdecf7f1eb', 0, '2016-02-25 19:03:12', 0),
('5934127c-9f72-539c-37f1-56cf52d0909a', '55effc16-809c-3ff4-f464-56cf528b4049', '7d36f6ea-fe4f-ce5b-a5c7-56cdec262879', 0, '2016-02-25 19:22:19', 0),
('59341d14-66f9-93dc-bffa-56cf528b159c', '55effc16-809c-3ff4-f464-56cf528b4049', '7bfe6725-ea46-5799-b738-56cdec39e037', 0, '2016-02-25 19:22:19', 0),
('59729288-f9d4-e48b-b659-56cf529f4f32', '55effc16-809c-3ff4-f464-56cf528b4049', '304f30cf-87d1-c037-7d60-56cdec576eda', -98, '2016-02-25 19:22:19', 0),
('597298e5-e36c-7a11-3da6-56cf52270522', '55effc16-809c-3ff4-f464-56cf528b4049', '30eb783c-9d05-33df-b503-56cdeca5349a', 0, '2016-02-25 19:22:19', 0),
('59b11d7e-16da-9c40-379d-56cf52ec406d', '55effc16-809c-3ff4-f464-56cf528b4049', '30eb7b8d-a6ca-567d-f4b6-56cdec01bc25', 0, '2016-02-25 19:22:19', 0),
('59ef983f-5974-92bf-9820-56cf52a114e6', '55effc16-809c-3ff4-f464-56cf528b4049', '3187bdfd-73e6-159e-a382-56cdecc9fd90', 0, '2016-02-25 19:22:19', 0),
('59ef9d70-f1d6-0219-a376-56cf52ebdb78', '55effc16-809c-3ff4-f464-56cf528b4049', '3187bbfc-1742-bd35-bbb1-56cdecdac7a0', 0, '2016-02-25 19:22:19', 0),
('5a2e12dc-734b-352f-2859-56cf52d8d1e3', '55effc16-809c-3ff4-f464-56cf528b4049', '3187bd0c-33b2-ed29-a61d-56cdeca594e8', 0, '2016-02-25 19:22:19', 0),
('5a2e1fc1-7da5-90f1-83f8-56cf5279d5c6', '55effc16-809c-3ff4-f464-56cf528b4049', '304f382d-f9f7-2448-b7ce-56cdece78a77', 0, '2016-02-25 19:22:19', 0),
('5a6c9ae0-6dc3-a558-ae0c-56cf52e0c94a', '55effc16-809c-3ff4-f464-56cf528b4049', '7372f9c8-8489-9a4b-8955-56cdec03f542', 89, '2016-02-25 19:22:19', 0),
('5a6c9eef-29a2-6c95-1a0b-56cf529a244b', '55effc16-809c-3ff4-f464-56cf528b4049', '304f3ad2-a50f-737e-9249-56cdeccb5054', 0, '2016-02-25 19:22:19', 0),
('5aab1710-39fc-1974-7df9-56cf526889eb', '55effc16-809c-3ff4-f464-56cf528b4049', '74ab7213-b309-c53d-48e1-56cdec683548', 0, '2016-02-25 19:22:19', 0),
('5ae995c2-d220-2e29-c7a6-56cf52281449', '55effc16-809c-3ff4-f464-56cf528b4049', '7547bbf9-bfe0-3c3c-05ae-56cdec37b9ae', 0, '2016-02-25 19:22:19', 0),
('5ae9978f-5a78-0843-a3c3-56cf520462a8', '55effc16-809c-3ff4-f464-56cf528b4049', '74ab7acb-68ac-e950-4a8e-56cdecd118a6', 0, '2016-02-25 19:22:19', 0),
('5b28141a-1c0c-b12d-df68-56cf52fa1d91', '55effc16-809c-3ff4-f464-56cf528b4049', '74ab75d9-4028-9914-de0c-56cdec48f0d6', 0, '2016-02-25 19:22:19', 0),
('5b281494-a0e8-36ce-8624-56cf5256f1e3', '55effc16-809c-3ff4-f464-56cf528b4049', '740f39cb-a788-caa1-70ce-56cdec0a0ecd', 0, '2016-02-25 19:22:19', 0),
('5b669099-cb4a-6fb5-2af5-56cf52245e4f', '55effc16-809c-3ff4-f464-56cf528b4049', '7547be18-77e1-e56c-05a9-56cdec9bef04', 0, '2016-02-25 19:22:19', 0),
('5ba51020-b5f1-8847-f680-56cf52b0fdec', '55effc16-809c-3ff4-f464-56cf528b4049', '740f3c61-8fe5-c695-ffe1-56cdec5ba0a1', 0, '2016-02-25 19:22:19', 0),
('5ba5179e-3d7c-b8e1-2c0f-56cf52fc5372', '55effc16-809c-3ff4-f464-56cf528b4049', '62f82b7b-4792-15a8-bf69-56cdec585597', -98, '2016-02-25 19:22:19', 0),
('5be3946b-d0be-5bfa-c7de-56cf52a17bbe', '55effc16-809c-3ff4-f464-56cf528b4049', '63946767-0fda-3b44-6e6e-56cdecc3696a', 0, '2016-02-25 19:22:19', 0),
('5be39d47-9be8-356b-7630-56cf524e58ec', '55effc16-809c-3ff4-f464-56cf528b4049', '6394693d-9e98-501b-751e-56cdecee05cb', 0, '2016-02-25 19:22:19', 0),
('5c221178-4a8a-14aa-4800-56cf524002b8', '55effc16-809c-3ff4-f464-56cf528b4049', '6430ac46-3c8f-b466-ea73-56cdecd0bcf7', 0, '2016-02-25 19:22:19', 0),
('5c609224-aa4a-73ad-8a6f-56cf522a25b9', '55effc16-809c-3ff4-f464-56cf528b4049', '6430ab5e-cb6b-4a78-9de1-56cdecfebdb7', 0, '2016-02-25 19:22:19', 0),
('5c609cb4-cf21-394b-4c98-56cf5223cfab', '55effc16-809c-3ff4-f464-56cf528b4049', '62f8229b-c83f-c6a1-654a-56cdecd2033e', 0, '2016-02-25 19:22:19', 0),
('5c9f27b7-7f20-a042-744e-56cf5216d229', '55effc16-809c-3ff4-f464-56cf528b4049', '62f8295c-c54e-8af3-d827-56cdec5d9852', 0, '2016-02-25 19:22:19', 0),
('5c9f2bfc-a124-e20f-a1ae-56cf5217c527', '55effc16-809c-3ff4-f464-56cf528b4049', '6430a33a-a196-e742-efb4-56cdecf1eb53', 0, '2016-02-25 19:22:19', 0),
('5cdda6be-ed1e-3a74-f21c-56cf52acdd80', '55effc16-809c-3ff4-f464-56cf528b4049', '6cbc22f5-7a2f-4e90-3165-56cdec1896b7', -98, '2016-02-25 19:22:19', 0),
('5d16074c-9756-3d5e-5684-56cf4faa177c', '85f6e014-4622-795d-bde1-56cf4a829665', '6d58677b-5905-bdb7-8895-56cdec11c6b1', 0, '2016-02-25 19:03:12', 0),
('5d1607c6-fbd5-abbf-2a2e-56cf4f0cf1df', '85f6e014-4622-795d-bde1-56cf4a829665', '6df4a3a3-2995-3d7b-ccee-56cdecf9ca3b', 0, '2016-02-25 19:03:12', 0),
('5d1c2591-9fa8-e6ba-9704-56cf529ef21c', '55effc16-809c-3ff4-f464-56cf528b4049', '6d5860ec-f1f6-c318-bd2e-56cdecf7f1eb', 0, '2016-02-25 19:22:19', 0),
('5d1c2e61-34c9-2e61-7754-56cf526f5774', '55effc16-809c-3ff4-f464-56cf528b4049', '6d58677b-5905-bdb7-8895-56cdec11c6b1', 0, '2016-02-25 19:22:19', 0),
('5d5aa375-71e7-f767-ecee-56cf524c31a6', '55effc16-809c-3ff4-f464-56cf528b4049', '6df4a3a3-2995-3d7b-ccee-56cdecf9ca3b', 0, '2016-02-25 19:22:19', 0),
('5d5aa97a-ab27-1ed4-4ea7-56cf52c8153a', '55effc16-809c-3ff4-f464-56cf528b4049', '6df4a567-ad97-5790-63d3-56cdecb17770', 0, '2016-02-25 19:22:19', 0),
('5d9926fa-b941-7d96-cc8f-56cf5200dc3c', '55effc16-809c-3ff4-f464-56cf528b4049', '6df4ac58-4604-b27a-029c-56cdec973aa5', 0, '2016-02-25 19:22:19', 0),
('5d992bb1-6b34-a8e1-3726-56cf5240164b', '55effc16-809c-3ff4-f464-56cf528b4049', '6d586c6f-e01b-aba4-5070-56cdec3432ed', 0, '2016-02-25 19:22:19', 0),
('5dd7acb0-fb00-020c-c5ad-56cf52a5cc2a', '55effc16-809c-3ff4-f464-56cf528b4049', '6cbc2b1f-00f3-fe68-f7db-56cdec812648', 0, '2016-02-25 19:22:19', 0),
('5e16257d-80d3-749d-935f-56cf52b7f3f9', '55effc16-809c-3ff4-f464-56cf528b4049', '32480a66-369c-b9f3-37c4-56cdec67a037', -98, '2016-02-25 19:22:19', 0),
('5e162978-0123-6078-4516-56cf5281e7a8', '55effc16-809c-3ff4-f464-56cf528b4049', '45d00526-254f-d1ed-d381-56cdece7c5ca', 0, '2016-02-25 19:22:19', 0),
('5e54afa4-125c-a5b9-3ac3-56cf52b6595e', '55effc16-809c-3ff4-f464-56cf528b4049', '3c0c0cdf-edc7-db23-2bed-56cdeca05ddc', 0, '2016-02-25 19:22:19', 0),
('5e9321c0-999c-aa54-7efc-56cf5208fc97', '55effc16-809c-3ff4-f464-56cf528b4049', '45d00440-85b7-a568-9be1-56cdecaf1630', 0, '2016-02-25 19:22:19', 0),
('5e932976-f8b7-c568-1da5-56cf52cd2d52', '55effc16-809c-3ff4-f464-56cf528b4049', '45d00175-dabc-5afb-12aa-56cdeca2a2f9', 0, '2016-02-25 19:22:19', 0),
('5ed1a062-97b5-8c2c-7789-56cf52924920', '55effc16-809c-3ff4-f464-56cf528b4049', '4f9401f8-ffe0-dea4-b1c8-56cdec1f9371', 0, '2016-02-25 19:22:19', 0),
('5ed1acd7-2ce8-e085-f583-56cf5225363d', '55effc16-809c-3ff4-f464-56cf528b4049', '3c0c0e50-e656-4a31-431c-56cdecb5d3ce', 0, '2016-02-25 19:22:19', 0),
('5f10227f-00f3-a95a-6ace-56cf524e34e6', '55effc16-809c-3ff4-f464-56cf528b4049', '324802fb-647c-25e9-31fa-56cdec8595a0', 0, '2016-02-25 19:22:19', 0),
('5f4ea094-a3e7-0478-fe1b-56cf52be1e34', '55effc16-809c-3ff4-f464-56cf528b4049', '27c3af65-f8b3-f474-db93-56cdec82403d', -98, '2016-02-25 19:22:19', 0),
('5f4ea330-f31d-2dd7-5c82-56cf52e12bd0', '55effc16-809c-3ff4-f464-56cf528b4049', '28fc2df4-b650-8fea-c43d-56cdec731309', 0, '2016-02-25 19:22:19', 0),
('5f8d27c6-66b2-1984-5213-56cf526d8019', '55effc16-809c-3ff4-f464-56cf528b4049', '28fc27f9-9adf-75d2-e441-56cdece77527', 0, '2016-02-25 19:22:19', 0),
('5fcbabb5-20fb-c32e-b55c-56cf521de140', '55effc16-809c-3ff4-f464-56cf528b4049', '28fc23e1-d4d9-6fc3-ccc0-56cdec490bf9', 0, '2016-02-25 19:22:19', 0),
('5fcbaf2b-8233-93e9-f718-56cf525307e4', '55effc16-809c-3ff4-f464-56cf528b4049', '29986b57-1968-52e4-ba83-56cdeca4a423', 0, '2016-02-25 19:22:19', 0),
('600a2cf6-bb74-5cf7-8def-56cf52bbd8cf', '55effc16-809c-3ff4-f464-56cf528b4049', '29986c60-e3d6-6515-9d8c-56cdec824907', 0, '2016-02-25 19:22:19', 0),
('600a2ee4-dea3-53a2-2df1-56cf5285c92f', '55effc16-809c-3ff4-f464-56cf528b4049', '285fee4b-2ddc-7359-ba0b-56cdec8e730f', 0, '2016-02-25 19:22:19', 0),
('6048a46f-4632-9a06-aca9-56cf526f5ebe', '55effc16-809c-3ff4-f464-56cf528b4049', '285fe893-829f-b7a4-a583-56cdec73f288', 0, '2016-02-25 19:22:19', 0),
('608722c0-5b32-3b93-e3a9-56cf525bae96', '55effc16-809c-3ff4-f464-56cf528b4049', '90bf17fb-6828-4428-d5e5-56cdecf34e6a', -98, '2016-02-25 19:22:19', 0),
('60872f95-7093-4f57-e779-56cf525bfd1e', '55effc16-809c-3ff4-f464-56cf528b4049', '91f796f2-156f-8bfb-9113-56cdeca68acc', 0, '2016-02-25 19:22:19', 0),
('60c5bb06-dd6e-4b4d-e4b6-56cf520c9dad', '55effc16-809c-3ff4-f464-56cf528b4049', '915b565f-cf3e-1886-8215-56cdec12693c', 0, '2016-02-25 19:22:19', 0),
('60ff0155-fd36-e361-8834-56cf4f072182', '85f6e014-4622-795d-bde1-56cf4a829665', '6df4a567-ad97-5790-63d3-56cdecb17770', 0, '2016-02-25 19:03:12', 0),
('60ff0444-6b8b-3409-febf-56cf4f9265b7', '85f6e014-4622-795d-bde1-56cf4a829665', '6d586c6f-e01b-aba4-5070-56cdec3432ed', 0, '2016-02-25 19:03:12', 0),
('61043c07-c1c3-34fe-a2c7-56cf52ae4787', '55effc16-809c-3ff4-f464-56cf528b4049', '91f79a4a-2720-1170-d8e7-56cdec6f9022', 0, '2016-02-25 19:22:19', 0),
('61043ec4-9394-17e9-7537-56cf52d68465', '55effc16-809c-3ff4-f464-56cf528b4049', '9293d41e-f105-cead-f873-56cdec8df346', 0, '2016-02-25 19:22:19', 0),
('6142b4d7-eb9b-bad6-c5ab-56cf5244a10c', '55effc16-809c-3ff4-f464-56cf528b4049', '915b52ef-3944-7936-0b0e-56cdec8b743a', 0, '2016-02-25 19:22:19', 0);
INSERT INTO `acl_roles_actions` (`id`, `role_id`, `action_id`, `access_override`, `date_modified`, `deleted`) VALUES
('6142beb9-066e-8b22-91b1-56cf527ee5e0', '55effc16-809c-3ff4-f464-56cf528b4049', '9293de9f-2f65-a7c1-dac5-56cdec9a96a5', 0, '2016-02-25 19:22:19', 0),
('61813223-1e97-684b-10f7-56cf521d5fe7', '55effc16-809c-3ff4-f464-56cf528b4049', '90bf169b-1ea3-b5cf-b3c2-56cdec5257ff', 0, '2016-02-25 19:22:19', 0),
('61bfb252-97fe-5a92-de3e-56cf52f048b3', '55effc16-809c-3ff4-f464-56cf528b4049', '994a959e-c38d-05df-e9d0-56cdec8259db', 89, '2016-02-25 19:22:19', 0),
('61bfbfea-4a0a-2c51-59ce-56cf527201ee', '55effc16-809c-3ff4-f464-56cf528b4049', '9a8313f7-a165-62a3-f6a5-56cdec303cd7', 0, '2016-02-25 19:22:19', 0),
('61fe38e0-4327-511e-8cbb-56cf520c2539', '55effc16-809c-3ff4-f464-56cf528b4049', '9a8316ce-b8e2-4c44-59d3-56cdeccfe6b9', 0, '2016-02-25 19:22:19', 0),
('623cb44a-89b2-0dbf-bee7-56cf5260fdae', '55effc16-809c-3ff4-f464-56cf528b4049', '9a831103-6445-c601-8931-56cdec4da2e7', 0, '2016-02-25 19:22:19', 0),
('623cbf87-7f84-39c3-3ec4-56cf52560140', '55effc16-809c-3ff4-f464-56cf528b4049', '9b1f562d-5de9-2609-254e-56cdec5443d1', 0, '2016-02-25 19:22:19', 0),
('627b384e-e0c2-76e5-3978-56cf52a5683d', '55effc16-809c-3ff4-f464-56cf528b4049', '99e6da36-ca46-28f6-f1e4-56cdecd6f1dd', 0, '2016-02-25 19:22:19', 0),
('627b3cce-3d46-9d77-fe31-56cf529c8ae3', '55effc16-809c-3ff4-f464-56cf528b4049', '9b1f50c5-2826-e023-1491-56cdec27b9f5', 0, '2016-02-25 19:22:19', 0),
('62b9b5da-08ae-e7dd-376a-56cf52812dca', '55effc16-809c-3ff4-f464-56cf528b4049', 'a77806cd-4d6b-bad0-b455-56cdec8cbb90', -98, '2016-02-25 19:22:19', 0),
('62b9bc16-e844-aa16-10e1-56cf52efcd34', '55effc16-809c-3ff4-f464-56cf528b4049', '99e6d4e5-cf83-331b-1b19-56cdec3e3eeb', 0, '2016-02-25 19:22:19', 0),
('62f83152-faaa-d9b4-3b8b-56cf520d856b', '55effc16-809c-3ff4-f464-56cf528b4049', 'bb000bd2-c5ff-2522-802d-56cdec74f84c', 0, '2016-02-25 19:22:19', 0),
('6336b6ae-6bb1-56e4-3c21-56cf52f0d09c', '55effc16-809c-3ff4-f464-56cf528b4049', 'bb000eb1-49b9-0800-baf8-56cdec19c21f', 0, '2016-02-25 19:22:19', 0),
('6336b91a-93c8-57d0-b08d-56cf527c3e46', '55effc16-809c-3ff4-f464-56cf528b4049', 'b13c091e-2d88-078a-5b6a-56cdec4d8d4c', 0, '2016-02-25 19:22:19', 0),
('63753598-3e6e-5c55-684e-56cf52bc8097', '55effc16-809c-3ff4-f464-56cf528b4049', 'bb000781-30ec-0bca-d199-56cdec0ff3cc', 0, '2016-02-25 19:22:19', 0),
('6375377d-aefd-c5d7-d504-56cf5219f1a9', '55effc16-809c-3ff4-f464-56cf528b4049', 'b13c026c-8f9f-1d3d-f50d-56cdecef0f6e', 0, '2016-02-25 19:22:19', 0),
('63b3bb5f-88b4-641f-2fe2-56cf52baf13d', '55effc16-809c-3ff4-f464-56cf528b4049', 'c4c40319-10b7-08fb-9deb-56cdec41d432', 0, '2016-02-25 19:22:19', 0),
('63f23f96-2b63-e58d-5e22-56cf52b3b4a5', '55effc16-809c-3ff4-f464-56cf528b4049', 'b13c01f3-727f-a8ac-3975-56cdec6b8327', 0, '2016-02-25 19:22:19', 0),
('64e70689-c2b1-76c4-bfd5-56cf4fbced48', '85f6e014-4622-795d-bde1-56cf4a829665', '6df4ac58-4604-b27a-029c-56cdec973aa5', 0, '2016-02-25 19:03:12', 0),
('68cf00a6-5d0a-8a7d-62bc-56cf4f15b715', '85f6e014-4622-795d-bde1-56cf4a829665', '6cbc2b1f-00f3-fe68-f7db-56cdec812648', 0, '2016-02-25 19:03:12', 0),
('68cf09cb-4655-a739-ed52-56cf4ffffe1c', '85f6e014-4622-795d-bde1-56cf4a829665', '32480a66-369c-b9f3-37c4-56cdec67a037', -98, '2016-02-25 19:03:12', 0),
('6cb70485-71cf-9c6d-94fb-56cf4ff3d326', '85f6e014-4622-795d-bde1-56cf4a829665', '45d00526-254f-d1ed-d381-56cdece7c5ca', 0, '2016-02-25 19:03:12', 0),
('6cb70e58-858a-7bbc-ee76-56cf4f86eba6', '85f6e014-4622-795d-bde1-56cf4a829665', '3c0c0cdf-edc7-db23-2bed-56cdeca05ddc', 0, '2016-02-25 19:03:12', 0),
('709f0fda-26da-6bc5-955c-56cf4f5a9dfa', '85f6e014-4622-795d-bde1-56cf4a829665', '45d00175-dabc-5afb-12aa-56cdeca2a2f9', 0, '2016-02-25 19:03:12', 0),
('7250001b-f22d-823f-396f-56cf4ff2e73f', '85f6e014-4622-795d-bde1-56cf4a829665', '7c9abce8-46cd-915e-166c-56cdecca8263', 0, '2016-02-25 19:03:12', 0),
('74870532-a077-6224-5916-56cf4f07f5f5', '85f6e014-4622-795d-bde1-56cf4a829665', '45d00440-85b7-a568-9be1-56cdecaf1630', 0, '2016-02-25 19:03:12', 0),
('74870ed6-69bc-6aa5-0048-56cf4f2cc262', '85f6e014-4622-795d-bde1-56cf4a829665', '3c0c0e50-e656-4a31-431c-56cdecb5d3ce', 0, '2016-02-25 19:03:12', 0),
('786f0457-6b79-3b7a-8a12-56cf4fa9ef3d', '85f6e014-4622-795d-bde1-56cf4a829665', '4f9401f8-ffe0-dea4-b1c8-56cdec1f9371', 0, '2016-02-25 19:03:12', 0),
('7b6d702a-dec2-b766-8c40-56cee3e36b2a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '16ac9f31-61dd-9c39-297c-56cdec83d86a', 0, '2016-02-26 10:53:44', 0),
('7b6d708c-6443-7205-15f8-56cee3425650', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '1574161f-a535-97bf-1027-56cdec1f47df', 0, '2016-02-26 10:53:44', 0),
('7b6d73f1-ee1a-6ada-3d94-56cee3b2d9fa', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '15741114-c0ba-f279-f588-56cdec6c559e', 0, '2016-02-26 10:53:44', 0),
('7b6d748a-7619-e541-00b1-56cee38405d2', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '14d7d43b-77b6-8a09-9a24-56cdec1003fc', 89, '2016-02-26 10:53:44', 0),
('7b6d7832-3da3-f015-fa4c-56cee3e0d976', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '161055c3-53ad-2977-e398-56cdeca68276', 0, '2016-02-26 10:53:44', 0),
('7b6d7922-f237-862c-48ef-56cee36fbefd', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '16105372-05d8-6637-957f-56cdec995194', 0, '2016-02-26 10:53:44', 0),
('7b6d7d86-d62e-74e3-7955-56cee3934067', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '16105230-442a-bc29-a26c-56cdec269d7b', 0, '2016-02-26 10:53:44', 0),
('7c570483-0510-6d5a-7368-56cf4fadb637', '85f6e014-4622-795d-bde1-56cf4a829665', '324802fb-647c-25e9-31fa-56cdec8595a0', 0, '2016-02-25 19:03:12', 0),
('7c5709fc-2257-e883-479e-56cf4feacf3b', '85f6e014-4622-795d-bde1-56cf4a829665', '27c3af65-f8b3-f474-db93-56cdec82403d', -98, '2016-02-25 19:03:12', 0),
('7f3c7292-9f4d-d700-8220-56cee39ba38d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4b2a46b0-2eb6-74a5-093c-56cdec3750e0', 0, '2016-02-26 10:53:44', 0),
('7f3c72a5-4630-40b8-baad-56cee3155d12', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '49f1ce69-e8f3-e233-b782-56cdec21e0b1', 89, '2016-02-26 10:53:44', 0),
('7f3c73e6-142d-1911-5771-56cee3179a88', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'c9819733-4303-cb23-3a72-56cdec805c84', 0, '2016-02-26 10:53:44', 0),
('7f3c744b-1c80-bf53-84df-56cee30aed2d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'c8e55289-8bc4-4192-6441-56cdec1ffb83', 0, '2016-02-26 10:53:44', 0),
('7f3c754e-b4b0-42a9-9646-56cee34a52d5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4a8e0639-626b-12ce-08fe-56cdec954e44', 0, '2016-02-26 10:53:44', 0),
('7f3c75e1-5d94-c105-e8b9-56cee3b831bd', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd6ef256b-9418-e055-298a-56cdec9c3813', 0, '2016-02-26 10:53:44', 0),
('7f3c7654-2779-62fa-28c6-56cee34f2e0f', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'ca1dd3c9-9c4b-16d1-767f-56cdec8731fb', 0, '2016-02-26 10:53:44', 0),
('7f3c76b4-5125-9aff-1356-56cee3cba99d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'ca1dd328-840e-1e2d-f0f7-56cdec69ae4f', 0, '2016-02-26 10:53:44', 0),
('7f3c7777-3d6e-1d89-07d1-56cee3d3d33a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4bc68a82-35cc-f64a-0885-56cdec753737', 0, '2016-02-26 10:53:44', 0),
('7f3c7882-c8b0-aae1-25b0-56cee385b8ef', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd652e639-4b43-72a3-6628-56cdecf18c0c', 0, '2016-02-26 10:53:44', 0),
('7f3c78b8-bdad-0fc1-fc60-56cee3f98cab', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'c981995d-8db2-0f62-0f99-56cdeccabcd0', 0, '2016-02-26 10:53:44', 0),
('7f3c7913-063c-055c-e143-56cee338834a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'c8e55aba-4b03-83da-b59b-56cdec0a88f1', 89, '2016-02-26 10:53:44', 0),
('7f3c7a0f-ae2b-b7cf-1591-56cee38eb076', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'caba15d7-bf6f-5958-d81d-56cdec956a8a', 0, '2016-02-26 10:53:44', 0),
('7f3c7a33-673a-ac84-416c-56cee32621d8', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd5b6a7b4-4694-2e88-c790-56cdec370884', -98, '2016-02-26 10:53:44', 0),
('7f3c7a3c-764a-9a03-6101-56cee3063771', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd652ee7e-fc21-18e6-19ab-56cdec5f692b', 0, '2016-02-26 10:53:44', 0),
('7f3c7c39-89be-5a6d-d9a9-56cee37a1e45', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd827a740-6cd2-ffb0-c481-56cdecd4fa85', 0, '2016-02-26 10:53:44', 0),
('7f3c7cea-d36b-ac9d-10ee-56cee370b6d4', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd78b69ca-5084-d4a2-d59d-56cdec23ebbf', 0, '2016-02-26 10:53:44', 0),
('7f3c7e6d-cbf2-63ce-ac71-56cee3d5beb5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd78b6b83-d047-3d4d-cb9d-56cdecd9c661', 0, '2016-02-26 10:53:44', 0),
('7f3c7f0f-e057-235d-bc6a-56cee3fd614b', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '14d7d88a-fa01-441c-9662-56cdeca4425c', 0, '2016-02-26 10:53:44', 0),
('7f3c7f17-1db2-06a8-8e7d-56cee327b7ac', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'd6ef2dec-b837-67fa-4eeb-56cdecf90424', 0, '2016-02-26 10:53:44', 0),
('7f3c7f60-1155-602a-64f3-56cee3639867', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'caba132b-2823-c890-27d2-56cdecd4f7d9', 0, '2016-02-26 10:53:44', 0),
('830b7049-f55b-f28b-32fa-56cee334f18a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'be85f05a-05c0-2a69-3c75-56cdecba2645', 0, '2016-02-26 10:53:44', 0),
('830b70ee-4c7b-4174-6130-56cee31cbfe0', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4b2a421a-be3d-707e-72de-56cdec430fd7', 0, '2016-02-26 10:53:44', 0),
('830b7156-7238-40be-6e4e-56cee3e24f27', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '57fb9ba5-7b0a-93fd-b4f4-56cdecee24bc', 0, '2016-02-26 10:53:44', 0),
('830b7174-7ed1-daf7-4b7b-56cee3f1f2f6', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bde9b8a6-c214-96ba-40a1-56cdec3419ef', 0, '2016-02-26 10:53:44', 0),
('830b7251-55cc-b995-d696-56cee3d5c5ea', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '57fb9ccb-84f3-d718-856f-56cdec603da9', 0, '2016-02-26 10:53:44', 0),
('830b7268-e703-7977-40c4-56cee35cf89a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'be85f260-ec1c-19b4-b81b-56cdeccd7955', 0, '2016-02-26 10:53:44', 0),
('830b7338-468e-1096-da82-56cee34a92b6', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bcb133f6-2022-85f7-2045-56cdece12226', -98, '2016-02-26 10:53:44', 0),
('830b734b-274e-c8f0-7d1e-56cee3360275', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1506ec4-5d5e-2550-998f-56cdec917e71', -98, '2016-02-26 10:53:44', 0),
('830b7364-9261-1adf-7b0c-56cee3619ad1', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bd4d771c-7a3c-7478-2a64-56cdec83b099', 0, '2016-02-26 10:53:44', 0),
('830b7533-5e52-e3e1-04f8-56cee3341335', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '575f5bbe-ff39-3f83-c3ee-56cdec0527d4', 0, '2016-02-26 10:53:44', 0),
('830b760b-85dd-61f9-27c9-56cee3917634', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4a8e0e6f-521b-6725-2f03-56cdec16693f', 0, '2016-02-26 10:53:44', 0),
('830b76a0-737a-82fb-17c0-56cee3843de5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '5897de46-e3f6-f0ab-3dcb-56cdec4c4391', 0, '2016-02-26 10:53:44', 0),
('830b76a1-1808-dfcd-5c1e-56cee30f64ef', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '56c3176d-c92d-a626-64d0-56cdec1ba43c', 89, '2016-02-26 10:53:44', 0),
('830b76ec-5a10-ca6a-3e06-56cee31c4b38', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '57fb9077-0dc4-09c2-54c1-56cdec2fcb4c', 0, '2016-02-26 10:53:44', 0),
('830b771a-c3e2-332e-2fe7-56cee3605d4a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4bc6834e-6298-e541-f38d-56cdec229512', 0, '2016-02-26 10:53:44', 0),
('830b787a-c76f-be7a-3a83-56cee39c405c', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4a8e0dae-d9c0-2783-efb2-56cdec27840a', 0, '2016-02-26 10:53:44', 0),
('830b7965-b67f-7f7d-e9f3-56cee3b4f2b6', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bde9b83c-4cf2-944f-e5fd-56cdecfac197', 0, '2016-02-26 10:53:44', 0),
('830b7a25-e907-9f09-a277-56cee3d15c8a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bcb1326c-8c49-f763-4b49-56cdec336058', 0, '2016-02-26 10:53:44', 0),
('830b7b3f-1b55-e1d1-c810-56cee3ae54a5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bd4d7fc1-5d4e-2f1b-d53c-56cdece3986c', 0, '2016-02-26 10:53:44', 0),
('830b7b6c-07d1-7f32-a76b-56cee31158fe', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '5897ddc0-2c9c-84b5-5107-56cdec1c9617', 0, '2016-02-26 10:53:44', 0),
('830b7fce-79ed-87a0-db58-56cee3f706f7', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '575f52be-23af-2bf5-0f40-56cdec3f84ba', 0, '2016-02-26 10:53:44', 0),
('84270f1f-0af3-9d4b-1890-56cf4f5490a6', '85f6e014-4622-795d-bde1-56cf4a829665', '28fc2df4-b650-8fea-c43d-56cdec731309', 0, '2016-02-25 19:03:12', 0),
('86da706c-d9b6-5ee4-8e5e-56cee361cbcd', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '88cfcd98-2984-fe4b-4bd3-56cdec5b83d0', 0, '2016-02-26 10:53:44', 0),
('86da717b-3a57-67e9-2f6a-56cee30526f7', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1eca01c-122c-21af-6d20-56cdec7c1ae4', 0, '2016-02-26 10:53:44', 0),
('86da7331-35bf-e1a9-e50a-56cee33f073d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '87974d95-9641-0845-1b34-56cdecc0c155', 0, '2016-02-26 10:53:44', 0),
('86da7336-4dc4-0c15-3f9e-56cee344727e', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e288e39e-c594-4b2a-2053-56cdec35ad38', 0, '2016-02-26 10:53:44', 0),
('86da73c7-fbf8-27c2-8ce1-56cee39e3718', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e3252f6e-fa05-fcad-4b0a-56cdec2cb0ce', 0, '2016-02-26 10:53:44', 0),
('86da75d2-4efc-31d7-4c13-56cee35d3a39', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '88338565-c80c-c96f-0d2a-56cdec96bcee', 0, '2016-02-26 10:53:44', 0),
('86da7710-8be9-dfd7-2bc2-56cee3e0e748', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '88338e1e-725f-1999-7426-56cdece13725', 0, '2016-02-26 10:53:44', 0),
('86da780f-7624-fbd7-67c6-56cee3f35a99', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1ecaafe-21b0-35b7-840f-56cdec8a90cc', 0, '2016-02-26 10:53:44', 0),
('86da78aa-af9e-ad5c-a116-56cee3b762aa', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e287e328-68b8-4d89-86a5-56cdecd9a49f', 0, '2016-02-26 10:53:44', 0),
('86da78e9-e2d5-caee-f905-56cee335d668', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e0b32578-0066-4644-fa90-56cdec75394c', -98, '2016-02-26 10:53:44', 0),
('86da7a1b-9bfc-8e34-870d-56cee33075ed', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e288eedf-37da-cc95-25a8-56cdec9b60a7', 0, '2016-02-26 10:53:44', 0),
('86da7a30-f066-45d1-1f88-56cee3003a5e', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '86fb0d3d-47d0-a74b-d5ac-56cdec657b74', 0, '2016-02-26 10:53:44', 0),
('86da7a4c-bcad-612f-4468-56cee3d353e4', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e288eb94-e337-dfe0-14ec-56cdecfd037c', 0, '2016-02-26 10:53:44', 0),
('86da7b19-0bd9-8bb3-98f0-56cee301699f', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '86fb06a2-9e3e-8847-7bde-56cdecdd001b', 0, '2016-02-26 10:53:44', 0),
('86da7b4f-f47c-7ae0-4879-56cee31b3a4a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1506e58-17e1-437b-25ac-56cdec0f9c2c', 0, '2016-02-26 10:53:44', 0),
('86da7ba7-55bc-50de-a9e8-56cee3d6d2ab', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '8797447a-0e90-4df7-f8bf-56cdec706357', 0, '2016-02-26 10:53:44', 0),
('86da7cf7-795c-3df7-541f-56cee36ee8d9', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1eba0de-8968-1b9c-d799-56cdecd2d7ee', 0, '2016-02-26 10:53:44', 0),
('86da7e8b-aae2-b0af-8537-56cee3c12140', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '88cfc781-5782-a5dd-6f44-56cdec7729d2', 0, '2016-02-26 10:53:44', 0),
('86da7f8f-9d99-d1f4-2009-56cee37a7b5b', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1eba702-72ea-a559-f5ad-56cdec52097d', 0, '2016-02-26 10:53:44', 0),
('880f03d8-10fc-1963-3687-56cf4f7f6b1e', '85f6e014-4622-795d-bde1-56cf4a829665', '29986b57-1968-52e4-ba83-56cdeca4a423', 0, '2016-02-25 19:03:12', 0),
('880f09c2-9256-1213-f84a-56cf4f5f6f50', '85f6e014-4622-795d-bde1-56cf4a829665', '28fc27f9-9adf-75d2-e441-56cdece77527', 0, '2016-02-25 19:03:12', 0),
('8aa971a7-1aa3-c418-204a-56cee3603585', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '30eb783c-9d05-33df-b503-56cdeca5349a', 75, '2016-02-26 10:53:44', 0),
('8aa9721a-cc9c-2558-b853-56cee3cb8296', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7b623381-cad8-8d7a-818b-56cdec046596', 89, '2016-02-26 10:53:44', 0),
('8aa972de-79f3-a7d0-58d9-56cee3a12aff', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '1fd4471d-8cbb-5ccb-d584-56cdecd557fa', 0, '2016-02-26 10:53:44', 0),
('8aa97300-6eaa-b55f-7312-56cee341fb76', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7c9abce8-46cd-915e-166c-56cdecca8263', 0, '2016-02-26 10:53:44', 0),
('8aa97456-29ff-be32-9124-56cee397d82e', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '2070a70f-c9c9-ab77-c082-56cdece04ff7', 0, '2016-02-26 10:53:44', 0),
('8aa97516-524e-8953-1ae8-56cee32f470b', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e14f6cb1-fe3a-4bab-730a-56cdec435f9c', 0, '2016-02-26 10:53:44', 0),
('8aa97569-687f-4362-0559-56cee352b7d5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7bfe6725-ea46-5799-b738-56cdec39e037', 0, '2016-02-26 10:53:44', 0),
('8aa975de-5803-0f42-a036-56cee3f2d7fa', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e1eba26c-36af-481d-087d-56cdec0e733a', 0, '2016-02-26 10:53:44', 0),
('8aa975e2-669b-fe22-f41c-56cee3fefdf5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '304f30cf-87d1-c037-7d60-56cdec576eda', 89, '2016-02-26 10:53:44', 0),
('8aa9769a-a168-82c0-a9c5-56cee3168522', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '2070a78f-b40c-1737-a5f2-56cdecc438a4', 0, '2016-02-26 10:53:44', 0),
('8aa97777-ba24-7e00-593f-56cee3ce1664', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7bfe6b78-374d-6105-6777-56cdecded90d', 0, '2016-02-26 10:53:44', 0),
('8aa9778e-3a6b-4649-ca51-56cee3f4a047', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e14f62f8-917e-164a-7b57-56cdeccdee72', 0, '2016-02-26 10:53:44', 0),
('8aa977cd-f0f2-e499-eabd-56cee36b5a70', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '1fd44c1f-5607-4bb9-997c-56cdecb26b5c', 0, '2016-02-26 10:53:44', 0),
('8aa97829-c4b9-ca63-5524-56cee37dbb00', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '1f380b24-e398-3dfe-6a32-56cdec6af30f', 89, '2016-02-26 10:53:44', 0),
('8aa9790c-1896-ef0e-6fa3-56cee3e98f60', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '3187bdfd-73e6-159e-a382-56cdecc9fd90', 75, '2016-02-26 10:53:44', 0),
('8aa97992-02bd-29fd-ac70-56cee3ef2b72', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '2070afcc-be95-e14b-d080-56cdec232675', 0, '2016-02-26 10:53:44', 0),
('8aa97996-6692-395a-9344-56cee3a33c8f', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '3187bbfc-1742-bd35-bbb1-56cdecdac7a0', 90, '2016-02-26 10:53:44', 0),
('8aa979cf-f7dc-29cf-241a-56cee3d0a381', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7bfe63ab-0e84-0127-b9f4-56cdec9295a2', 0, '2016-02-26 10:53:44', 0),
('8aa97a9c-bc87-7266-886f-56cee311f4a9', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '1f380c32-6f35-9c96-7981-56cdec3ab498', 0, '2016-02-26 10:53:44', 0),
('8aa97aaf-65b7-dbe5-a822-56cee3624cd7', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7c9ababb-1318-a321-18b5-56cdec7d2e9c', 0, '2016-02-26 10:53:44', 0),
('8aa97b11-c6ec-580b-9360-56cee3044b72', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '1f3809f6-3f4b-976c-9082-56cdec06ec59', 0, '2016-02-26 10:53:44', 0),
('8aa97c7f-30f0-02e9-8c57-56cee30a3b51', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '30eb7b8d-a6ca-567d-f4b6-56cdec01bc25', 75, '2016-02-26 10:53:44', 0),
('8aa97d07-2b8e-20e9-bc5a-56cee38eddd8', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7d36f412-d040-8c97-8b71-56cdecd0d604', 0, '2016-02-26 10:53:44', 0),
('8aa97d33-370b-7e7e-18e2-56cee3216493', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7d36f6ea-fe4f-ce5b-a5c7-56cdec262879', 0, '2016-02-26 10:53:44', 0),
('8aa97e8c-c6ac-6f88-8b8a-56cee380ea69', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'e287e56d-45fd-70aa-9afe-56cdec72ea3c', 0, '2016-02-26 10:53:44', 0),
('8bf70a44-8258-f7ae-6a73-56cf4fdf16fd', '85f6e014-4622-795d-bde1-56cf4a829665', '28fc23e1-d4d9-6fc3-ccc0-56cdec490bf9', 0, '2016-02-25 19:03:12', 0),
('8e7870da-cd60-9cbc-85cb-56cee303586d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '304f3ad2-a50f-737e-9249-56cdeccb5054', 0, '2016-02-26 10:53:44', 0),
('8e7871de-223f-5d0d-6153-56cee3a17599', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7372f9c8-8489-9a4b-8955-56cdec03f542', 89, '2016-02-26 10:53:44', 0),
('8e787214-a197-3505-6d04-56cee30f6c80', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6430ab5e-cb6b-4a78-9de1-56cdecfebdb7', 0, '2016-02-26 10:53:44', 0),
('8e78728c-1d55-a3e4-6664-56cee310ab58', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '740f39cb-a788-caa1-70ce-56cdec0a0ecd', 0, '2016-02-26 10:53:44', 0),
('8e787339-ba59-ff8b-480d-56cee301840e', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '740f3c61-8fe5-c695-ffe1-56cdec5ba0a1', 0, '2016-02-26 10:53:44', 0),
('8e7873ae-261b-50a9-3a27-56cee3943ff6', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '62f82b7b-4792-15a8-bf69-56cdec585597', -98, '2016-02-26 10:53:44', 0),
('8e7873f9-5626-ad53-b279-56cee38896f5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6430ac46-3c8f-b466-ea73-56cdecd0bcf7', 0, '2016-02-26 10:53:44', 0),
('8e787467-2cdf-9062-0716-56cee3242cb8', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6cbc22f5-7a2f-4e90-3165-56cdec1896b7', 89, '2016-02-26 10:53:44', 0),
('8e78746a-04b1-4b70-6262-56cee3665b9d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '63946767-0fda-3b44-6e6e-56cdecc3696a', 0, '2016-02-26 10:53:44', 0),
('8e787580-ca6f-4bd8-b481-56cee3b37658', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '74ab7acb-68ac-e950-4a8e-56cdecd118a6', 0, '2016-02-26 10:53:44', 0),
('8e787660-2d65-5cff-6e82-56cee376c789', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6d5860ec-f1f6-c318-bd2e-56cdecf7f1eb', 0, '2016-02-26 10:53:44', 0),
('8e787778-6442-e3eb-95f3-56cee329c6ce', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6430a33a-a196-e742-efb4-56cdecf1eb53', 0, '2016-02-26 10:53:44', 0),
('8e7877b3-c4bc-6f55-7788-56cee3e4171d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '74ab75d9-4028-9914-de0c-56cdec48f0d6', 0, '2016-02-26 10:53:44', 0),
('8e7878ff-a7b6-a6fa-2f01-56cee3691d98', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7547be18-77e1-e56c-05a9-56cdec9bef04', 0, '2016-02-26 10:53:44', 0),
('8e78794b-51e0-c314-0d1b-56cee302f6a9', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '74ab7213-b309-c53d-48e1-56cdec683548', 0, '2016-02-26 10:53:44', 0),
('8e7879f6-5ff9-2295-cba5-56cee312b546', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '304f382d-f9f7-2448-b7ce-56cdece78a77', 0, '2016-02-26 10:53:44', 0),
('8e787a39-519c-eb9f-846d-56cee3034bf9', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6394693d-9e98-501b-751e-56cdecee05cb', 0, '2016-02-26 10:53:44', 0),
('8e787a54-5946-625a-38f8-56cee3464b17', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '7547bbf9-bfe0-3c3c-05ae-56cdec37b9ae', 0, '2016-02-26 10:53:44', 0),
('8e787bb4-d266-9998-6a82-56cee36efd06', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6df4a3a3-2995-3d7b-ccee-56cdecf9ca3b', 0, '2016-02-26 10:53:44', 0),
('8e787cc9-c9d9-fa9f-60fe-56cee3504ec3', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '62f8229b-c83f-c6a1-654a-56cdecd2033e', 0, '2016-02-26 10:53:44', 0),
('8e787d46-e55e-a0df-ec5d-56cee3d040f6', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6d58677b-5905-bdb7-8895-56cdec11c6b1', 0, '2016-02-26 10:53:44', 0),
('8e787ef1-71e3-b6c1-7c05-56cee392cd67', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '3187bd0c-33b2-ed29-a61d-56cdeca594e8', 0, '2016-02-26 10:53:44', 0),
('8e787f3a-6d58-9465-4025-56cee3fb3189', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6d586c6f-e01b-aba4-5070-56cdec3432ed', 0, '2016-02-26 10:53:44', 0),
('8e787fe9-3f49-a739-8411-56cee3cc1c14', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6df4a567-ad97-5790-63d3-56cdecb17770', 0, '2016-02-26 10:53:44', 0),
('8e787ffc-949c-2576-48e2-56cee3b30b2c', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '62f8295c-c54e-8af3-d827-56cdec5d9852', 0, '2016-02-26 10:53:44', 0),
('8fdf0a03-6dff-dc4d-a86b-56cf4fdb12df', '85f6e014-4622-795d-bde1-56cf4a829665', '285fee4b-2ddc-7359-ba0b-56cdec8e730f', 0, '2016-02-25 19:03:12', 0),
('8fdf0db5-bcc3-cd17-8832-56cf4f1ba261', '85f6e014-4622-795d-bde1-56cf4a829665', '29986c60-e3d6-6515-9d8c-56cdec824907', 0, '2016-02-25 19:03:12', 0),
('92477005-65fc-e037-b213-56cee335971e', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '32480a66-369c-b9f3-37c4-56cdec67a037', -98, '2016-02-26 10:53:44', 0),
('92477043-08a4-f080-9baf-56cee334cbd9', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '285fee4b-2ddc-7359-ba0b-56cdec8e730f', 0, '2016-02-26 10:53:44', 0),
('924770a2-1d5e-9dc2-ea7d-56cee3d41b46', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6df4ac58-4604-b27a-029c-56cdec973aa5', 0, '2016-02-26 10:53:44', 0),
('92477132-58c7-cea2-6ba3-56cee388fd30', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6cbc2b1f-00f3-fe68-f7db-56cdec812648', 0, '2016-02-26 10:53:44', 0),
('924771a5-9c85-3b62-cdd2-56cee35ccd44', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '324802fb-647c-25e9-31fa-56cdec8595a0', 0, '2016-02-26 10:53:44', 0),
('9247736c-d485-318d-1220-56cee364ad57', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '3c0c0cdf-edc7-db23-2bed-56cdeca05ddc', 0, '2016-02-26 10:53:44', 0),
('9247748f-b468-f4e0-6738-56cee311cb37', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '28fc23e1-d4d9-6fc3-ccc0-56cdec490bf9', 0, '2016-02-26 10:53:44', 0),
('9247751e-3b2e-2b44-c354-56cee371e52b', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '45d00526-254f-d1ed-d381-56cdece7c5ca', 0, '2016-02-26 10:53:44', 0),
('924775cf-dfb1-9d22-1049-56cee3f160d1', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '29986c60-e3d6-6515-9d8c-56cdec824907', 0, '2016-02-26 10:53:44', 0),
('92477689-d1e5-0b13-acb4-56cee37facf5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '45d00440-85b7-a568-9be1-56cdecaf1630', 0, '2016-02-26 10:53:44', 0),
('924777d1-e41f-6bdb-4df9-56cee3972633', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '915b52ef-3944-7936-0b0e-56cdec8b743a', 0, '2016-02-26 10:53:44', 0),
('92477829-568a-f023-12c0-56cee3f13947', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '915b565f-cf3e-1886-8215-56cdec12693c', 0, '2016-02-26 10:53:44', 0),
('92477897-7be3-8ca8-7737-56cee398b6d6', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '285fe893-829f-b7a4-a583-56cdec73f288', 0, '2016-02-26 10:53:44', 0),
('92477969-6b46-8379-c253-56cee33ca9a5', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '28fc27f9-9adf-75d2-e441-56cdece77527', 0, '2016-02-26 10:53:44', 0),
('924779aa-585d-3916-0b75-56cee3158a03', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '28fc2df4-b650-8fea-c43d-56cdec731309', 0, '2016-02-26 10:53:44', 0),
('92477aab-592f-7116-0245-56cee3a4731c', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '91f796f2-156f-8bfb-9113-56cdeca68acc', 0, '2016-02-26 10:53:44', 0),
('92477afc-b346-8217-2a90-56cee345d56f', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '29986b57-1968-52e4-ba83-56cdeca4a423', 0, '2016-02-26 10:53:44', 0),
('92477b15-51f1-6ab6-2bd6-56cee34e860a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '4f9401f8-ffe0-dea4-b1c8-56cdec1f9371', 0, '2016-02-26 10:53:44', 0),
('92477b84-6bbb-baf7-a0a9-56cee31c3a66', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '45d00175-dabc-5afb-12aa-56cdeca2a2f9', 0, '2016-02-26 10:53:44', 0),
('92477c30-ea64-e092-0b57-56cee39d5d92', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '90bf17fb-6828-4428-d5e5-56cdecf34e6a', 89, '2016-02-26 10:53:44', 0),
('92477c47-6d00-1758-97ad-56cee3ee4603', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '3c0c0e50-e656-4a31-431c-56cdecb5d3ce', 0, '2016-02-26 10:53:44', 0),
('92477cf1-a5f9-dfc6-dc99-56cee3105469', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '91f79a4a-2720-1170-d8e7-56cdec6f9022', 0, '2016-02-26 10:53:44', 0),
('92477fc0-f1a6-8efb-953e-56cee32a4d3d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9293d41e-f105-cead-f873-56cdec8df346', 0, '2016-02-26 10:53:44', 0),
('92477fef-b22f-91b8-5c7f-56cee349baea', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '27c3af65-f8b3-f474-db93-56cdec82403d', 89, '2016-02-26 10:53:44', 0),
('93c70294-c516-8d89-ad59-56cf4f53fa88', '85f6e014-4622-795d-bde1-56cf4a829665', '285fe893-829f-b7a4-a583-56cdec73f288', 0, '2016-02-25 19:03:12', 0),
('96167037-489d-69ba-cd64-56cee3d9ebb9', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '99e6da36-ca46-28f6-f1e4-56cdecd6f1dd', 0, '2016-02-26 10:53:44', 0),
('961671d8-ea67-67e0-2727-56cee3b29e92', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9a8316ce-b8e2-4c44-59d3-56cdeccfe6b9', 0, '2016-02-26 10:53:44', 0),
('961672b0-53a5-7539-d054-56cee33ccebf', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9b1f50c5-2826-e023-1491-56cdec27b9f5', 0, '2016-02-26 10:53:44', 0),
('961673ea-8312-3703-b7f0-56cee3a08618', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '90bf169b-1ea3-b5cf-b3c2-56cdec5257ff', 0, '2016-02-26 10:53:44', 0),
('96167428-323c-a0a9-083d-56cee3b514fd', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9a831103-6445-c601-8931-56cdec4da2e7', 0, '2016-02-26 10:53:44', 0),
('96167554-229c-efa5-0654-56cee3894ca7', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bb000eb1-49b9-0800-baf8-56cdec19c21f', 0, '2016-02-26 10:53:44', 0),
('961675f4-b0cb-7f3e-5db9-56cee3968886', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'c4c40319-10b7-08fb-9deb-56cdec41d432', 0, '2016-02-26 10:53:44', 0),
('9616761f-0fcf-a2aa-a92d-56cee31a673d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'b13c01f3-727f-a8ac-3975-56cdec6b8327', 0, '2016-02-26 10:53:44', 0),
('961677c9-bb41-f306-3333-56cee32b6912', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bb000bd2-c5ff-2522-802d-56cdec74f84c', 0, '2016-02-26 10:53:44', 0),
('961678e0-00f6-4608-1506-56cee37921e7', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '994a959e-c38d-05df-e9d0-56cdec8259db', 89, '2016-02-26 10:53:44', 0),
('961679b3-8168-dba6-e5db-56cee3b59939', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9b1f562d-5de9-2609-254e-56cdec5443d1', 0, '2016-02-26 10:53:44', 0),
('961679cc-2797-181b-2b6b-56cee34c080d', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'bb000781-30ec-0bca-d199-56cdec0ff3cc', 0, '2016-02-26 10:53:44', 0),
('961679f0-c3cd-dcc7-0d05-56cee3d99b91', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '99e6d4e5-cf83-331b-1b19-56cdec3e3eeb', 0, '2016-02-26 10:53:44', 0),
('96167cb8-f21b-d0d0-5041-56cee3c1e7a7', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'b13c091e-2d88-078a-5b6a-56cdec4d8d4c', 0, '2016-02-26 10:53:44', 0),
('96167d52-bbe6-6399-05f5-56cee339cfd2', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'a77806cd-4d6b-bad0-b455-56cdec8cbb90', 89, '2016-02-26 10:53:44', 0),
('96167e97-2d73-38fc-a5bc-56cee367fcfe', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9a8313f7-a165-62a3-f6a5-56cdec303cd7', 0, '2016-02-26 10:53:44', 0),
('96167ed9-0554-d140-c572-56cee34c09c0', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'b13c026c-8f9f-1d3d-f50d-56cdecef0f6e', 0, '2016-02-26 10:53:44', 0),
('96167fd8-3ed7-be1f-4998-56cee3a36889', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '9293de9f-2f65-a7c1-dac5-56cdec9a96a5', 0, '2016-02-26 10:53:44', 0),
('97af0948-f200-a317-edce-56cf4f498b37', '85f6e014-4622-795d-bde1-56cf4a829665', '90bf17fb-6828-4428-d5e5-56cdecf34e6a', 89, '2016-02-25 19:03:12', 0),
('9b97096f-9068-82da-abfb-56cf4fa2b96d', '85f6e014-4622-795d-bde1-56cf4a829665', '915b565f-cf3e-1886-8215-56cdec12693c', 0, '2016-02-25 19:03:12', 0),
('9b970f3b-2170-a165-4f73-56cf4f966f36', '85f6e014-4622-795d-bde1-56cf4a829665', '91f796f2-156f-8bfb-9113-56cdeca68acc', 0, '2016-02-25 19:03:12', 0),
('9f7f0a2b-2409-0383-3652-56cf4fbe6bcd', '85f6e014-4622-795d-bde1-56cf4a829665', '9293d41e-f105-cead-f873-56cdec8df346', 0, '2016-02-25 19:03:12', 0),
('a3680cf7-9bd5-1f44-6db3-56cf4f856e35', '85f6e014-4622-795d-bde1-56cf4a829665', '91f79a4a-2720-1170-d8e7-56cdec6f9022', 0, '2016-02-25 19:03:12', 0),
('a3680de3-b496-f225-2427-56cf4f51d89c', '85f6e014-4622-795d-bde1-56cf4a829665', '915b52ef-3944-7936-0b0e-56cdec8b743a', 0, '2016-02-25 19:03:12', 0),
('a750005f-21d3-dd27-f16a-56cf4f79728b', '85f6e014-4622-795d-bde1-56cf4a829665', '9293de9f-2f65-a7c1-dac5-56cdec9a96a5', 0, '2016-02-25 19:03:12', 0),
('a7500a03-ccd3-ef6e-e59a-56cf4ff0f4cc', '85f6e014-4622-795d-bde1-56cf4a829665', '90bf169b-1ea3-b5cf-b3c2-56cdec5257ff', 0, '2016-02-25 19:03:12', 0),
('ab380cb2-a147-7aa1-3a7c-56cf4fb1bc8a', '85f6e014-4622-795d-bde1-56cf4a829665', '9a8313f7-a165-62a3-f6a5-56cdec303cd7', 0, '2016-02-25 19:03:12', 0),
('ab380dad-0ccd-40e2-7461-56cf4f839d8b', '85f6e014-4622-795d-bde1-56cf4a829665', '994a959e-c38d-05df-e9d0-56cdec8259db', 89, '2016-02-25 19:03:12', 0),
('af2002f9-eb01-7362-d063-56cf4f5d82df', '85f6e014-4622-795d-bde1-56cf4a829665', '9a8316ce-b8e2-4c44-59d3-56cdeccfe6b9', 0, '2016-02-25 19:03:12', 0),
('b0d00df1-d603-20a5-0902-56cf4f4ce601', '85f6e014-4622-795d-bde1-56cf4a829665', '7bfe6b78-374d-6105-6777-56cdecded90d', 0, '2016-02-25 19:03:12', 0),
('b0d00edd-65c0-53c5-7151-56cf4fd3a558', '85f6e014-4622-795d-bde1-56cf4a829665', '7d36f6ea-fe4f-ce5b-a5c7-56cdec262879', 0, '2016-02-25 19:03:12', 0),
('b308009c-70d2-d3e4-49b9-56cf4fca0321', '85f6e014-4622-795d-bde1-56cf4a829665', '9b1f562d-5de9-2609-254e-56cdec5443d1', 0, '2016-02-25 19:03:12', 0),
('b3080e31-8f88-48fa-71ce-56cf4fd416c0', '85f6e014-4622-795d-bde1-56cf4a829665', '9a831103-6445-c601-8931-56cdec4da2e7', 0, '2016-02-25 19:03:12', 0),
('b6f00439-5a4c-79b5-0580-56cf4fa815db', '85f6e014-4622-795d-bde1-56cf4a829665', '9b1f50c5-2826-e023-1491-56cdec27b9f5', 0, '2016-02-25 19:03:12', 0),
('b6f00828-5569-01b0-727f-56cf4f03bf4e', '85f6e014-4622-795d-bde1-56cf4a829665', '99e6da36-ca46-28f6-f1e4-56cdecd6f1dd', 0, '2016-02-25 19:03:12', 0),
('bad804ff-c426-a333-c6f8-56cf4f374a0b', '85f6e014-4622-795d-bde1-56cf4a829665', 'a77806cd-4d6b-bad0-b455-56cdec8cbb90', 89, '2016-02-25 19:03:12', 0),
('bad805ef-a51b-85a2-61b7-56cf4ff8d1ea', '85f6e014-4622-795d-bde1-56cf4a829665', '99e6d4e5-cf83-331b-1b19-56cdec3e3eeb', 0, '2016-02-25 19:03:12', 0),
('bec00345-9b15-5891-895b-56cf4fb27f1c', '85f6e014-4622-795d-bde1-56cf4a829665', 'bb000bd2-c5ff-2522-802d-56cdec74f84c', 0, '2016-02-25 19:03:12', 0),
('c2a80217-3b12-425c-f7b3-56cf4f8cc8a6', '85f6e014-4622-795d-bde1-56cf4a829665', 'b13c091e-2d88-078a-5b6a-56cdec4d8d4c', 0, '2016-02-25 19:03:12', 0),
('c2a803ba-ea70-2004-1a76-56cf4f4f23f9', '85f6e014-4622-795d-bde1-56cf4a829665', 'bb000eb1-49b9-0800-baf8-56cdec19c21f', 0, '2016-02-25 19:03:12', 0),
('c6900365-45e4-9ccc-b86e-56cf4f28aace', '85f6e014-4622-795d-bde1-56cf4a829665', 'bb000781-30ec-0bca-d199-56cdec0ff3cc', 0, '2016-02-25 19:03:12', 0),
('ca7808da-d149-c6a9-d0c2-56cf4f9bccd5', '85f6e014-4622-795d-bde1-56cf4a829665', 'b13c026c-8f9f-1d3d-f50d-56cdecef0f6e', 0, '2016-02-25 19:03:12', 0),
('ca780b60-2951-4f16-d149-56cf4f62d1a9', '85f6e014-4622-795d-bde1-56cf4a829665', 'c4c40319-10b7-08fb-9deb-56cdec41d432', 0, '2016-02-25 19:03:12', 0),
('ce600e4b-da4f-b68b-51f3-56cf4f68ea22', '85f6e014-4622-795d-bde1-56cf4a829665', 'b13c01f3-727f-a8ac-3975-56cdec6b8327', 0, '2016-02-25 19:03:12', 0),
('e43a9360-2f29-04f7-a664-56cf4f0452cc', '85f6e014-4622-795d-bde1-56cf4a829665', '14d7d43b-77b6-8a09-9a24-56cdec1003fc', -98, '2016-02-25 19:03:12', 0),
('e4f62d5f-ff0e-796d-2100-56cf4f1ec58c', '85f6e014-4622-795d-bde1-56cf4a829665', '16105230-442a-bc29-a26c-56cdec269d7b', 0, '2016-02-25 19:03:12', 0),
('e534a0f4-e987-7518-8439-56cf4fa99435', '85f6e014-4622-795d-bde1-56cf4a829665', '15741114-c0ba-f279-f588-56cdec6c559e', 0, '2016-02-25 19:03:12', 0),
('e57327e3-4c07-d597-938e-56cf4f59a01b', '85f6e014-4622-795d-bde1-56cf4a829665', '161055c3-53ad-2977-e398-56cdeca68276', 0, '2016-02-25 19:03:12', 0),
('e5b1ae2c-1781-e1b4-419e-56cf4f4e823b', '85f6e014-4622-795d-bde1-56cf4a829665', '16105372-05d8-6637-957f-56cdec995194', 0, '2016-02-25 19:03:12', 0),
('e5b1afdb-fb51-fd39-d369-56cf4fa4e5c5', '85f6e014-4622-795d-bde1-56cf4a829665', '1574161f-a535-97bf-1027-56cdec1f47df', 0, '2016-02-25 19:03:12', 0),
('e62eab34-fbf8-5633-6c95-56cf4fb2c4d9', '85f6e014-4622-795d-bde1-56cf4a829665', '16ac9f31-61dd-9c39-297c-56cdec83d86a', 0, '2016-02-25 19:03:12', 0),
('e66d2691-fc76-970a-68d2-56cf4f8c0f3c', '85f6e014-4622-795d-bde1-56cf4a829665', '14d7d88a-fa01-441c-9662-56cdeca4425c', 0, '2016-02-25 19:03:12', 0),
('e6aba2bf-8364-b940-a216-56cf4fadbe66', '85f6e014-4622-795d-bde1-56cf4a829665', 'd5b6a7b4-4694-2e88-c790-56cdec370884', -98, '2016-02-25 19:03:12', 0),
('e6ea23fb-0db5-6456-358d-56cf4fa0dbda', '85f6e014-4622-795d-bde1-56cf4a829665', 'd6ef2dec-b837-67fa-4eeb-56cdecf90424', 0, '2016-02-25 19:03:12', 0),
('e728a09a-95d4-ddf9-d7c1-56cf4fdc9a30', '85f6e014-4622-795d-bde1-56cf4a829665', 'd78b69ca-5084-d4a2-d59d-56cdec23ebbf', 0, '2016-02-25 19:03:12', 0),
('e728ac88-f890-42de-705d-56cf4fbe64c7', '85f6e014-4622-795d-bde1-56cf4a829665', 'd6ef256b-9418-e055-298a-56cdec9c3813', 0, '2016-02-25 19:03:12', 0),
('e767203a-a3dd-7e8f-e4bf-56cf4f678cf2', '85f6e014-4622-795d-bde1-56cf4a829665', 'd78b6b83-d047-3d4d-cb9d-56cdecd9c661', 0, '2016-02-25 19:03:12', 0),
('e7a5abd4-75a5-8445-55d1-56cf4f0339c9', '85f6e014-4622-795d-bde1-56cf4a829665', 'd652ee7e-fc21-18e6-19ab-56cdec5f692b', 0, '2016-02-25 19:03:12', 0),
('e7e421fd-06bc-ff4f-4446-56cf4ff9bc76', '85f6e014-4622-795d-bde1-56cf4a829665', 'd827a740-6cd2-ffb0-c481-56cdecd4fa85', 0, '2016-02-25 19:03:12', 0),
('e822a256-d1bc-935c-a82b-56cf4f8d3fa3', '85f6e014-4622-795d-bde1-56cf4a829665', 'd652e639-4b43-72a3-6628-56cdecf18c0c', 0, '2016-02-25 19:03:12', 0),
('e8612276-ecd7-6d85-7b2e-56cf4fedd1f7', '85f6e014-4622-795d-bde1-56cf4a829665', 'c8e55aba-4b03-83da-b59b-56cdec0a88f1', 89, '2016-02-25 19:03:12', 0),
('e89fa19f-cd10-cc49-c631-56cf4f32be67', '85f6e014-4622-795d-bde1-56cf4a829665', 'ca1dd328-840e-1e2d-f0f7-56cdec69ae4f', 0, '2016-02-25 19:03:12', 0),
('e8de28c9-d3c2-dd7b-77ed-56cf4f5e27cd', '85f6e014-4622-795d-bde1-56cf4a829665', 'c981995d-8db2-0f62-0f99-56cdeccabcd0', 0, '2016-02-25 19:03:12', 0),
('e91cb8f6-661e-7b4b-4bf0-56cf4f9536a1', '85f6e014-4622-795d-bde1-56cf4a829665', 'caba132b-2823-c890-27d2-56cdecd4f7d9', 0, '2016-02-25 19:03:12', 0),
('e95b31b0-c931-3611-32b0-56cf4f81b7b7', '85f6e014-4622-795d-bde1-56cf4a829665', 'c9819733-4303-cb23-3a72-56cdec805c84', 0, '2016-02-25 19:03:12', 0),
('e95b3e9d-e44b-af4a-ff74-56cf4fcb9163', '85f6e014-4622-795d-bde1-56cf4a829665', 'ca1dd3c9-9c4b-16d1-767f-56cdec8731fb', 0, '2016-02-25 19:03:12', 0),
('e999b9be-9ac4-13fc-6ecd-56cf4f8a9b0f', '85f6e014-4622-795d-bde1-56cf4a829665', 'caba15d7-bf6f-5958-d81d-56cdec956a8a', 0, '2016-02-25 19:03:12', 0),
('e9d83335-42b5-4054-ae51-56cf4f1a4b30', '85f6e014-4622-795d-bde1-56cf4a829665', 'c8e55289-8bc4-4192-6441-56cdec1ffb83', 0, '2016-02-25 19:03:12', 0),
('ea16bc96-11ad-1b6d-8049-56cf4f0b2905', '85f6e014-4622-795d-bde1-56cf4a829665', '49f1ce69-e8f3-e233-b782-56cdec21e0b1', 89, '2016-02-25 19:03:12', 0),
('ea55350d-2984-5808-4011-56cf4f3a6c17', '85f6e014-4622-795d-bde1-56cf4a829665', '4a8e0639-626b-12ce-08fe-56cdec954e44', 0, '2016-02-25 19:03:12', 0),
('ea55366b-f891-7de2-9b82-56cf4f513f66', '85f6e014-4622-795d-bde1-56cf4a829665', '4b2a46b0-2eb6-74a5-093c-56cdec3750e0', 0, '2016-02-25 19:03:12', 0),
('ea93be29-5e70-5450-d97f-56cf4f4eeda9', '85f6e014-4622-795d-bde1-56cf4a829665', '4bc68a82-35cc-f64a-0885-56cdec753737', 0, '2016-02-25 19:03:12', 0),
('ead238df-5d86-57c2-790c-56cf4f9ddf30', '85f6e014-4622-795d-bde1-56cf4a829665', '4b2a421a-be3d-707e-72de-56cdec430fd7', 0, '2016-02-25 19:03:12', 0),
('eb10b1b6-bc9d-ebf9-ae0c-56cf4f8c0660', '85f6e014-4622-795d-bde1-56cf4a829665', '4bc6834e-6298-e541-f38d-56cdec229512', 0, '2016-02-25 19:03:12', 0),
('eb10bc38-d0c4-53ab-d77d-56cf4fc52795', '85f6e014-4622-795d-bde1-56cf4a829665', '4a8e0e6f-521b-6725-2f03-56cdec16693f', 0, '2016-02-25 19:03:12', 0),
('eb4f3d40-2ab9-2c51-108a-56cf4f1cd47f', '85f6e014-4622-795d-bde1-56cf4a829665', '4a8e0dae-d9c0-2783-efb2-56cdec27840a', 0, '2016-02-25 19:03:12', 0),
('eb8db41d-fee3-d4b5-5897-56cf4fe36397', '85f6e014-4622-795d-bde1-56cf4a829665', '56c3176d-c92d-a626-64d0-56cdec1ba43c', 89, '2016-02-25 19:03:12', 0),
('ebcc3b5d-2f92-45ce-6ce2-56cf4f33fc17', '85f6e014-4622-795d-bde1-56cf4a829665', '57fb9077-0dc4-09c2-54c1-56cdec2fcb4c', 0, '2016-02-25 19:03:12', 0),
('ec0ab133-6820-6527-7f15-56cf4fb8c2e3', '85f6e014-4622-795d-bde1-56cf4a829665', '5897de46-e3f6-f0ab-3dcb-56cdec4c4391', 0, '2016-02-25 19:03:12', 0),
('ec0abd31-abfa-6185-a7cb-56cf4fa0b547', '85f6e014-4622-795d-bde1-56cf4a829665', '57fb9ccb-84f3-d718-856f-56cdec603da9', 0, '2016-02-25 19:03:12', 0),
('ec49392e-db41-d705-fe08-56cf4ff8f9c3', '85f6e014-4622-795d-bde1-56cf4a829665', '57fb9ba5-7b0a-93fd-b4f4-56cdecee24bc', 0, '2016-02-25 19:03:12', 0),
('ec87bdc6-d76e-c2af-a5a8-56cf4fad3238', '85f6e014-4622-795d-bde1-56cf4a829665', '575f5bbe-ff39-3f83-c3ee-56cdec0527d4', 0, '2016-02-25 19:03:12', 0),
('ecc6312c-b6ba-27cc-ca37-56cf4f0fb5c1', '85f6e014-4622-795d-bde1-56cf4a829665', '5897ddc0-2c9c-84b5-5107-56cdec1c9617', 0, '2016-02-25 19:03:12', 0),
('ed04bbc6-2515-0730-479e-56cf4f28bfbe', '85f6e014-4622-795d-bde1-56cf4a829665', '575f52be-23af-2bf5-0f40-56cdec3f84ba', 0, '2016-02-25 19:03:12', 0),
('ed4348ff-f2aa-115b-249f-56cf4f682c29', '85f6e014-4622-795d-bde1-56cf4a829665', 'bcb133f6-2022-85f7-2045-56cdece12226', -98, '2016-02-25 19:03:12', 0),
('ed81c4b8-13c7-0015-d39b-56cf4f0a1ee2', '85f6e014-4622-795d-bde1-56cf4a829665', 'bde9b83c-4cf2-944f-e5fd-56cdecfac197', 0, '2016-02-25 19:03:12', 0),
('ed81cb5e-40f9-f7e0-292d-56cf4f3578c5', '85f6e014-4622-795d-bde1-56cf4a829665', 'bd4d7fc1-5d4e-2f1b-d53c-56cdece3986c', 0, '2016-02-25 19:03:12', 0),
('edc04b90-658d-d58b-f98f-56cf4f69d2e0', '85f6e014-4622-795d-bde1-56cf4a829665', 'be85f260-ec1c-19b4-b81b-56cdeccd7955', 0, '2016-02-25 19:03:12', 0),
('edfecbbc-0c62-eddd-2375-56cf4f724864', '85f6e014-4622-795d-bde1-56cf4a829665', 'bde9b8a6-c214-96ba-40a1-56cdec3419ef', 0, '2016-02-25 19:03:12', 0),
('ee3d48d8-5ea2-3a72-f5a9-56cf4ffdc368', '85f6e014-4622-795d-bde1-56cf4a829665', 'bd4d771c-7a3c-7478-2a64-56cdec83b099', 0, '2016-02-25 19:03:12', 0),
('ee3d4f31-f2df-bc0b-19bd-56cf4f58aff5', '85f6e014-4622-795d-bde1-56cf4a829665', 'be85f05a-05c0-2a69-3c75-56cdecba2645', 0, '2016-02-25 19:03:12', 0),
('ee7bc86a-65ed-2fc8-1dde-56cf4f2f7827', '85f6e014-4622-795d-bde1-56cf4a829665', 'bcb1326c-8c49-f763-4b49-56cdec336058', 0, '2016-02-25 19:03:12', 0),
('eeba4016-27e6-e3a2-a47d-56cf4ffa100d', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1506ec4-5d5e-2550-998f-56cdec917e71', -98, '2016-02-25 19:03:12', 0),
('eeba425e-74ef-ae30-5644-56cf4f867494', '85f6e014-4622-795d-bde1-56cf4a829665', 'e288eb94-e337-dfe0-14ec-56cdecfd037c', 0, '2016-02-25 19:03:12', 0),
('eef8c199-bc7f-3821-81e5-56cf4f123486', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1ecaafe-21b0-35b7-840f-56cdec8a90cc', 0, '2016-02-25 19:03:12', 0),
('ef37428a-3166-846b-a9cb-56cf4f5b7ba8', '85f6e014-4622-795d-bde1-56cf4a829665', 'e288e39e-c594-4b2a-2053-56cdec35ad38', 0, '2016-02-25 19:03:12', 0),
('ef5007eb-d84d-6b38-35c4-56cf4f5874ef', '85f6e014-4622-795d-bde1-56cf4a829665', '7bfe6725-ea46-5799-b738-56cdec39e037', 0, '2016-02-25 19:03:12', 0),
('ef75c0eb-7bb0-cff4-4047-56cf4f386127', '85f6e014-4622-795d-bde1-56cf4a829665', 'e288eedf-37da-cc95-25a8-56cdec9b60a7', 0, '2016-02-25 19:03:12', 0),
('ef75c5bf-33b9-7b68-e875-56cf4fb5f873', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1eca01c-122c-21af-6d20-56cdec7c1ae4', 0, '2016-02-25 19:03:12', 0),
('efb44ade-3b76-5741-d4b4-56cf4f2a4019', '85f6e014-4622-795d-bde1-56cf4a829665', 'e3252f6e-fa05-fcad-4b0a-56cdec2cb0ce', 0, '2016-02-25 19:03:12', 0),
('eff2c9ac-eea9-ceaf-ce7c-56cf4fc50bfd', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1506e58-17e1-437b-25ac-56cdec0f9c2c', 0, '2016-02-25 19:03:12', 0),
('f03148e5-9504-b2f3-57ec-56cf4f25ac7a', '85f6e014-4622-795d-bde1-56cf4a829665', '86fb06a2-9e3e-8847-7bde-56cdecdd001b', -98, '2016-02-25 19:03:12', 0),
('f0314a80-1eb0-3a10-6dfb-56cf4fa70d5b', '85f6e014-4622-795d-bde1-56cf4a829665', '88338565-c80c-c96f-0d2a-56cdec96bcee', 0, '2016-02-25 19:03:12', 0),
('f06fc6ec-22de-af59-3539-56cf4f9dd8ce', '85f6e014-4622-795d-bde1-56cf4a829665', '87974d95-9641-0845-1b34-56cdecc0c155', 0, '2016-02-25 19:03:12', 0),
('f0ae41f7-9d8f-ce6a-43f0-56cf4fda47e8', '85f6e014-4622-795d-bde1-56cf4a829665', '88cfcd98-2984-fe4b-4bd3-56cdec5b83d0', 0, '2016-02-25 19:03:12', 0),
('f0ecc817-990c-48a7-1f45-56cf4fed19e3', '85f6e014-4622-795d-bde1-56cf4a829665', '88338e1e-725f-1999-7426-56cdece13725', 0, '2016-02-25 19:03:12', 0),
('f12b4004-69f8-b852-f817-56cf4f8e3c7c', '85f6e014-4622-795d-bde1-56cf4a829665', '88cfc781-5782-a5dd-6f44-56cdec7729d2', 0, '2016-02-25 19:03:12', 0),
('f12b4c95-fadc-dcbb-95a0-56cf4fa0a20f', '85f6e014-4622-795d-bde1-56cf4a829665', '8797447a-0e90-4df7-f8bf-56cdec706357', 0, '2016-02-25 19:03:12', 0),
('f169c247-4a13-2674-6503-56cf4fb37abd', '85f6e014-4622-795d-bde1-56cf4a829665', '86fb0d3d-47d0-a74b-d5ac-56cdec657b74', 0, '2016-02-25 19:03:12', 0),
('f1a85515-a722-4fb8-a358-56cf4fc3d91c', '85f6e014-4622-795d-bde1-56cf4a829665', 'e0b32578-0066-4644-fa90-56cdec75394c', -98, '2016-02-25 19:03:12', 0),
('f1a8591a-7296-3c35-9bca-56cf4fc7f445', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1eba702-72ea-a559-f5ad-56cdec52097d', 0, '2016-02-25 19:03:12', 0),
('f1e6d267-9550-cf05-69ef-56cf4ff8851a', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1eba0de-8968-1b9c-d799-56cdecd2d7ee', 0, '2016-02-25 19:03:12', 0),
('f2255c37-7295-b431-b2b3-56cf4fbaca3f', '85f6e014-4622-795d-bde1-56cf4a829665', 'e287e328-68b8-4d89-86a5-56cdecd9a49f', 0, '2016-02-25 19:03:12', 0),
('f2255e90-5b66-d825-6fdb-56cf4f79785c', '85f6e014-4622-795d-bde1-56cf4a829665', 'e1eba26c-36af-481d-087d-56cdec0e733a', 0, '2016-02-25 19:03:12', 0),
('f263d9ba-9329-162c-d1bb-56cf4f643c30', '85f6e014-4622-795d-bde1-56cf4a829665', 'e14f62f8-917e-164a-7b57-56cdeccdee72', 0, '2016-02-25 19:03:12', 0),
('f2a25219-6e3f-ea5b-a730-56cf4f0cdef8', '85f6e014-4622-795d-bde1-56cf4a829665', 'e14f6cb1-fe3a-4bab-730a-56cdec435f9c', 0, '2016-02-25 19:03:12', 0),
('f2a25d64-27b8-7c7d-fb59-56cf4fa513d0', '85f6e014-4622-795d-bde1-56cf4a829665', 'e287e56d-45fd-70aa-9afe-56cdec72ea3c', 0, '2016-02-25 19:03:12', 0),
('f2e0d0d8-30b5-a2b5-64bd-56cf4fe68dc5', '85f6e014-4622-795d-bde1-56cf4a829665', '1fd4471d-8cbb-5ccb-d584-56cdecd557fa', 0, '2016-02-25 19:03:12', 0),
('f2e0d7a0-8a30-7375-52a0-56cf4f39c86b', '85f6e014-4622-795d-bde1-56cf4a829665', '1f380b24-e398-3dfe-6a32-56cdec6af30f', -98, '2016-02-25 19:03:12', 0),
('f31f51a2-3b88-53a7-5a6e-56cf4f64fd28', '85f6e014-4622-795d-bde1-56cf4a829665', '1fd44c1f-5607-4bb9-997c-56cdecb26b5c', 0, '2016-02-25 19:03:12', 0),
('f35dd545-b712-3118-24d0-56cf4f7eca48', '85f6e014-4622-795d-bde1-56cf4a829665', '2070a78f-b40c-1737-a5f2-56cdecc438a4', 0, '2016-02-25 19:03:12', 0),
('f35ddd00-0fb1-6421-ca9d-56cf4fa54306', '85f6e014-4622-795d-bde1-56cf4a829665', '2070afcc-be95-e14b-d080-56cdec232675', 0, '2016-02-25 19:03:12', 0),
('f39c537f-3fca-a153-3ea3-56cf4f5a8ae2', '85f6e014-4622-795d-bde1-56cf4a829665', '1f3809f6-3f4b-976c-9082-56cdec06ec59', 0, '2016-02-25 19:03:12', 0),
('f39c5895-30c6-abd8-e870-56cf4f8c6a3f', '85f6e014-4622-795d-bde1-56cf4a829665', '2070a70f-c9c9-ab77-c082-56cdece04ff7', 0, '2016-02-25 19:03:12', 0),
('f3dad7a6-03f6-05ef-4f49-56cf4f4836d0', '85f6e014-4622-795d-bde1-56cf4a829665', '1f380c32-6f35-9c96-7981-56cdec3ab498', 0, '2016-02-25 19:03:12', 0),
('f4195580-a3a0-4736-7087-56cf4fd72052', '85f6e014-4622-795d-bde1-56cf4a829665', '7b623381-cad8-8d7a-818b-56cdec046596', -98, '2016-02-25 19:03:12', 0),
('f41959e6-0743-1754-33a3-56cf4f90a422', '85f6e014-4622-795d-bde1-56cf4a829665', '7c9ababb-1318-a321-18b5-56cdec7d2e9c', 0, '2016-02-25 19:03:12', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_roles_users`
--

CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acl_roles_users`
--

INSERT INTO `acl_roles_users` (`id`, `role_id`, `user_id`, `date_modified`, `deleted`) VALUES
('16140825-19bb-0d32-b803-56cf4fb16649', '85f6e014-4622-795d-bde1-56cf4a829665', '409c1178-a159-5926-566a-56cf4fe4f450', '2016-02-25 19:01:22', 0),
('48ad0fc7-bab3-b07d-4511-56cee206cb8a', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'b2752bed-800e-8997-c965-56cee29a3cdb', '2016-02-25 12:19:51', 1),
('63a36d90-de8b-3853-a94f-56cf5496bf67', '55effc16-809c-3ff4-f464-56cf528b4049', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', '2016-02-25 19:22:15', 0),
('9d8106d3-978b-a7ec-e7fd-56cf46785009', 'e900e1cc-5f05-d544-b881-56cf44eeee58', '816c0648-ca93-1d95-11f9-56cf45208e84', '2016-02-25 18:21:30', 0),
('b458b186-4210-6d0b-f650-56cef152a796', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '2016-02-25 12:21:17', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `address_book`
--

CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) DEFAULT NULL,
  `bean_id` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bugs`
--

CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bugs`
--

INSERT INTO `bugs` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `bug_number`, `type`, `status`, `priority`, `resolution`, `work_log`, `found_in_release`, `fixed_in_release`, `source`, `product_category`) VALUES
('8d4e6d93-c22d-5e05-e678-56d050a6ba5e', 'Acerca de la oferta spa', '2016-02-26 13:16:35', '2016-02-26 13:16:35', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', '', 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 1, 'Defect', 'Assigned', 'Urgent', ' ', '', '', '', ' ', ' ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bugs_audit`
--

CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls`
--

CREATE TABLE `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `direction` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls`
--

INSERT INTO `calls` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `duration_hours`, `duration_minutes`, `date_start`, `date_end`, `parent_type`, `status`, `direction`, `parent_id`, `reminder_time`, `email_reminder_time`, `email_reminder_sent`, `outlook_id`, `repeat_type`, `repeat_interval`, `repeat_dow`, `repeat_until`, `repeat_count`, `repeat_parent_id`, `recurring_source`) VALUES
('1e1e80d2-8f42-5d63-7e7e-56cf6384411c', 'Confirmación de cita', '2016-02-25 20:27:01', '2016-02-25 20:27:01', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', '', 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 0, 15, '2016-03-04 20:30:00', '2016-03-04 20:45:00', 'Leads', 'Planned', 'Inbound', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', 1800, -1, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls_contacts`
--

CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls_leads`
--

CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls_leads`
--

INSERT INTO `calls_leads` (`id`, `call_id`, `lead_id`, `required`, `accept_status`, `date_modified`, `deleted`) VALUES
('20128215-8c9d-913b-c01a-56cf633f9bda', '1e1e80d2-8f42-5d63-7e7e-56cf6384411c', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', '1', 'none', '2016-02-25 20:27:01', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calls_users`
--

CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calls_users`
--

INSERT INTO `calls_users` (`id`, `call_id`, `user_id`, `required`, `accept_status`, `date_modified`, `deleted`) VALUES
('2b4db2bc-442a-8d35-7b1c-56cf633b9746', '1e1e80d2-8f42-5d63-7e7e-56cf6384411c', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', '1', 'accept', '2016-02-26 13:15:41', 0),
('2c47b68d-ce53-667a-05c2-56cf6337cb08', '1e1e80d2-8f42-5d63-7e7e-56cf6384411c', '816c0648-ca93-1d95-11f9-56cf45208e84', '1', 'none', '2016-02-25 20:27:01', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaigns`
--

CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(100) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `campaigns`
--

INSERT INTO `campaigns` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `deleted`, `assigned_user_id`, `tracker_key`, `tracker_count`, `refer_url`, `tracker_text`, `start_date`, `end_date`, `status`, `impressions`, `currency_id`, `budget`, `expected_cost`, `actual_cost`, `expected_revenue`, `campaign_type`, `objective`, `content`, `frequency`) VALUES
('df781430-6d7e-2794-5764-56d0380c6338', 'Ofertas Spa', '2016-02-26 11:33:49', '2016-02-26 12:25:59', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 0, '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 1, 0, 'http://', NULL, '2016-03-01', '2016-03-11', 'Active', 400, '-99', 500, 650, 700, 3000, 'Email', 'El objetivo de la campaña es atraer el máximo de clientes potenciales y contactos a nuestra campaña publicitaria.', 'Oferta por una noche en Hoteles Albarracín, descuento del 50% en Spa durante todo el día.', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaigns_audit`
--

CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `campaigns_audit`
--

INSERT INTO `campaigns_audit` (`id`, `parent_id`, `date_created`, `created_by`, `field_name`, `data_type`, `before_value_string`, `after_value_string`, `before_value_text`, `after_value_text`) VALUES
('cd9ab248-20b7-a28b-3793-56d03efdaf4f', 'df781430-6d7e-2794-5764-56d0380c6338', '2016-02-26 12:00:52', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'status', 'enum', 'Planning', 'Active', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaign_log`
--

CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaign_trkrs`
--

CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(30) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cases`
--

CREATE TABLE `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_number` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cases_audit`
--

CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cases_bugs`
--

CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `category` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`category`, `name`, `value`) VALUES
('notify', 'fromaddress', 'josemi_9000@yahoo.es'),
('notify', 'fromname', 'Hoteles Albarracín'),
('notify', 'send_by_default', '1'),
('notify', 'on', '1'),
('notify', 'send_from_assigning_user', '0'),
('info', 'sugar_version', '6.5.22'),
('MySettings', 'tab', 'YToxNDp7aTowO3M6NDoiSG9tZSI7aToxO3M6ODoiQWNjb3VudHMiO2k6MjtzOjg6IkNvbnRhY3RzIjtpOjM7czoxMzoiT3Bwb3J0dW5pdGllcyI7aTo0O3M6NToiTGVhZHMiO2k6NTtzOjg6IkNhbGVuZGFyIjtpOjY7czo5OiJDYW1wYWlnbnMiO2k6NztzOjU6IkNhbGxzIjtpOjg7czo4OiJNZWV0aW5ncyI7aTo5O3M6NToiVGFza3MiO2k6MTA7czo1OiJOb3RlcyI7aToxMTtzOjk6IlByb3NwZWN0cyI7aToxMjtzOjQ6IkJ1Z3MiO2k6MTM7czoxMzoiUHJvc3BlY3RMaXN0cyI7fQ=='),
('portal', 'on', '0'),
('tracker', 'Tracker', '1'),
('system', 'skypeout_on', '1'),
('sugarfeed', 'enabled', '1'),
('sugarfeed', 'module_UserFeed', '1'),
('sugarfeed', 'module_Cases', '1'),
('sugarfeed', 'module_Contacts', '1'),
('sugarfeed', 'module_Leads', '1'),
('sugarfeed', 'module_Opportunities', '1'),
('Update', 'CheckUpdates', 'automatic'),
('system', 'name', 'Hoteles Albarricín'),
('license', 'msg_admin', ''),
('license', 'msg_all', ''),
('license', 'last_validation', 'success'),
('license', 'latest_versions', 'YToxOntpOjA7YToyOntzOjc6InZlcnNpb24iO3M6NjoiNi41LjIwIjtzOjExOiJkZXNjcmlwdGlvbiI7czoxNjM6IlRoZSBsYXRlc3QgdmVyc2lvbiBvZiBTdWdhckNSTSBpcyA2LjUuMjAuICBQbGVhc2UgdmlzaXQgPGEgaHJlZj0iaHR0cDovL3N1cHBvcnQuc3VnYXJjcm0uY29tIiB0YXJnZXQ9Il9uZXciPnN1cHBvcnQuc3VnYXJjcm0uY29tPC9hPiB0byBhY3F1aXJlIHRoZSBsYXRlc3QgdmVyc2lvbi4iO319'),
('Update', 'last_check_date', '1456336148'),
('system', 'adminwizard', '1'),
('notify', 'allow_default_outbound', '2'),
('proxy', 'on', '0'),
('proxy', 'host', ''),
('proxy', 'port', ''),
('proxy', 'auth', '0'),
('proxy', 'username', ''),
('proxy', 'password', ''),
('captcha', 'on', '0'),
('captcha', 'public_key', ''),
('captcha', 'private_key', ''),
('ldap', 'hostname', ''),
('ldap', 'port', ''),
('ldap', 'base_dn', ''),
('ldap', 'login_filter', ''),
('ldap', 'bind_attr', ''),
('ldap', 'login_attr', ''),
('ldap', 'group_dn', ''),
('ldap', 'group_name', ''),
('ldap', 'group_user_attr', ''),
('ldap', 'group_attr', ''),
('ldap', 'admin_user', ''),
('ldap', 'admin_password', ''),
('ldap', 'auto_create_users', '0'),
('ldap', 'enc_key', ''),
('system', 'ldap_enabled', '0'),
('ldap', 'group_attr_req_dn', '0'),
('ldap', 'group', '0'),
('ldap', 'authentication', '0'),
('MySettings', 'disable_useredit', 'no'),
('MySettings', 'hide_subpanels', 'YTowOnt9'),
('massemailer', 'email_copy', '2'),
('massemailer', 'campaign_emails_per_run', '500'),
('massemailer', 'tracking_entities_location_type', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `salutation`, `first_name`, `last_name`, `title`, `department`, `do_not_call`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `primary_address_street`, `primary_address_city`, `primary_address_state`, `primary_address_postalcode`, `primary_address_country`, `alt_address_street`, `alt_address_city`, `alt_address_state`, `alt_address_postalcode`, `alt_address_country`, `assistant`, `assistant_phone`, `lead_source`, `reports_to_id`, `birthdate`, `campaign_id`) VALUES
('572c47d0-be2b-c421-e235-56cf5c83e9f9', '2016-02-25 19:58:22', '2016-02-25 19:58:22', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', '', 0, '816c0648-ca93-1d95-11f9-56cf45208e84', 'Mr.', 'José', 'Pérez', '', '', 0, NULL, '', '958473108', NULL, '', 'AV constitución Nº46', 'Granada', 'Granada', '18020', 'España', '', '', '', '', '', NULL, NULL, ' ', '', NULL, ''),
('7c7f33c0-8c66-1dde-e8c9-56d04aeb8681', '2016-02-26 12:53:19', '2016-02-26 12:53:19', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '', 0, '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Ms.', 'Maria', 'Molina', '', '', 0, NULL, '', '', NULL, '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, ' ', '', NULL, ''),
('7fa89531-289c-bb16-72d6-56cf64ceb37a', '2016-02-25 20:30:57', '2016-02-25 20:30:57', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', '', 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Mr.', 'Jorge', 'Herrera', '', '', 0, '', '', '', '', '', 'C/ galicia nº13', '', 'Granada', '18230', 'España', '', '', '', '', '', '', '', 'Direct Mail', '', NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_audit`
--

CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_bugs`
--

CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_cases`
--

CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_users`
--

CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cron_remove_documents`
--

CREATE TABLE `cron_remove_documents` (
  `id` varchar(36) NOT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `currencies`
--

CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `symbol` varchar(36) DEFAULT NULL,
  `iso4217` varchar(3) DEFAULT NULL,
  `conversion_rate` double DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `custom_fields`
--

CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents`
--

CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT 'Sugar',
  `doc_url` varchar(255) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_accounts`
--

CREATE TABLE `documents_accounts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_bugs`
--

CREATE TABLE `documents_bugs` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_cases`
--

CREATE TABLE `documents_cases` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_contacts`
--

CREATE TABLE `documents_contacts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents_opportunities`
--

CREATE TABLE `documents_opportunities` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_revisions`
--

CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `doc_url` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eapm`
--

CREATE TABLE `eapm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `application` varchar(100) DEFAULT 'webex',
  `api_data` text,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `oauth_token` varchar(255) DEFAULT NULL,
  `oauth_secret` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emailman`
--

CREATE TABLE `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails`
--

CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `reply_to_status` tinyint(1) DEFAULT NULL,
  `intent` varchar(100) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails_beans`
--

CREATE TABLE `emails_beans` (
  `id` char(36) NOT NULL,
  `email_id` char(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails_email_addr_rel`
--

CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) DEFAULT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails_text`
--

CREATE TABLE `emails_text` (
  `email_id` char(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `reply_to_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_addresses`
--

CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `email_address_caps` varchar(255) DEFAULT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `email_addresses`
--

INSERT INTO `email_addresses` (`id`, `email_address`, `email_address_caps`, `invalid_email`, `opt_out`, `date_created`, `date_modified`, `deleted`) VALUES
('341a205f-be9c-986a-f6dc-56cdf52d8419', 'ps3josemiguel@gmail.com', 'PS3JOSEMIGUEL@GMAIL.COM', 0, 0, '2016-02-24 18:26:08', '2016-02-24 18:26:08', 0),
('845a1049-7ce0-ecb6-4790-56cf459ac44b', 'josemigueljaimezdiaz@gmail.com', 'JOSEMIGUELJAIMEZDIAZ@GMAIL.COM', 0, 0, '2016-02-25 18:19:07', '2016-02-25 18:19:07', 0),
('8c1c4603-c339-7f40-dc34-56cf642a60eb', 'JorgeHerrera@hotmail.com', 'JORGEHERRERA@HOTMAIL.COM', 0, 0, '2016-02-25 20:30:57', '2016-02-25 20:30:57', 0),
('b524b8dd-b347-6939-a357-56cee2bc5e84', 'josemi_9000@hotmail.com', 'JOSEMI_9000@HOTMAIL.COM', 0, 0, '2016-02-25 11:16:31', '2016-02-25 11:16:31', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_addr_bean_rel`
--

CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `email_addr_bean_rel`
--

INSERT INTO `email_addr_bean_rel` (`id`, `email_address_id`, `bean_id`, `bean_module`, `primary_address`, `reply_to_address`, `date_created`, `date_modified`, `deleted`) VALUES
('33dbabbc-a4ce-c4da-c352-56cdf5ed9037', '341a205f-be9c-986a-f6dc-56cdf52d8419', '1', 'Users', 1, 0, '2016-02-24 18:26:08', '2016-02-24 18:26:08', 0),
('845a1b10-439a-d823-2095-56cf4506e29d', '845a1049-7ce0-ecb6-4790-56cf459ac44b', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Users', 1, 0, '2016-02-25 18:19:07', '2016-02-25 18:19:07', 0),
('8bddce37-37ec-0ea3-f844-56cf645abd35', '8c1c4603-c339-7f40-dc34-56cf642a60eb', '7fa89531-289c-bb16-72d6-56cf64ceb37a', 'Contacts', 1, 0, '2016-02-25 20:30:57', '2016-02-25 20:30:57', 0),
('8d54ccd9-f5bb-e1f2-52c8-56cf6404eb1f', '8c1c4603-c339-7f40-dc34-56cf642a60eb', '7fe7189d-6ab1-6354-73c7-56cf64525f61', 'Accounts', 1, 0, '2016-02-25 20:30:57', '2016-02-25 20:30:57', 0),
('b524bbe7-69a0-21ce-4f70-56cee2cfa259', 'b524b8dd-b347-6939-a357-56cee2bc5e84', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'Users', 1, 0, '2016-02-25 11:16:31', '2016-02-25 12:19:51', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_cache`
--

CREATE TABLE `email_cache` (
  `ie_id` char(36) DEFAULT NULL,
  `mbox` varchar(60) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) UNSIGNED DEFAULT NULL,
  `imap_uid` int(10) UNSIGNED DEFAULT NULL,
  `msgno` int(10) UNSIGNED DEFAULT NULL,
  `recent` tinyint(4) DEFAULT NULL,
  `flagged` tinyint(4) DEFAULT NULL,
  `answered` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_marketing`
--

CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `email_marketing`
--

INSERT INTO `email_marketing` (`id`, `deleted`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `from_name`, `from_addr`, `reply_to_name`, `reply_to_addr`, `inbound_email_id`, `date_start`, `template_id`, `status`, `campaign_id`, `all_prospect_lists`) VALUES
('6bb1f9e7-ea27-a70a-9e0b-56d03de6d907', 0, '2016-02-26 11:56:47', '2016-02-26 11:56:47', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Boleting de campaña spa', 'Hoteles Albarracín', 'josemi_9000@yahoo.es', 'jose área de marketing', '', '50ac1c00-7971-0334-35b3-56cf670c115a', '2016-03-01 11:00:00', 'c12b8434-8370-0e77-fd0b-56d03d7b3cf6', 'active', 'df781430-6d7e-2794-5764-56d0380c6338', 0),
('ef6759b6-27c6-820e-bbb2-56d03e025446', 0, '2016-02-26 12:02:27', '2016-02-26 12:02:27', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Copia de  Boleting de campaña spa', 'Hoteles Albarracín', 'josemi_9000@yahoo.es', 'jose área de marketing', '', '50ac1c00-7971-0334-35b3-56cf670c115a', '2016-03-01 11:00:00', 'c12b8434-8370-0e77-fd0b-56d03d7b3cf6', 'active', 'df781430-6d7e-2794-5764-56d0380c6338', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_marketing_prospect_lists`
--

CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `email_templates`
--

CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `body_html` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `text_only` tinyint(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `email_templates`
--

INSERT INTO `email_templates` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `published`, `name`, `description`, `subject`, `body`, `body_html`, `deleted`, `assigned_user_id`, `text_only`, `type`) VALUES
('5febd664-6bcb-50b5-57eb-56cdeccb9e24', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'off', 'System-generated password email', 'This template is used when the System Administrator sends a new password to a user.', 'New account information', '\nHere is your account username and temporary password:\nUsername : $contact_user_user_name\nPassword : $contact_user_user_hash\n\n$config_site_url\n\nAfter you log in using the above password, you may be required to reset the password to one of your own choice.', '<div><table width="550"><tbody><tr><td><p>Here is your account username and temporary password:</p><p>Username : $contact_user_user_name </p><p>Password : $contact_user_user_hash </p><br /><p>$config_site_url</p><br /><p>After you log in using the above password, you may be required to reset the password to one of your own choice.</p>   </td>         </tr><tr><td></td>         </tr></tbody></table></div>', 0, NULL, 0, NULL),
('612450c5-fc43-7b64-6320-56cdecba05c5', '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'off', 'Forgot Password email', 'This template is used to send a user a link to click to reset the user''s account password.', 'Reset your account password', '\nYou recently requested on $contact_user_pwd_last_changed to be able to reset your account password.\n\nClick on the link below to reset your password:\n\n$contact_user_link_guid', '<div><table width="550"><tbody><tr><td><p>You recently requested on $contact_user_pwd_last_changed to be able to reset your account password. </p><p>Click on the link below to reset your password:</p><p> $contact_user_link_guid </p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>', 0, NULL, 0, NULL),
('c12b8434-8370-0e77-fd0b-56d03d7b3cf6', '2016-02-26 11:56:31', '2016-02-26 11:56:31', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'off', 'jose antonio', '', 'Ofertas Spa', '', '\r\n<p>$contact_salutation</p>\r\n', 0, NULL, 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fields_meta_data`
--

CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL,
  `ext3` varchar(255) DEFAULT NULL,
  `ext4` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fields_meta_data`
--

INSERT INTO `fields_meta_data` (`id`, `name`, `vname`, `comments`, `help`, `custom_module`, `type`, `len`, `required`, `default_value`, `date_modified`, `deleted`, `audited`, `massupdate`, `duplicate_merge`, `reportable`, `importable`, `ext1`, `ext2`, `ext3`, `ext4`) VALUES
('Accountsnif_c', 'nif_c', 'LBL_NIF', '', 'Ejemplo: 00000000G', 'Accounts', 'varchar', 10, 1, '', '2016-02-26 13:24:12', 0, 0, 0, 0, 1, 'true', '', '', '', ''),
('Usersdni_c', 'dni_c', 'LBL_DNI', NULL, 'Ejemplo: 11111111F', 'Users', 'varchar', 10, 1, '00000000X', '2016-02-25 13:17:26', 0, 0, 0, 0, 1, 'true', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folders`
--

CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `folders`
--

INSERT INTO `folders` (`id`, `name`, `folder_type`, `parent_folder`, `has_child`, `is_group`, `is_dynamic`, `dynamic_query`, `assign_to_id`, `created_by`, `modified_by`, `deleted`) VALUES
('c54f74fe-5f1f-253f-d5e3-56ce25d913db', 'Mi Correo', 'inbound', '', 1, 0, 1, 'SELECT emails.id polymorphic_id, ''Emails'' polymorphic_module FROM emails\n								   JOIN emails_text on emails.id = emails_text.email_id\n                                   WHERE (type = ''inbound'' OR status = ''inbound'') AND assigned_user_id = ''1'' AND emails.deleted = ''0'' AND status NOT IN (''sent'', ''archived'', ''draft'') AND type NOT IN (''out'', ''archived'', ''draft'')', '', '1', '1', 0),
('c7820847-4797-cdbc-d4c9-56ce25105bf2', 'Borradores', 'draft', 'c54f74fe-5f1f-253f-d5e3-56ce25d913db', 0, 0, 1, 'SELECT emails.id polymorphic_id, ''Emails'' polymorphic_module FROM emails\n								   JOIN emails_text on emails.id = emails_text.email_id\n                                   WHERE (type = ''draft'' OR status = ''draft'') AND assigned_user_id = ''1'' AND emails.deleted = ''0'' AND status NOT IN (''archived'') AND type NOT IN (''archived'')', '', '1', '1', 0),
('c9b48352-fff2-0399-0de7-56ce2596d971', 'Emails Enviados', 'sent', 'c54f74fe-5f1f-253f-d5e3-56ce25d913db', 0, 0, 1, 'SELECT emails.id polymorphic_id, ''Emails'' polymorphic_module FROM emails\n								   JOIN emails_text on emails.id = emails_text.email_id\n                                   WHERE (type = ''out'' OR status = ''sent'') AND assigned_user_id = ''1'' AND emails.deleted = ''0'' AND status NOT IN (''archived'') AND type NOT IN (''archived'')', '', '1', '1', 0),
('cf522b52-9af8-9ac3-00f0-56ce25d43942', 'Mis Emails Archivados', 'archived', 'c54f74fe-5f1f-253f-d5e3-56ce25d913db', 0, 0, 1, '', '', '1', '1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folders_rel`
--

CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) DEFAULT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folders_subscriptions`
--

CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `folders_subscriptions`
--

INSERT INTO `folders_subscriptions` (`id`, `folder_id`, `assigned_user_id`) VALUES
('c54f741e-dd6e-fafa-133a-56ce257c6653', 'c54f74fe-5f1f-253f-d5e3-56ce25d913db', '1'),
('c7820469-95b4-ee30-c21e-56ce25730315', 'c7820847-4797-cdbc-d4c9-56ce25105bf2', '1'),
('c9b48bbc-f557-b1fb-da6f-56ce2541c700', 'c9b48352-fff2-0399-0de7-56ce2596d971', '1'),
('cf522f83-6722-2af0-accc-56ce25b4be05', 'cf522b52-9af8-9ac3-00f0-56ce25d43942', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `import_maps`
--

CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `source` varchar(36) DEFAULT NULL,
  `enclosure` varchar(1) DEFAULT ' ',
  `delimiter` varchar(1) DEFAULT ',',
  `module` varchar(36) DEFAULT NULL,
  `content` text,
  `default_values` text,
  `has_header` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inbound_email`
--

CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `server_url` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `email_password` varchar(100) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `mailbox` text,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `inbound_email`
--

INSERT INTO `inbound_email` (`id`, `deleted`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `status`, `server_url`, `email_user`, `email_password`, `port`, `service`, `mailbox`, `delete_seen`, `mailbox_type`, `template_id`, `stored_options`, `group_id`, `is_personal`, `groupfolder_id`) VALUES
('50ac1c00-7971-0334-35b3-56cf670c115a', 0, '2016-02-25 20:42:50', '2016-02-25 20:42:50', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Hoteles Albarracín', 'Active', 'smtp.mail.yahoo.com', 'josemi_9000@yahoo.es', 'QqZ2O3LSeb33DmVYiSb0oA==', 143, '::::::imap::::', 'INBOX', 0, 'bounce', NULL, 'YToxMTp7czo5OiJmcm9tX25hbWUiO3M6MTk6IkhvdGVsZXMgQWxiYXJyYWPDrW4iO3M6OToiZnJvbV9hZGRyIjtzOjIwOiJqb3NlbWlfOTAwMEB5YWhvby5lcyI7czoxMzoicmVwbHlfdG9fbmFtZSI7czowOiIiO3M6MTM6InJlcGx5X3RvX2FkZHIiO3M6MDoiIjtzOjEwOiJvbmx5X3NpbmNlIjtiOjE7czoxMzoiZmlsdGVyX2RvbWFpbiI7TjtzOjMwOiJlbWFpbF9udW1fYXV0b3JlcGxpZXNfMjRfaG91cnMiO047czoyNjoiYWxsb3dfb3V0Ym91bmRfZ3JvdXBfdXNhZ2UiO2I6MDtzOjExOiJ0cmFzaEZvbGRlciI7czowOiIiO3M6MTA6InNlbnRGb2xkZXIiO3M6MDoiIjtzOjEyOiJpc0F1dG9JbXBvcnQiO2I6MDt9', '4958fef5-1e58-10ed-11ea-56cf67cb83cd', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inbound_email_autoreply`
--

CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `autoreplied_to` varchar(100) DEFAULT NULL,
  `ie_id` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inbound_email_cache_ts`
--

CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `job_queue`
--

CREATE TABLE `job_queue` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `scheduler_id` char(36) DEFAULT NULL,
  `execute_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `resolution` varchar(20) DEFAULT NULL,
  `message` text,
  `target` varchar(255) DEFAULT NULL,
  `data` text,
  `requeue` tinyint(1) DEFAULT '0',
  `retry_count` tinyint(4) DEFAULT NULL,
  `failure_count` tinyint(4) DEFAULT NULL,
  `job_delay` int(11) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leads`
--

CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `leads`
--

INSERT INTO `leads` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `salutation`, `first_name`, `last_name`, `title`, `department`, `do_not_call`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `primary_address_street`, `primary_address_city`, `primary_address_state`, `primary_address_postalcode`, `primary_address_country`, `alt_address_street`, `alt_address_city`, `alt_address_state`, `alt_address_postalcode`, `alt_address_country`, `assistant`, `assistant_phone`, `converted`, `refered_by`, `lead_source`, `lead_source_description`, `status`, `status_description`, `reports_to_id`, `account_name`, `account_description`, `contact_id`, `account_id`, `opportunity_id`, `opportunity_name`, `opportunity_amount`, `campaign_id`, `birthdate`, `portal_name`, `portal_app`, `website`) VALUES
('84ee4c6a-0149-067a-a81d-56cf61a35c8a', '2016-02-25 20:20:03', '2016-02-25 20:30:57', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Mr.', 'Carlos', 'Gomez', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Direct Mail', 'Llamada a la tienda\r\npara solicitar información acerca de las nuevas camas acuáticas.', 'Converted', 'El viernes 04/03/2016 Salvador Gallego lo\r\nllamara y concierta una cita para el miércoles 09/03/2016', '', NULL, NULL, '7fa89531-289c-bb16-72d6-56cf64ceb37a', '7fe7189d-6ab1-6354-73c7-56cf64525f61', '', NULL, NULL, '', NULL, NULL, NULL, 'http://');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leads_audit`
--

CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `leads_audit`
--

INSERT INTO `leads_audit` (`id`, `parent_id`, `date_created`, `created_by`, `field_name`, `data_type`, `before_value_string`, `after_value_string`, `before_value_text`, `after_value_text`) VALUES
('8f48df3b-0133-b670-881d-56cf64f713cf', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', '2016-02-25 20:30:57', '816c0648-ca93-1d95-11f9-56cf45208e84', 'status', 'enum', 'In Process', 'Converted', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linked_documents`
--

CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meetings`
--

CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `join_url` varchar(200) DEFAULT NULL,
  `host_url` varchar(400) DEFAULT NULL,
  `displayed_url` varchar(400) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `external_id` varchar(50) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `type` varchar(255) DEFAULT 'Sugar',
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `meetings`
--

INSERT INTO `meetings` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `location`, `password`, `join_url`, `host_url`, `displayed_url`, `creator`, `external_id`, `duration_hours`, `duration_minutes`, `date_start`, `date_end`, `parent_type`, `status`, `type`, `parent_id`, `reminder_time`, `email_reminder_time`, `email_reminder_sent`, `outlook_id`, `sequence`, `repeat_type`, `repeat_interval`, `repeat_dow`, `repeat_until`, `repeat_count`, `repeat_parent_id`, `recurring_source`) VALUES
('292438af-9f76-9fe3-8b55-56cf5fe18bad', 'Propuesta para oportunidad', '2016-02-25 20:09:52', '2016-02-25 20:09:59', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Esta reunión es importante para la resolución de un importante negocio con un cliente potencial.', 0, '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 15, '2016-03-03 07:00:00', '2016-03-03 09:15:00', 'Opportunities', 'Planned', 'Sugar', '', 1800, -1, 0, NULL, 0, ' ', 1, NULL, NULL, 10, '', NULL),
('c6c6d62c-7c06-e9e8-93bc-56d04ffc6576', 'Reunión General', '2016-02-26 13:12:02', '2016-02-26 13:14:45', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, '816c0648-ca93-1d95-11f9-56cf45208e84', 'Sala 23', NULL, NULL, NULL, NULL, NULL, NULL, 2, 30, '2016-03-03 08:00:00', '2016-03-03 10:30:00', 'Accounts', 'Planned', 'Sugar', 'a9f6a964-b67a-e5cd-809b-56d04ab03144', 1800, -1, 0, NULL, 0, NULL, 1, NULL, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meetings_contacts`
--

CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meetings_leads`
--

CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meetings_users`
--

CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `meetings_users`
--

INSERT INTO `meetings_users` (`id`, `meeting_id`, `user_id`, `required`, `accept_status`, `date_modified`, `deleted`) VALUES
('2c124143-3d17-e513-d8a1-56cf5f96ef12', '292438af-9f76-9fe3-8b55-56cf5fe18bad', '816c0648-ca93-1d95-11f9-56cf45208e84', '1', 'accept', '2016-02-25 20:09:52', 0),
('cd1fe6d7-d74e-7497-e8e8-56d04ff3a854', 'c6c6d62c-7c06-e9e8-93bc-56d04ffc6576', '816c0648-ca93-1d95-11f9-56cf45208e84', '1', 'accept', '2016-02-26 13:12:02', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notes`
--

CREATE TABLE `notes` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) DEFAULT NULL,
  `embed_flag` tinyint(1) DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_consumer`
--

CREATE TABLE `oauth_consumer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  `c_secret` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_nonce`
--

CREATE TABLE `oauth_nonce` (
  `conskey` varchar(32) NOT NULL,
  `nonce` varchar(32) NOT NULL,
  `nonce_ts` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_tokens`
--

CREATE TABLE `oauth_tokens` (
  `id` char(36) NOT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `tstate` varchar(1) DEFAULT NULL,
  `consumer` char(36) NOT NULL,
  `token_ts` bigint(20) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `callback_url` varchar(255) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opportunities`
--

CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_usdollar` double DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `date_closed` date DEFAULT NULL,
  `next_step` varchar(100) DEFAULT NULL,
  `sales_stage` varchar(255) DEFAULT NULL,
  `probability` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `opportunities`
--

INSERT INTO `opportunities` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `opportunity_type`, `campaign_id`, `lead_source`, `amount`, `amount_usdollar`, `currency_id`, `date_closed`, `next_step`, `sales_stage`, `probability`) VALUES
('10fafcd7-13da-d3cc-e185-56d04b8f4d19', 'Oferta Spa', '2016-02-26 12:55:49', '2016-02-26 12:56:54', '409c1178-a159-5926-566a-56cf4fe4f450', '409c1178-a159-5926-566a-56cf4fe4f450', NULL, 0, '409c1178-a159-5926-566a-56cf4fe4f450', ' ', '', ' ', 12000, 12000, '-99', '2016-03-11', NULL, 'Closed Won', 100),
('cc0cbc57-0f93-b561-06c0-56cf5d092089', 'Servicio', '2016-02-25 20:02:39', '2016-02-25 20:02:39', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', '', 0, '816c0648-ca93-1d95-11f9-56cf45208e84', 'Existing Business', '', 'Email', 12, 12, '-99', '2016-03-04', '', 'Proposal/Price Quote', 70);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opportunities_audit`
--

CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `opportunities_audit`
--

INSERT INTO `opportunities_audit` (`id`, `parent_id`, `date_created`, `created_by`, `field_name`, `data_type`, `before_value_string`, `after_value_string`, `before_value_text`, `after_value_text`) VALUES
('72cdb88e-0dc4-bb77-3488-56d04b0b4847', '10fafcd7-13da-d3cc-e185-56d04b8f4d19', '2016-02-26 12:56:54', '409c1178-a159-5926-566a-56cf4fe4f450', 'amount_usdollar', 'currency', '12', '12000', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opportunities_contacts`
--

CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outbound_email`
--

CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(15) DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `mail_sendtype` varchar(8) DEFAULT 'smtp',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` int(5) DEFAULT '0',
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `outbound_email`
--

INSERT INTO `outbound_email` (`id`, `name`, `type`, `user_id`, `mail_sendtype`, `mail_smtptype`, `mail_smtpserver`, `mail_smtpport`, `mail_smtpuser`, `mail_smtppass`, `mail_smtpauth_req`, `mail_smtpssl`) VALUES
('545ad33a-fc73-b5a7-c479-56cdecc27d11', 'system', 'system', '1', 'SMTP', 'yahoomail', 'smtp.mail.yahoo.com', 465, 'josemi_9000@yahoo.es', 'tACPx7IRnr9oWQOb69DDIA==', 1, 1),
('a502d894-1c69-0c6c-b32c-56cedbc6b93d', 'josemi', 'user', '1', 'SMTP', 'yahoomail', 'smtp.mail.yahoo.com', 465, 'josemi_9000@yahoo.es', 'tACPx7IRnr9oWQOb69DDIA==', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_accounts`
--

CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_bugs`
--

CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_cases`
--

CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_contacts`
--

CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_opportunities`
--

CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_products`
--

CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_task`
--

CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `project_id` char(36) NOT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `duration_unit` text,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `time_due` time DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT NULL,
  `order_number` int(11) DEFAULT '1',
  `task_number` int(11) DEFAULT NULL,
  `estimated_effort` int(11) DEFAULT NULL,
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `utilization` int(11) DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project_task_audit`
--

CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospects`
--

CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospect_lists`
--

CREATE TABLE `prospect_lists` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prospect_lists`
--

INSERT INTO `prospect_lists` (`assigned_user_id`, `id`, `name`, `list_type`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `deleted`, `description`, `domain_name`) VALUES
('6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'db846b79-3b32-7208-e565-56d032684fac', 'Personas residentes en Granada', 'default', '2016-02-26 11:11:37', '2016-02-26 12:26:51', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 0, 'Va orientado para gente que esta residiendo en Granada.', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospect_lists_prospects`
--

CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prospect_lists_prospects`
--

INSERT INTO `prospect_lists_prospects` (`id`, `prospect_list_id`, `related_id`, `related_type`, `date_modified`, `deleted`) VALUES
('3e9247cb-38e0-0daf-79f5-56d044dc9314', 'db846b79-3b32-7208-e565-56d032684fac', '572c47d0-be2b-c421-e235-56cf5c83e9f9', 'Contacts', '2016-02-26 12:26:51', 0),
('a8a7246e-439e-7a96-48ab-56d044c513b4', 'db846b79-3b32-7208-e565-56d032684fac', '7fa89531-289c-bb16-72d6-56cf64ceb37a', 'Contacts', '2016-02-26 12:26:42', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospect_list_campaigns`
--

CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prospect_list_campaigns`
--

INSERT INTO `prospect_list_campaigns` (`id`, `prospect_list_id`, `campaign_id`, `date_modified`, `deleted`) VALUES
('e16c2c1f-60f6-c13a-bd08-56d03804beaf', 'db846b79-3b32-7208-e565-56d032684fac', 'df781430-6d7e-2794-5764-56d0380c6338', '2016-02-26 11:33:49', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relationships`
--

CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(128) DEFAULT NULL,
  `join_key_lhs` varchar(128) DEFAULT NULL,
  `join_key_rhs` varchar(128) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `relationships`
--

INSERT INTO `relationships` (`id`, `relationship_name`, `lhs_module`, `lhs_table`, `lhs_key`, `rhs_module`, `rhs_table`, `rhs_key`, `join_table`, `join_key_lhs`, `join_key_rhs`, `relationship_type`, `relationship_role_column`, `relationship_role_column_value`, `reverse`, `deleted`) VALUES
('101a5bdf-bed7-5d3f-af50-56d051a35b5a', 'prospects_modified_user', 'Users', 'users', 'id', 'Prospects', 'prospects', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('101a5ee6-23e6-6912-3f99-56d0510314c6', 'prospects_created_by', 'Users', 'users', 'id', 'Prospects', 'prospects', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1058de1b-1afa-1d87-3c06-56d051bf4c5a', 'prospects_assigned_user', 'Users', 'users', 'id', 'Prospects', 'prospects', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('10975d17-c6fd-fbe1-5e90-56d0514555e5', 'prospects_email_addresses', 'Prospects', 'prospects', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Prospects', 0, 0),
('10d5d31e-c919-db82-145f-56d05144e5ef', 'prospects_email_addresses_primary', 'Prospects', 'prospects', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('10d5d845-9bfa-df74-1015-56d051f39af7', 'prospect_tasks', 'Prospects', 'prospects', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('11145019-2663-def0-ead6-56d051a0773a', 'prospect_notes', 'Prospects', 'prospects', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('1152dae0-758c-8242-2cf1-56d05163911b', 'prospect_meetings', 'Prospects', 'prospects', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('11915191-4fe2-1d86-da80-56d05191e9b4', 'prospect_calls', 'Prospects', 'prospects', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('11915f25-5191-bb61-b20f-56d051842808', 'prospect_emails', 'Prospects', 'prospects', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('11cfd627-5820-b18d-efc2-56d051802625', 'prospect_campaign_log', 'Prospects', 'prospects', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Prospects', 0, 0),
('13590594-9bf3-3119-e966-56d051e1f00e', 'projects_emails', 'Project', 'project', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('1385648f-9e2e-afad-272f-56d051b17b12', 'email_template_email_marketings', 'EmailTemplates', 'email_templates', 'id', 'EmailMarketing', 'email_marketing', 'template_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('14fc67cb-d411-abaf-365b-56d05149b866', 'campaign_campaigntrakers', 'Campaigns', 'campaigns', 'id', 'CampaignTrackers', 'campaign_trkrs', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('17410edb-5231-f8ec-2358-56d0516a1b69', 'projects_project_tasks', 'Project', 'project', 'id', 'ProjectTask', 'project_task', 'project_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1b2a08f4-0159-18e8-bdb5-56d051589598', 'projects_assigned_user', 'Users', 'users', 'id', 'Project', 'project', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1c4f87f6-3b56-a2e2-3de1-56d05121f6db', 'schedulers_created_by_rel', 'Users', 'users', 'id', 'Schedulers', 'schedulers', 'created_by', NULL, NULL, NULL, 'one-to-one', NULL, NULL, 0, 0),
('1c8e0ac5-1b0b-e833-a734-56d051e7f26b', 'schedulers_modified_user_id_rel', 'Users', 'users', 'id', 'Schedulers', 'schedulers', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1ccc84c8-553d-03fd-0cca-56d051005426', 'schedulers_jobs_rel', 'Schedulers', 'schedulers', 'id', 'SchedulersJobs', 'job_queue', 'scheduler_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1d498993-fad9-8a37-2158-56d0512e5b48', 'schedulersjobs_assigned_user', 'Users', 'users', 'id', 'SchedulersJobs', 'schedulersjobs', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('20761caa-407b-2e77-dc5e-56d05152b0a6', 'contacts_modified_user', 'Users', 'users', 'id', 'Contacts', 'contacts', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('20b49c4f-66cc-d8e8-35a0-56d051f10222', 'contacts_created_by', 'Users', 'users', 'id', 'Contacts', 'contacts', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('20f31bff-a10f-00ed-d37b-56d051b9addb', 'contacts_assigned_user', 'Users', 'users', 'id', 'Contacts', 'contacts', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('20f31cbf-f338-9dd5-c2de-56d0517d4b49', 'contacts_email_addresses', 'Contacts', 'contacts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Contacts', 0, 0),
('21319fb2-c1b3-bbe0-4228-56d051b7377f', 'contacts_email_addresses_primary', 'Contacts', 'contacts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('217011de-b7e5-9068-39ca-56d051802455', 'contact_direct_reports', 'Contacts', 'contacts', 'id', 'Contacts', 'contacts', 'reports_to_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('21701854-effe-669b-cba0-56d05104f498', 'contact_leads', 'Contacts', 'contacts', 'id', 'Leads', 'leads', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('21ae93ce-dc56-9e87-6775-56d05164c169', 'contact_notes', 'Contacts', 'contacts', 'id', 'Notes', 'notes', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('21ed12b1-e771-7470-d3cd-56d051ff8faf', 'contact_tasks', 'Contacts', 'contacts', 'id', 'Tasks', 'tasks', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('21ed1565-9a1d-2ea1-9adc-56d051becd9d', 'contact_tasks_parent', 'Contacts', 'contacts', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Contacts', 0, 0),
('222b93e6-bb28-b156-4962-56d051fc16c8', 'contact_notes_parent', 'Contacts', 'contacts', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Contacts', 0, 0),
('222b9700-a783-c896-9309-56d051c5ac3d', 'contact_campaign_log', 'Contacts', 'contacts', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Contacts', 0, 0),
('22fa0271-5ed6-3163-a3f5-56d051ed3210', 'projects_modified_user', 'Users', 'users', 'id', 'Project', 'project', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('249ca026-94c7-74c5-ad87-56d051974f5c', 'accounts_modified_user', 'Users', 'users', 'id', 'Accounts', 'accounts', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('24db2ce4-6803-1ebc-2b12-56d051d8e96b', 'accounts_created_by', 'Users', 'users', 'id', 'Accounts', 'accounts', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('24db2f66-e1a0-7131-cf2c-56d051e7c38e', 'accounts_assigned_user', 'Users', 'users', 'id', 'Accounts', 'accounts', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2519a9c6-a1a2-6583-c063-56d0512abfe8', 'accounts_email_addresses', 'Accounts', 'accounts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Accounts', 0, 0),
('25582546-1425-ab13-e894-56d051efb307', 'member_accounts', 'Accounts', 'accounts', 'id', 'Accounts', 'accounts', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('25582775-9f09-88a1-b13e-56d0516a03c9', 'accounts_email_addresses_primary', 'Accounts', 'accounts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('2596ad78-550e-ee40-8dcf-56d051b77336', 'account_cases', 'Accounts', 'accounts', 'id', 'Cases', 'cases', 'account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('25d52a1f-8950-ad07-586a-56d051877436', 'account_tasks', 'Accounts', 'accounts', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('25d52e15-f8f7-1c30-d634-56d051f01d07', 'account_notes', 'Accounts', 'accounts', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('2613af4d-3a2d-e4dc-cf0b-56d0512cb89e', 'account_meetings', 'Accounts', 'accounts', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('26522092-a684-d6c7-a441-56d051c127ca', 'account_emails', 'Accounts', 'accounts', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('26522c42-893c-fcb0-c3f1-56d051cab96b', 'account_calls', 'Accounts', 'accounts', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('2690a1c9-623d-888b-6757-56d051695a92', 'account_campaign_log', 'Accounts', 'accounts', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Accounts', 0, 0),
('2690a3d3-404c-f83e-61d8-56d051b7302c', 'account_leads', 'Accounts', 'accounts', 'id', 'Leads', 'leads', 'account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('29403ca4-952e-f4b4-532e-56d051346ab6', 'opportunities_created_by', 'Users', 'users', 'id', 'Opportunities', 'opportunities', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('29403ebb-0246-01f0-a69b-56d05101cdc5', 'opportunities_modified_user', 'Users', 'users', 'id', 'Opportunities', 'opportunities', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('297eb3ce-3f74-6e3b-0007-56d05156ca72', 'opportunities_assigned_user', 'Users', 'users', 'id', 'Opportunities', 'opportunities', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('29bd3f65-5f4f-0b56-f182-56d051ea001f', 'opportunity_calls', 'Opportunities', 'opportunities', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('29fbba62-18d5-143f-2224-56d0513455f6', 'opportunity_tasks', 'Opportunities', 'opportunities', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('29fbbe1c-a122-81c8-e0ec-56d0518f1fa6', 'opportunity_meetings', 'Opportunities', 'opportunities', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('2a3a3009-abb5-e7f4-8656-56d0512ad659', 'opportunity_emails', 'Opportunities', 'opportunities', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('2a3a369b-f70c-7ae6-bf7d-56d05187b890', 'opportunity_notes', 'Opportunities', 'opportunities', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('2a78b842-af56-6f54-83de-56d0515d5c27', 'opportunity_leads', 'Opportunities', 'opportunities', 'id', 'Leads', 'leads', 'opportunity_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2a78ba45-5f19-9966-d796-56d05128bd45', 'opportunity_currencies', 'Opportunities', 'opportunities', 'currency_id', 'Currencies', 'currencies', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2ab73214-0df7-b802-b857-56d051e396b5', 'opportunities_campaign', 'Campaigns', 'campaigns', 'id', 'Opportunities', 'opportunities', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2aca04bd-5f37-9fb2-c357-56d051f64f4b', 'projects_created_by', 'Users', 'users', 'id', 'Project', 'project', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2cab48e1-b4b8-ad41-71bf-56d051650278', 'emailtemplates_assigned_user', 'Users', 'users', 'id', 'EmailTemplates', 'email_templates', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2e9f435c-8ab0-86d2-14ff-56d0515e079d', 'notes_assigned_user', 'Users', 'users', 'id', 'Notes', 'notes', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2eddcd38-6453-8a22-7b88-56d0519fb417', 'notes_modified_user', 'Users', 'users', 'id', 'Notes', 'notes', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2f1c422a-e1ef-1593-fefd-56d05168d973', 'notes_created_by', 'Users', 'users', 'id', 'Notes', 'notes', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('318d5352-8b6f-4200-13db-56d051e15668', 'calls_modified_user', 'Users', 'users', 'id', 'Calls', 'calls', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('31cbd35c-c2b0-5b28-bee9-56d0518ec825', 'calls_created_by', 'Users', 'users', 'id', 'Calls', 'calls', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('320a51ac-b726-bb87-6cb4-56d05154e8dd', 'calls_assigned_user', 'Users', 'users', 'id', 'Calls', 'calls', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('320a5592-68f1-ba8c-3672-56d05163d045', 'calls_notes', 'Calls', 'calls', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Calls', 0, 0),
('35f26f80-5f41-825f-eb96-56d0513874c7', 'emails_assigned_user', 'Users', 'users', 'id', 'Emails', 'emails', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3630e4e3-d49f-a18a-989a-56d051e986d5', 'emails_created_by', 'Users', 'users', 'id', 'Emails', 'emails', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3630ee08-381f-4f17-e92b-56d05103baed', 'emails_modified_user', 'Users', 'users', 'id', 'Emails', 'emails', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('366f66b0-8e6b-0cd2-72f2-56d05112f352', 'emails_notes_rel', 'Emails', 'emails', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('36ade944-b67d-e341-1b18-56d0513a4e4f', 'emails_accounts_rel', 'Emails', 'emails', 'id', 'Accounts', 'accounts', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Accounts', 0, 0),
('36aded1c-b240-9c5b-f175-56d0516699da', 'emails_contacts_rel', 'Emails', 'emails', 'id', 'Contacts', 'contacts', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Contacts', 0, 0),
('36ec614f-01b9-df4a-48c4-56d0516974f2', 'emails_leads_rel', 'Emails', 'emails', 'id', 'Leads', 'leads', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Leads', 0, 0),
('36ec6753-a1b5-3516-6cd4-56d051d19711', 'emails_meetings_rel', 'Emails', 'emails', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('399bf797-d739-1263-86e6-56d0510bca99', 'meetings_modified_user', 'Users', 'users', 'id', 'Meetings', 'meetings', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('39da7599-26c5-e6d7-14b3-56d05192e38e', 'meetings_created_by', 'Users', 'users', 'id', 'Meetings', 'meetings', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3a18fa5b-cc5b-675f-2a00-56d051ecd3e2', 'meetings_assigned_user', 'Users', 'users', 'id', 'Meetings', 'meetings', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3a5770d0-f287-b310-1f31-56d051debe67', 'meetings_notes', 'Meetings', 'meetings', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Meetings', 0, 0),
('3c89f11f-cf40-c2dc-68dd-56d0513d549d', 'tasks_modified_user', 'Users', 'users', 'id', 'Tasks', 'tasks', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3c89fedf-58f8-28b1-362c-56d051f8f02a', 'tasks_created_by', 'Users', 'users', 'id', 'Tasks', 'tasks', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3cc87fb8-4dc5-e58c-4729-56d051674ecf', 'tasks_assigned_user', 'Users', 'users', 'id', 'Tasks', 'tasks', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3d06fd58-70fd-04aa-90d6-56d051a7ea96', 'tasks_notes', 'Tasks', 'tasks', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('493c2b78-689b-8afe-9fbc-56d05127930a', 'documents_modified_user', 'Users', 'users', 'id', 'Documents', 'documents', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('497aa660-cf66-f883-0958-56d051b26e4a', 'documents_created_by', 'Users', 'users', 'id', 'Documents', 'documents', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('49b9232d-4d28-8671-d1d8-56d0511bd0d5', 'documents_assigned_user', 'Users', 'users', 'id', 'Documents', 'documents', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('49f7a5bb-311b-3b84-a3c1-56d0517b764b', 'document_revisions', 'Documents', 'documents', 'id', 'DocumentRevisions', 'document_revisions', 'document_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('4bebb68d-4161-d5e3-32c1-56d0512d3871', 'revisions_created_by', 'Users', 'users', 'id', 'DocumentRevisions', 'document_revisions', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('510c478e-15b5-630d-d429-56d0516037e1', 'inbound_email_created_by', 'Users', 'users', 'id', 'InboundEmail', 'inbound_email', 'created_by', NULL, NULL, NULL, 'one-to-one', NULL, NULL, 0, 0),
('514acaca-f4b9-19ea-5256-56d0512876dc', 'inbound_email_modified_user_id', 'Users', 'users', 'id', 'InboundEmail', 'inbound_email', 'modified_user_id', NULL, NULL, NULL, 'one-to-one', NULL, NULL, 0, 0),
('51da0e85-ca53-fd78-0f41-56d051a351ea', 'project_tasks_notes', 'ProjectTask', 'project_task', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('533ed4a2-5787-c858-13ca-56d05107954d', 'saved_search_assigned_user', 'Users', 'users', 'id', 'SavedSearch', 'saved_search', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('55c20ca4-f7b4-4f5c-3d95-56d051a35d79', 'project_tasks_tasks', 'ProjectTask', 'project_task', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('57e26775-c1c8-ff73-8c6f-56d051f96be7', 'sugarfeed_modified_user', 'Users', 'users', 'id', 'SugarFeed', 'sugarfeed', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('5820eaa6-410f-7744-6a84-56d051519106', 'sugarfeed_created_by', 'Users', 'users', 'id', 'SugarFeed', 'sugarfeed', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('585f6bf6-af4d-4b21-f35e-56d0516d11b1', 'sugarfeed_assigned_user', 'Users', 'users', 'id', 'SugarFeed', 'sugarfeed', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('59aa00ee-5606-318b-cac3-56d0513113cc', 'project_tasks_calls', 'ProjectTask', 'project_task', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('59aa0cb5-effa-ed2c-6f84-56d05111d3ee', 'project_tasks_meetings', 'ProjectTask', 'project_task', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('5d920b23-0521-7b12-94c5-56d05182c3dd', 'project_tasks_emails', 'ProjectTask', 'project_task', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('5dfcf623-e69b-136b-619d-56d05194404e', 'eapm_modified_user', 'Users', 'users', 'id', 'EAPM', 'eapm', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('5e3b7ec4-ac00-eaf0-72f2-56d051a6e029', 'eapm_created_by', 'Users', 'users', 'id', 'EAPM', 'eapm', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('5e79fc2d-bb43-6394-b1cc-56d05129b850', 'eapm_assigned_user', 'Users', 'users', 'id', 'EAPM', 'eapm', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('61680269-1dd3-2b09-d131-56d0510c8147', 'oauthkeys_created_by', 'Users', 'users', 'id', 'OAuthKeys', 'oauthkeys', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('6168074d-f9b6-5e6c-d3fa-56d05100e76b', 'oauthkeys_modified_user', 'Users', 'users', 'id', 'OAuthKeys', 'oauthkeys', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('617b0b8a-9bf3-385c-6fc1-56d051f48337', 'project_tasks_assigned_user', 'Users', 'users', 'id', 'ProjectTask', 'project_task', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('61a682d0-7ab2-4020-3259-56d0519f3a16', 'oauthkeys_assigned_user', 'Users', 'users', 'id', 'OAuthKeys', 'oauthkeys', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('635c088e-6974-100e-50f4-56d051eea03d', 'consumer_tokens', 'OAuthKeys', 'oauth_consumer', 'id', 'OAuthTokens', 'oauth_tokens', 'consumer', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('63d91d52-a99f-c4c3-26c4-56d0519e6738', 'oauthtokens_assigned_user', 'Users', 'users', 'id', 'OAuthTokens', 'oauth_tokens', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('65630261-2240-8722-5ac1-56d0515af93e', 'project_tasks_created_by', 'Users', 'users', 'id', 'ProjectTask', 'project_task', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('65630b98-91e2-8cff-a16c-56d0510c7887', 'project_tasks_modified_user', 'Users', 'users', 'id', 'ProjectTask', 'project_task', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('6c64b498-7721-92ae-ca61-56d05183ab70', 'accounts_bugs', 'Accounts', 'accounts', 'id', 'Bugs', 'bugs', 'id', 'accounts_bugs', 'account_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('6ca3373b-e691-4ce8-21e5-56d051a35cdf', 'accounts_opportunities', 'Accounts', 'accounts', 'id', 'Opportunities', 'opportunities', 'id', 'accounts_opportunities', 'account_id', 'opportunity_id', 'many-to-many', NULL, NULL, 0, 0),
('6ca33caa-b5fe-012a-57a0-56d051f43087', 'accounts_contacts', 'Accounts', 'accounts', 'id', 'Contacts', 'contacts', 'id', 'accounts_contacts', 'account_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('6ce1baf6-0bea-3aad-29c6-56d05180f7a5', 'calls_contacts', 'Calls', 'calls', 'id', 'Contacts', 'contacts', 'id', 'calls_contacts', 'call_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('6d203167-a143-af99-da37-56d051a805fd', 'calls_users', 'Calls', 'calls', 'id', 'Users', 'users', 'id', 'calls_users', 'call_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('6d2039d7-ec3f-dcc0-e5c7-56d051be9ffb', 'calls_leads', 'Calls', 'calls', 'id', 'Leads', 'leads', 'id', 'calls_leads', 'call_id', 'lead_id', 'many-to-many', NULL, NULL, 0, 0),
('6d5eb0b3-e673-d99f-dc39-56d0516763e4', 'cases_bugs', 'Cases', 'cases', 'id', 'Bugs', 'bugs', 'id', 'cases_bugs', 'case_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('6d9d3940-e2f4-dd7e-0488-56d05102cc1f', 'contacts_bugs', 'Contacts', 'contacts', 'id', 'Bugs', 'bugs', 'id', 'contacts_bugs', 'contact_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('6d9d3a85-3d9c-da55-29c8-56d0516e0331', 'contacts_cases', 'Contacts', 'contacts', 'id', 'Cases', 'cases', 'id', 'contacts_cases', 'contact_id', 'case_id', 'many-to-many', NULL, NULL, 0, 0),
('6ddbbcb4-7953-f870-12d0-56d05153a039', 'contacts_users', 'Contacts', 'contacts', 'id', 'Users', 'users', 'id', 'contacts_users', 'contact_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('6e1a324b-86fd-e185-a4df-56d05192e604', 'emails_cases_rel', 'Emails', 'emails', 'id', 'Cases', 'cases', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Cases', 0, 0),
('6e1a3e06-2a9c-2296-19fd-56d051db40f7', 'emails_bugs_rel', 'Emails', 'emails', 'id', 'Bugs', 'bugs', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Bugs', 0, 0),
('6e97350e-86f1-f7f7-fc0f-56d0510cbd07', 'emails_opportunities_rel', 'Emails', 'emails', 'id', 'Opportunities', 'opportunities', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Opportunities', 0, 0),
('6e9739a4-fe4b-be47-674c-56d051b13243', 'emails_tasks_rel', 'Emails', 'emails', 'id', 'Tasks', 'tasks', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Tasks', 0, 0),
('6ed5b329-2492-b3ff-57ad-56d05126d808', 'emails_project_task_rel', 'Emails', 'emails', 'id', 'ProjectTask', 'project_task', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'ProjectTask', 0, 0),
('6ed5bb5c-808b-4e6f-f400-56d051cff0d7', 'emails_users_rel', 'Emails', 'emails', 'id', 'Users', 'users', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Users', 0, 0),
('6f1430af-a508-1d4b-450e-56d0512268b3', 'emails_projects_rel', 'Emails', 'emails', 'id', 'Project', 'project', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Project', 0, 0),
('6f52b53f-3103-ec20-ba1a-56d051fe3f18', 'emails_prospects_rel', 'Emails', 'emails', 'id', 'Prospects', 'prospects', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Prospects', 0, 0),
('6f52bd5a-43e6-4b31-6cfe-56d0517099da', 'meetings_contacts', 'Meetings', 'meetings', 'id', 'Contacts', 'contacts', 'id', 'meetings_contacts', 'meeting_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('6f9134ab-e50c-9423-2206-56d051ba2ff7', 'meetings_leads', 'Meetings', 'meetings', 'id', 'Leads', 'leads', 'id', 'meetings_leads', 'meeting_id', 'lead_id', 'many-to-many', NULL, NULL, 0, 0),
('6f913e90-ae83-05bb-1f4d-56d0510f0299', 'meetings_users', 'Meetings', 'meetings', 'id', 'Users', 'users', 'id', 'meetings_users', 'meeting_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('6fcfbb30-fddf-8627-0392-56d05131f784', 'opportunities_contacts', 'Opportunities', 'opportunities', 'id', 'Contacts', 'contacts', 'id', 'opportunities_contacts', 'opportunity_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('700e3617-3d48-08c2-e7d3-56d0519c90b6', 'prospect_list_contacts', 'ProspectLists', 'prospect_lists', 'id', 'Contacts', 'contacts', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Contacts', 0, 0),
('700e3eea-64a7-896d-6dc7-56d051268674', 'prospect_list_campaigns', 'ProspectLists', 'prospect_lists', 'id', 'Campaigns', 'campaigns', 'id', 'prospect_list_campaigns', 'prospect_list_id', 'campaign_id', 'many-to-many', NULL, NULL, 0, 0),
('704cbf7a-f870-3c07-efbf-56d051d8501c', 'prospect_list_prospects', 'ProspectLists', 'prospect_lists', 'id', 'Prospects', 'prospects', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Prospects', 0, 0),
('708b3213-ac8b-76b3-46bd-56d051db3719', 'prospect_list_users', 'ProspectLists', 'prospect_lists', 'id', 'Users', 'users', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Users', 0, 0),
('708b372a-fc5d-7623-0e46-56d051576cbd', 'prospect_list_leads', 'ProspectLists', 'prospect_lists', 'id', 'Leads', 'leads', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Leads', 0, 0),
('70c9ce3a-683c-071f-fc33-56d05148afd7', 'roles_users', 'Roles', 'roles', 'id', 'Users', 'users', 'id', 'roles_users', 'role_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('70c9ceee-2cea-dab6-4fcc-56d051940e3a', 'prospect_list_accounts', 'ProspectLists', 'prospect_lists', 'id', 'Accounts', 'accounts', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Accounts', 0, 0),
('71084bef-0002-5ca6-6a4a-56d0513cd9a6', 'projects_bugs', 'Project', 'project', 'id', 'Bugs', 'bugs', 'id', 'projects_bugs', 'project_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('7146c071-524a-6c94-00d0-56d0519298d4', 'projects_accounts', 'Project', 'project', 'id', 'Accounts', 'accounts', 'id', 'projects_accounts', 'project_id', 'account_id', 'many-to-many', NULL, NULL, 0, 0),
('7146c9e4-00fe-b2aa-b87c-56d051aa3a0b', 'projects_cases', 'Project', 'project', 'id', 'Cases', 'cases', 'id', 'projects_cases', 'project_id', 'case_id', 'many-to-many', NULL, NULL, 0, 0),
('7185424a-a5c0-6e5a-9ef1-56d051cd0c96', 'projects_contacts', 'Project', 'project', 'id', 'Contacts', 'contacts', 'id', 'projects_contacts', 'project_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('71c3c1ff-f490-7d31-94d1-56d05162c0a7', 'projects_opportunities', 'Project', 'project', 'id', 'Opportunities', 'opportunities', 'id', 'projects_opportunities', 'project_id', 'opportunity_id', 'many-to-many', NULL, NULL, 0, 0),
('71c3c23b-d0c1-dc78-e09a-56d05118c9e6', 'acl_roles_actions', 'ACLRoles', 'acl_roles', 'id', 'ACLActions', 'acl_actions', 'id', 'acl_roles_actions', 'role_id', 'action_id', 'many-to-many', NULL, NULL, 0, 0),
('72024a03-3f62-eafa-fed9-56d051a33856', 'acl_roles_users', 'ACLRoles', 'acl_roles', 'id', 'Users', 'users', 'id', 'acl_roles_users', 'role_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('7240c587-c418-2f9d-ce33-56d0513f63c1', 'leads_documents', 'Leads', 'leads', 'id', 'Documents', 'documents', 'id', 'linked_documents', 'parent_id', 'document_id', 'many-to-many', 'parent_type', 'Leads', 0, 0),
('7240ca35-52f5-3cd7-821d-56d051d9f951', 'email_marketing_prospect_lists', 'EmailMarketing', 'email_marketing', 'id', 'ProspectLists', 'prospect_lists', 'id', 'email_marketing_prospect_lists', 'email_marketing_id', 'prospect_list_id', 'many-to-many', NULL, NULL, 0, 0),
('727f4134-7066-f5f7-2dbc-56d051d3a78b', 'documents_accounts', 'Documents', 'documents', 'id', 'Accounts', 'accounts', 'id', 'documents_accounts', 'document_id', 'account_id', 'many-to-many', NULL, NULL, 0, 0),
('72bdc0a8-0de8-3030-c33c-56d0517a01d2', 'documents_contacts', 'Documents', 'documents', 'id', 'Contacts', 'contacts', 'id', 'documents_contacts', 'document_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('72bdc930-546f-8d7d-c3ba-56d05176843e', 'documents_opportunities', 'Documents', 'documents', 'id', 'Opportunities', 'opportunities', 'id', 'documents_opportunities', 'document_id', 'opportunity_id', 'many-to-many', NULL, NULL, 0, 0),
('72fc4ef3-d86d-2545-0218-56d0515e55bb', 'documents_cases', 'Documents', 'documents', 'id', 'Cases', 'cases', 'id', 'documents_cases', 'document_id', 'case_id', 'many-to-many', NULL, NULL, 0, 0),
('733ac02c-2648-823d-9b0f-56d0511c40ee', 'documents_bugs', 'Documents', 'documents', 'id', 'Bugs', 'bugs', 'id', 'documents_bugs', 'document_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('7a1006af-a08a-676c-0135-56d05166cd31', 'projects_notes', 'Project', 'project', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('905b0a4c-2c30-42bb-4391-56d05127ea17', 'campaigns_modified_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('94430a91-716a-fe02-e76c-56d051398594', 'campaigns_created_by', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('982b0b8d-01f6-57f2-fa38-56d051babfc4', 'campaigns_assigned_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('9c130e34-8e19-43b4-764c-56d0515470e7', 'campaign_accounts', 'Campaigns', 'campaigns', 'id', 'Accounts', 'accounts', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('9ffb0358-a429-ac83-21d0-56d05135c7b8', 'campaign_contacts', 'Campaigns', 'campaigns', 'id', 'Contacts', 'contacts', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('9ffb07bd-e345-93bd-ba9e-56d051ceed99', 'campaign_leads', 'Campaigns', 'campaigns', 'id', 'Leads', 'leads', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('a3e40312-d5d8-bae7-971c-56d051a0116c', 'campaign_prospects', 'Campaigns', 'campaigns', 'id', 'Prospects', 'prospects', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('a7cc0905-568a-a0ba-9b02-56d0514fd42e', 'campaign_opportunities', 'Campaigns', 'campaigns', 'id', 'Opportunities', 'opportunities', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('abb40013-4fe2-3358-541d-56d051e76448', 'campaign_emailman', 'Campaigns', 'campaigns', 'id', 'EmailMan', 'emailman', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('abb40f3b-6de6-6419-7799-56d051620fa7', 'campaign_email_marketing', 'Campaigns', 'campaigns', 'id', 'EmailMarketing', 'email_marketing', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('af9c08a2-a73b-4c84-9b31-56d051daf9e7', 'campaign_campaignlog', 'Campaigns', 'campaigns', 'id', 'CampaignLog', 'campaign_log', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('b38402e5-95f3-f2aa-afe4-56d0519fe146', 'campaign_assigned_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('b3840958-20cd-3e6b-1dad-56d051f2c2cb', 'campaign_modified_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('b8900b5a-5fdd-3212-1d2e-56d051a07736', 'projects_tasks', 'Project', 'project', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('d2c40a48-18b8-729d-eeee-56d051adfcca', 'prospectlists_assigned_user', 'Users', 'users', 'id', 'prospectlists', 'prospect_lists', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e3485e87-dd0f-fb4e-9444-56d05181ad14', 'leads_modified_user', 'Users', 'users', 'id', 'Leads', 'leads', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e3c551b9-cfd4-aa1d-959d-56d051dcc5fd', 'leads_created_by', 'Users', 'users', 'id', 'Leads', 'leads', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e3c55cc5-f5f7-a979-b417-56d051b0f7ca', 'leads_assigned_user', 'Users', 'users', 'id', 'Leads', 'leads', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e403d9a7-71a6-20e9-da18-56d05188bafd', 'leads_email_addresses', 'Leads', 'leads', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Leads', 0, 0),
('e44256e2-16c2-321e-0cca-56d051f5add9', 'leads_email_addresses_primary', 'Leads', 'leads', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('e480deba-9672-5bf8-57bc-56d051cc1268', 'lead_direct_reports', 'Leads', 'leads', 'id', 'Leads', 'leads', 'reports_to_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e4bf6fcd-40ed-e33c-30b6-56d051b84514', 'lead_tasks', 'Leads', 'leads', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('e4fde694-9c9c-d80f-b50b-56d051d7b551', 'lead_notes', 'Leads', 'leads', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('e4fdea87-515a-ea74-27fc-56d051a2b5b8', 'lead_meetings', 'Leads', 'leads', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('e53c646d-cd74-61fa-b4e5-56d0518aefdd', 'lead_calls', 'Leads', 'leads', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('e57ae084-a9b6-1c41-7572-56d051d5f961', 'lead_emails', 'Leads', 'leads', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('e5b9688b-bc2f-7bb0-dc50-56d051f25d3f', 'lead_campaign_log', 'Leads', 'leads', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Leads', 0, 0),
('e868e5f6-8903-a280-71fb-56d05105ad66', 'cases_modified_user', 'Users', 'users', 'id', 'Cases', 'cases', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e8a76b2f-2a4b-4d9f-1a7d-56d0517fa149', 'cases_created_by', 'Users', 'users', 'id', 'Cases', 'cases', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e8e5e76f-806f-a1ab-f484-56d0511f8e9d', 'cases_assigned_user', 'Users', 'users', 'id', 'Cases', 'cases', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e9247557-5ecb-d34f-f606-56d051d4c9e0', 'case_calls', 'Cases', 'cases', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('e962f99a-189e-5bd0-9595-56d0510af9c2', 'case_tasks', 'Cases', 'cases', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('e9a17e81-6e70-a75b-a2bd-56d051753534', 'case_notes', 'Cases', 'cases', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('e9dffcb0-6496-b3ad-97b8-56d0519156cf', 'case_meetings', 'Cases', 'cases', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('ea1e7b81-8e87-28c7-0143-56d051badf67', 'case_emails', 'Cases', 'cases', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('ed0c7582-9e6b-488a-ff00-56d051994139', 'bugs_modified_user', 'Users', 'users', 'id', 'Bugs', 'bugs', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ed4b0570-9c21-c786-11a2-56d051122df6', 'bugs_created_by', 'Users', 'users', 'id', 'Bugs', 'bugs', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ed898e25-16ce-a12c-b26f-56d0511e23a9', 'bugs_assigned_user', 'Users', 'users', 'id', 'Bugs', 'bugs', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('edc80069-e317-c3c7-bca0-56d051891d83', 'bug_tasks', 'Bugs', 'bugs', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('ee068be0-ee3c-3173-1bd1-56d05125b601', 'bug_meetings', 'Bugs', 'bugs', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('ee450b51-0b91-9cbd-7671-56d051818035', 'bug_emails', 'Bugs', 'bugs', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('ee450ba6-e9dc-1398-40da-56d0511dd15e', 'bug_calls', 'Bugs', 'bugs', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('ee8388b8-e52c-8942-b44f-56d051d22d37', 'bug_notes', 'Bugs', 'bugs', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('eec201d2-d970-215a-0000-56d051ec2999', 'bugs_release', 'Releases', 'releases', 'id', 'Bugs', 'bugs', 'found_in_release', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ef008416-6220-3ec0-1eb9-56d051ae680d', 'bugs_fixed_in_release', 'Releases', 'releases', 'id', 'Bugs', 'bugs', 'fixed_in_release', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('efbc0e3f-9bef-24f9-2c14-56d051a25a2b', 'user_direct_reports', 'Users', 'users', 'id', 'Users', 'users', 'reports_to_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('effa8608-0796-2419-ddee-56d051c90184', 'users_email_addresses', 'Users', 'users', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Users', 0, 0),
('f0390a96-7eb0-9507-1377-56d05141ac3c', 'users_email_addresses_primary', 'Users', 'users', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('f1ee9e41-63f8-735c-1066-56d051e35fb8', 'campaignlog_contact', 'CampaignLog', 'campaign_log', 'related_id', 'Contacts', 'contacts', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f22d1e2b-7cf1-396c-e131-56d05179a6d1', 'campaignlog_lead', 'CampaignLog', 'campaign_log', 'related_id', 'Leads', 'leads', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f26b962e-79f4-92ab-052d-56d051d7fa3b', 'campaignlog_targeted_users', 'CampaignLog', 'campaign_log', 'target_id', 'Users', 'users', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f26b9941-1c1f-a355-d622-56d051d24ed6', 'campaignlog_created_opportunities', 'CampaignLog', 'campaign_log', 'related_id', 'Opportunities', 'opportunities', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f2aa1b70-cde9-dcca-b2af-56d05164e5de', 'campaignlog_sent_emails', 'CampaignLog', 'campaign_log', 'related_id', 'Emails', 'emails', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f7100353-51f0-6c73-b444-56d051388a58', 'projects_meetings', 'Project', 'project', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('f71008ba-31f7-f715-5411-56d051b34367', 'projects_calls', 'Project', 'project', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `releases`
--

CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_modules`
--

CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_users`
--

CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `saved_search`
--

CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedulers`
--

CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `date_time_start` datetime DEFAULT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `schedulers`
--

INSERT INTO `schedulers` (`id`, `deleted`, `date_entered`, `date_modified`, `created_by`, `modified_user_id`, `name`, `job`, `date_time_start`, `date_time_end`, `job_interval`, `time_from`, `time_to`, `last_run`, `status`, `catch_up`) VALUES
('4ed4ce16-557a-766d-1aec-56cdecb73949', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Prune Tracker Tables', 'function::trimTracker', '2005-01-01 18:45:01', '2020-12-31 23:59:59', '0::2::1::*::*', NULL, NULL, NULL, 'Active', 1),
('4f71002c-15e4-a0d3-7f6c-56cdeced1d57', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Check Inbound Mailboxes', 'function::pollMonitoredInboxes', '2005-01-01 19:00:01', '2020-12-31 23:59:59', '*::*::*::*::*', NULL, NULL, NULL, 'Active', 0),
('4f710daf-db68-1a0f-3842-56cdec85706d', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Run Nightly Process Bounced Campaign Emails', 'function::pollMonitoredInboxesForBouncedCampaignEmails', '2005-01-01 19:30:01', '2020-12-31 23:59:59', '0::2-6::*::*::*', NULL, NULL, NULL, 'Active', 1),
('500d4199-0e6f-1e4c-9836-56cdec46f18c', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Run Nightly Mass Email Campaigns', 'function::runMassEmailCampaign', '2005-01-01 11:45:01', '2020-12-31 23:59:59', '0::2-6::*::*::*', NULL, NULL, NULL, 'Active', 1),
('500d4803-17fb-c1ef-3d60-56cdec62e5bf', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Prune Database on 1st of Month', 'function::pruneDatabase', '2005-01-01 06:15:01', '2020-12-31 23:59:59', '0::4::1::*::*', NULL, NULL, NULL, 'Inactive', 0),
('50a98042-960d-bb66-ba9e-56cdecb1bf97', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Clean Jobs Queue', 'function::cleanJobQueue', '2012-01-01 08:15:01', '2030-12-31 23:59:59', '0::5::*::*::*', NULL, NULL, NULL, 'Active', 0),
('50a9829e-f092-1970-fbf5-56cdec4372ea', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Removal of documents from filesystem', 'function::removeDocumentsFromFS', '2012-01-01 12:30:01', '2030-12-31 23:59:59', '0::3::1::*::*', NULL, NULL, NULL, 'Active', 0),
('50a98c75-bc4a-4d45-fd68-56cdecb3cdfc', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Run Email Reminder Notifications', 'function::sendEmailReminders', '2008-01-01 13:45:01', '2020-12-31 23:59:59', '*::*::*::*::*', NULL, NULL, NULL, 'Active', 0),
('5145c879-00c8-1f63-346c-56cdecbb2e16', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Prune SugarFeed Tables', 'function::trimSugarFeeds', '2005-01-01 18:00:01', '2020-12-31 23:59:59', '0::2::1::*::*', NULL, NULL, NULL, 'Active', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sugarfeed`
--

CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sugarfeed`
--

INSERT INTO `sugarfeed` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `related_module`, `related_id`, `link_url`, `link_type`) VALUES
('17158bce-6280-2226-ce64-56d04b580ea4', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:10fafcd7-13da-d3cc-e185-56d04b8f4d19:Oferta Spa] {SugarFeed.WITH} [Accounts:a9f6a964-b67a-e5cd-809b-56d04ab03144:jose_marketing] {SugarFeed.FOR} €12.00', '2016-02-26 12:55:49', '2016-02-26 12:55:49', '409c1178-a159-5926-566a-56cf4fe4f450', '409c1178-a159-5926-566a-56cf4fe4f450', NULL, 0, '409c1178-a159-5926-566a-56cf4fe4f450', 'Opportunities', '10fafcd7-13da-d3cc-e185-56d04b8f4d19', NULL, NULL),
('48553861-502f-6448-af51-56ce20a873ed', '<b>{this.CREATED_BY}</b> josemi', '2016-02-24 21:26:25', '2016-02-24 21:26:33', '1', '1', NULL, 1, '1', 'UserFeed', '1', NULL, NULL),
('7fabbca2-cc27-8f50-b8ea-56d04aa4d3e2', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_CONTACT} [Contacts:7c7f33c0-8c66-1dde-e8c9-56d04aeb8681:Molina,  Maria]', '2016-02-26 12:53:19', '2016-02-26 12:53:19', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', NULL, 0, '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Contacts', '7c7f33c0-8c66-1dde-e8c9-56d04aeb8681', NULL, NULL),
('879dc6ec-9dd2-374b-c777-56cf6119e2d2', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_LEAD} [Leads:84ee4c6a-0149-067a-a81d-56cf61a35c8a:Gomez,  Carlos]', '2016-02-25 20:20:03', '2016-02-25 20:20:03', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Leads', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', NULL, NULL),
('8aa5462f-bc28-23d3-0e4b-56cf64cca855', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_CONTACT} [Contacts:7fa89531-289c-bb16-72d6-56cf64ceb37a:Herrera,  Jorge]', '2016-02-25 20:30:57', '2016-02-25 20:30:57', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Contacts', '7fa89531-289c-bb16-72d6-56cf64ceb37a', NULL, NULL),
('8e4ed152-600a-3953-4642-56cf6433993e', '<b>{this.CREATED_BY}</b> {SugarFeed.CONVERTED_LEAD} [Leads:84ee4c6a-0149-067a-a81d-56cf61a35c8a:Gomez,  Carlos]', '2016-02-25 20:30:57', '2016-02-25 20:30:57', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Leads', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', NULL, NULL),
('a0e85808-4bf2-7523-0b28-56cf5cfdcc3c', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_CONTACT} [Contacts:572c47d0-be2b-c421-e235-56cf5c83e9f9:Pérez,  José]', '2016-02-25 19:58:22', '2016-02-25 19:58:22', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, '816c0648-ca93-1d95-11f9-56cf45208e84', 'Contacts', '572c47d0-be2b-c421-e235-56cf5c83e9f9', NULL, NULL),
('d0b04195-c952-212f-a40c-56cf5ddc20ff', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:cc0cbc57-0f93-b561-06c0-56cf5d092089:Servicio] {SugarFeed.WITH} [Accounts:8241cc33-5104-18df-a97b-56cf594cb6a6:Mi empresa] {SugarFeed.FOR} €12.00', '2016-02-25 20:02:39', '2016-02-25 20:02:39', '816c0648-ca93-1d95-11f9-56cf45208e84', '816c0648-ca93-1d95-11f9-56cf45208e84', NULL, 0, '816c0648-ca93-1d95-11f9-56cf45208e84', 'Opportunities', 'cc0cbc57-0f93-b561-06c0-56cf5d092089', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tasks`
--

CREATE TABLE `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `date_due_flag` tinyint(1) DEFAULT '0',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tracker`
--

CREATE TABLE `tracker` (
  `id` int(11) NOT NULL,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tracker`
--

INSERT INTO `tracker` (`id`, `monitor_id`, `user_id`, `module_name`, `item_id`, `item_summary`, `date_modified`, `action`, `session_id`, `visible`, `deleted`) VALUES
(86, 'cbe137d3-da8a-504f-3b00-56cef119ad9b', '1', 'Users', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'jaimez diaz,  jose miguel', '2016-02-25 12:19:46', 'detailview', '1s1n9p7ancl3mjvg4nmbglvu52', 0, 0),
(110, 'd4ee9040-5727-50c7-ebc8-56cf0183ec32', '1', 'Employees', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Díaz,  Jose', '2016-02-25 13:27:44', 'detailview', 'us23vq0ses4uc98tk7bfjkcl71', 1, 0),
(122, 'e9005948-37bb-287e-96a4-56cf4647f229', '1', 'ACLRoles', 'e900e1cc-5f05-d544-b881-56cf44eeee58', 'Jefatura', '2016-02-25 18:21:31', 'detailview', '9i9cok43ka89bovinqh300sbh3', 1, 0),
(125, '7beb11b3-587d-25aa-334d-56cf47e537f8', '1', 'Users', '1', 'Jaimez Diaz,  Jose Miguel', '2016-02-25 18:28:17', 'editview', 's0o16qdmfud00m24oohgm1dl91', 1, 0),
(128, 'dec70097-aad1-fd25-a75b-56cf47d7e7ba', '1', 'Users', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Albarrazin,  Teresa', '2016-02-25 18:28:42', 'detailview', 's0o16qdmfud00m24oohgm1dl91', 1, 0),
(131, '745ed07f-a2df-687c-de6c-56cf4fe554b7', '1', 'Users', '409c1178-a159-5926-566a-56cf4fe4f450', 'jaimez,  antonio', '2016-02-25 19:01:08', 'detailview', 's0o16qdmfud00m24oohgm1dl91', 1, 0),
(136, 'e83882bd-76c2-59c8-15c2-56cf54f51319', '1', 'Users', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Gallego,  Salvador', '2016-02-25 19:21:58', 'detailview', 's0o16qdmfud00m24oohgm1dl91', 1, 0),
(138, '62eb7e2b-e0fc-4ff0-b4c7-56cf5480e7f1', '1', 'ACLRoles', '55effc16-809c-3ff4-f464-56cf528b4049', 'Recepción', '2016-02-25 19:22:16', 'detailview', 's0o16qdmfud00m24oohgm1dl91', 1, 0),
(141, '1cc24877-d38e-f432-da18-56cf59d727df', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Users', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Albarracín,  Teresa', '2016-02-25 19:42:51', 'detailview', 'dtr2lscqd1p7a5bd93pq78f4n6', 1, 0),
(145, '29160215-7edd-ecea-1231-56cf5cfa4edc', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Accounts', '8241cc33-5104-18df-a97b-56cf594cb6a6', 'Mi empresa', '2016-02-25 19:55:52', 'detailview', 'dtr2lscqd1p7a5bd93pq78f4n6', 1, 0),
(146, 'a220e5c0-63cc-8a73-1f10-56cf5ca98fd3', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Contacts', '572c47d0-be2b-c421-e235-56cf5c83e9f9', 'Pérez, Sr. José', '2016-02-25 19:58:22', 'save', 'dtr2lscqd1p7a5bd93pq78f4n6', 1, 0),
(148, 'd9f80c75-c692-affd-5054-56cf5d509883', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Opportunities', 'cc0cbc57-0f93-b561-06c0-56cf5d092089', 'Servicio', '2016-02-25 20:02:39', 'detailview', 'dtr2lscqd1p7a5bd93pq78f4n6', 1, 0),
(153, '63563334-472d-e112-acbf-56cf64686404', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Leads', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', 'Gomez, Sr. Carlos', '2016-02-25 20:30:57', 'save', 'dtr2lscqd1p7a5bd93pq78f4n6', 1, 0),
(154, '3d7073fe-e3ec-3d53-afb1-56cf67a2b962', '816c0648-ca93-1d95-11f9-56cf45208e84', 'InboundEmail', '50ac1c00-7971-0334-35b3-56cf670c115a', 'Hoteles Albarracín', '2016-02-25 20:42:50', 'save', 'dtr2lscqd1p7a5bd93pq78f4n6', 1, 0),
(157, '24443647-0968-eced-c9e9-56d02d32ce93', '1', 'ACLRoles', 'a235c595-b3e5-f72b-b2db-56cee0123ce6', 'Marketing', '2016-02-26 10:48:11', 'save', '5n8c75c0bckj58tqpe85snunt5', 1, 0),
(159, '4df55abc-3720-7284-85c1-56d02e9c2a20', '1', 'ACLRoles', '85f6e014-4622-795d-bde1-56cf4a829665', 'Comerciales', '2016-02-26 10:54:08', 'detailview', '5n8c75c0bckj58tqpe85snunt5', 1, 0),
(168, 'a873c28f-1593-5c2e-4251-56d0363cbb12', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Contacts', '7fa89531-289c-bb16-72d6-56cf64ceb37a', 'Herrera, Sr. Jorge', '2016-02-26 11:27:01', 'editview', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(170, 'c1e70e2d-4700-e849-d0ff-56d03d30a516', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'EmailTemplates', 'c12b8434-8370-0e77-fd0b-56d03d7b3cf6', 'jose antonio', '2016-02-26 11:56:31', 'save', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(171, '5a1dbb9a-a473-3787-e4d3-56d03d649774', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'EmailMarketing', '6bb1f9e7-ea27-a70a-9e0b-56d03de6d907', 'Boleting de campaña spa', '2016-02-26 11:56:47', 'save', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(175, 'e0c11e28-d1b3-5f81-98ec-56d03eefa747', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'EmailMarketing', 'ef6759b6-27c6-820e-bbb2-56d03e025446', 'Copia de  Boleting de campaña spa', '2016-02-26 12:02:27', 'save', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(179, '3e2bba1e-44a6-aef9-88ef-56d04398f615', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Leads', '84ee4c6a-0149-067a-a81d-56cf61a35c8a', 'Gomez, Sr. Carlos', '2016-02-26 12:23:10', 'detailview', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(183, 'eab4e8e5-d4fa-50b2-9010-56d04476bb28', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Campaigns', 'df781430-6d7e-2794-5764-56d0380c6338', 'Ofertas Spa', '2016-02-26 12:25:59', 'detailview', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(184, '7cd9f80a-303c-00ab-204e-56d0448ca750', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'ProspectLists', 'db846b79-3b32-7208-e565-56d032684fac', 'Personas residentes en Granada', '2016-02-26 12:26:35', 'detailview', '11csfjmt8pc1eqkkl29fofo5r5', 1, 0),
(186, 'e1dff575-139b-1a30-7b8b-56d04ab75c2a', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Accounts', 'a9f6a964-b67a-e5cd-809b-56d04ab03144', 'jose_marketing', '2016-02-26 12:50:28', 'detailview', '2945079r5ekt3od3m3atipirl0', 1, 0),
(187, '8028c583-c492-ea48-965e-56d04ace3cf7', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Contacts', '7c7f33c0-8c66-1dde-e8c9-56d04aeb8681', 'Molina, Sra. Maria', '2016-02-26 12:53:19', 'save', '2945079r5ekt3od3m3atipirl0', 1, 0),
(191, 'a2e6e01e-709d-c760-0bd8-56d04b9fdca6', '409c1178-a159-5926-566a-56cf4fe4f450', 'Opportunities', '10fafcd7-13da-d3cc-e185-56d04b8f4d19', 'Oferta Spa', '2016-02-26 12:56:54', 'detailview', 'eh3udo667oa0ahkui6uq93tqb0', 1, 0),
(192, 'e777ca09-3704-c52c-c868-56d04bf1dd5c', '409c1178-a159-5926-566a-56cf4fe4f450', 'Contacts', '7c7f33c0-8c66-1dde-e8c9-56d04aeb8681', 'Molina, Sra. Maria', '2016-02-26 12:57:26', 'detailview', 'eh3udo667oa0ahkui6uq93tqb0', 1, 0),
(193, '154a4ea3-ba4b-896c-9c2e-56d04f8f90e9', '816c0648-ca93-1d95-11f9-56cf45208e84', 'Meetings', 'c6c6d62c-7c06-e9e8-93bc-56d04ffc6576', 'Reunión General', '2016-02-26 13:12:02', 'detailview', '6dk0p26hqmhm2avk31dc2kpvm1', 1, 0),
(197, '42c82e8f-03b8-cd7c-60db-56d04fc5f998', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Meetings', 'c6c6d62c-7c06-e9e8-93bc-56d04ffc6576', 'Reunión General', '2016-02-26 13:14:46', 'detailview', '7oi3dkefnm9ed8s0vkgtoj63e1', 1, 0),
(199, '91952fb3-33c5-c254-1981-56d050e7f13a', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'Bugs', '8d4e6d93-c22d-5e05-e678-56d050a6ba5e', 'Acerca de la oferta spa', '2016-02-26 13:16:44', 'detailview', '00o0hh4j9s6kc35mb55nn7sbj7', 1, 0),
(202, 'e4bc978f-db7e-55cc-4980-56d0505ed4c7', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Accounts', '8241cc33-5104-18df-a97b-56cf594cb6a6', 'Mi empresa', '2016-02-26 13:19:14', 'detailview', 'c4k6ikuo4algf396pq3p85m303', 1, 0),
(205, '81ae0f2e-2a53-c8cd-8932-56d050a6a91f', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'Accounts', '7fe7189d-6ab1-6354-73c7-56cf64525f61', 'Jose Miguel Jaimez Díaz', '2016-02-26 13:19:54', 'detailview', 'c4k6ikuo4algf396pq3p85m303', 1, 0),
(211, 'b840e9f1-1d37-5695-3243-56d05539569a', '1', 'Accounts', '7fe7189d-6ab1-6354-73c7-56cf64525f61', 'Jose Miguel Jaimez Díaz', '2016-02-26 13:39:17', 'detailview', '2id9uevv3nh3fvol28lvolg1q2', 1, 0),
(214, '1b3fa54b-1056-df73-ffcd-56d0555f290d', '1', 'Accounts', '8241cc33-5104-18df-a97b-56cf594cb6a6', 'Mi empresa', '2016-02-26 13:39:51', 'detailview', '2id9uevv3nh3fvol28lvolg1q2', 1, 0),
(215, 'ba2d9345-9488-c39a-6fa3-56d057395966', '1', 'Accounts', 'a9f6a964-b67a-e5cd-809b-56d04ab03144', 'jose_marketing', '2016-02-26 13:47:07', 'detailview', '2id9uevv3nh3fvol28lvolg1q2', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `upgrade_history`
--

CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` longtext,
  `date_entered` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `upgrade_history`
--

INSERT INTO `upgrade_history` (`id`, `filename`, `md5sum`, `type`, `status`, `version`, `name`, `description`, `id_name`, `manifest`, `date_entered`, `enabled`) VALUES
('76cc5027-a9be-17a8-8a10-56cdfbb5edfc', 'upload/upgrades/langpack/sugarcrm-spanish-language-pack-develop.zip', '87a6ffb32668161848b553d81a71dc59', 'langpack', 'installed', '1.5', 'Spanish (Spain) Language Pack', 'Spanish (Spain) Language Pack', 'es_es', 'YTozOntzOjg6Im1hbmlmZXN0IjthOjk6e3M6NDoibmFtZSI7czoyOToiU3BhbmlzaCAoU3BhaW4pIExhbmd1YWdlIFBhY2siO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjI5OiJTcGFuaXNoIChTcGFpbikgTGFuZ3VhZ2UgUGFjayI7czo0OiJ0eXBlIjtzOjg6ImxhbmdwYWNrIjtzOjE2OiJpc191bmluc3RhbGxhYmxlIjtzOjM6IlllcyI7czo3OiJ2ZXJzaW9uIjtzOjM6IjEuNSI7czoyNDoiYWNjZXB0YWJsZV9zdWdhcl9mbGF2b3JzIjthOjU6e2k6MDtzOjI6IkNFIjtpOjE7czozOiJQUk8iO2k6MjtzOjQ6IkNPUlAiO2k6MztzOjM6IkVOVCI7aTo0O3M6MzoiVUxUIjt9czo2OiJhdXRob3IiO3M6NDA6IkJUQUNUSUMgUy5DLkMuTC4gJiBMZW9uYXJkbyBCYXJyaWVudG9zIEMiO3M6MjU6ImFjY2VwdGFibGVfc3VnYXJfdmVyc2lvbnMiO2E6Mjp7czoxMzoiZXhhY3RfbWF0Y2hlcyI7YTowOnt9czoxMzoicmVnZXhfbWF0Y2hlcyI7YTozOntpOjA7czoxNToiNi41LlswLTldW2Etel0/IjtpOjE7czoxNToiNi40LlswLTldW2Etel0/IjtpOjI7czoxNToiNi4zLlswLTldW2Etel0/Ijt9fXM6MTQ6InB1Ymxpc2hlZF9kYXRlIjtzOjEwOiIyMDE1LTA1LTEyIjt9czoxMToiaW5zdGFsbGRlZnMiO2E6Mzp7czoyOiJpZCI7czo1OiJlc19lcyI7czo5OiJpbWFnZV9kaXIiO3M6MTc6IjxiYXNlcGF0aD4vaW1hZ2VzIjtzOjQ6ImNvcHkiO2E6Mzp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjE4OiI8YmFzZXBhdGg+L2luY2x1ZGUiO3M6MjoidG8iO3M6NzoiaW5jbHVkZSI7fWk6MTthOjI6e3M6NDoiZnJvbSI7czoxODoiPGJhc2VwYXRoPi9tb2R1bGVzIjtzOjI6InRvIjtzOjc6Im1vZHVsZXMiO31pOjI7YToyOntzOjQ6ImZyb20iO3M6MTg6IjxiYXNlcGF0aD4vaW5zdGFsbCI7czoyOiJ0byI7czo3OiJpbnN0YWxsIjt9fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==', '2016-02-24 18:52:43', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `user_hash` varchar(255) DEFAULT NULL,
  `system_generated_password` tinyint(1) DEFAULT NULL,
  `pwd_last_changed` datetime DEFAULT NULL,
  `authenticate_id` varchar(100) DEFAULT NULL,
  `sugar_login` tinyint(1) DEFAULT '1',
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `external_auth_only` tinyint(1) DEFAULT '0',
  `receive_notifications` tinyint(1) DEFAULT '1',
  `description` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `phone_home` varchar(50) DEFAULT NULL,
  `phone_mobile` varchar(50) DEFAULT NULL,
  `phone_work` varchar(50) DEFAULT NULL,
  `phone_other` varchar(50) DEFAULT NULL,
  `phone_fax` varchar(50) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `address_street` varchar(150) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `portal_only` tinyint(1) DEFAULT '0',
  `show_on_employees` tinyint(1) DEFAULT '1',
  `employee_status` varchar(100) DEFAULT NULL,
  `messenger_id` varchar(100) DEFAULT NULL,
  `messenger_type` varchar(100) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user_name`, `user_hash`, `system_generated_password`, `pwd_last_changed`, `authenticate_id`, `sugar_login`, `first_name`, `last_name`, `is_admin`, `external_auth_only`, `receive_notifications`, `description`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `title`, `department`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `status`, `address_street`, `address_city`, `address_state`, `address_country`, `address_postalcode`, `deleted`, `portal_only`, `show_on_employees`, `employee_status`, `messenger_id`, `messenger_type`, `reports_to_id`, `is_group`) VALUES
('1', 'admin', '$1$J2..ek..$dj7VLo3XZkF850s4zwTbw1', 0, NULL, NULL, 1, 'Jose Miguel', 'Jaimez Diaz', 1, 0, 1, NULL, '2016-02-24 17:46:46', '2016-02-24 18:26:08', '1', '', 'Administrator', NULL, NULL, '677153340', '958439494', NULL, NULL, 'Active', 'C/ galicia nº13', 'Atarfe', 'granada', 'España', '18230', 0, 0, 1, 'Active', NULL, NULL, '', 0),
('409c1178-a159-5926-566a-56cf4fe4f450', 'jaimez_comercial', '$1$Rj5.GB2.$JplPQ6Ye14Ch5m1hoywFa/', 0, '2016-02-25 19:01:00', NULL, 1, 'antonio', 'jaimez', 0, 0, 1, NULL, '2016-02-25 19:01:08', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', '1', 'Director comercial', 'Comercial', NULL, NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 'Active', NULL, ' ', '', 0),
('6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'jose_marketing', '$1$cd3./u3.$Nj3MLa0XOyfQRsMBE4/f/1', 0, '2016-02-25 12:20:00', NULL, 1, 'Jose', 'Díaz', 0, 0, 1, NULL, '2016-02-25 12:20:58', '2016-02-25 13:20:14', '1', '1', 'Responsable Marketing', 'Marketing', NULL, NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 'Active', NULL, ' ', '', 0),
('816c0648-ca93-1d95-11f9-56cf45208e84', 'albarracin_jefe', '$1$qA/.r63.$pCOziWP7AgY4BPJsgKTSS1', 0, '2016-02-25 18:21:00', NULL, 1, 'Teresa', 'Albarracín', 1, 0, 1, NULL, '2016-02-25 18:19:07', '2016-02-25 19:42:50', '816c0648-ca93-1d95-11f9-56cf45208e84', '1', 'Jefe de empresa', 'Jefatura', NULL, NULL, NULL, NULL, NULL, 'Active', 'C/ galicia nº13', 'granada', 'Granada', 'España', '18230', 0, 0, 1, 'Active', 'Jose Miguel Jaimez Díaz', ' ', '', 0),
('b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'salvador_recepcion', '$1$sM3.FM5.$ue.6MIuH4R0eOf9GURDUn.', 0, '2016-02-25 19:21:00', NULL, 1, 'Salvador', 'Gallego', 0, 0, 1, NULL, '2016-02-25 19:21:58', '2016-02-25 19:22:48', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', '1', 'Responsable de incidencias', 'Recepcion(Incidencias)', NULL, NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 'Active', NULL, ' ', '', 0),
('b2752bed-800e-8997-c965-56cee29a3cdb', 'josemi', '$1$OB2.9a2.$TMumf/.A9quaN8BgDQKr51', 0, '2016-02-25 12:13:00', NULL, 1, 'jose miguel', 'jaimez diaz', 0, 0, 1, NULL, '2016-02-25 11:16:31', '2016-02-25 12:19:51', '1', '1', 'Responsable de marketing', 'Marketing', NULL, NULL, NULL, NULL, NULL, 'Inactive', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 'Terminated', NULL, ' ', '1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_cstm`
--

CREATE TABLE `users_cstm` (
  `id_c` char(36) NOT NULL,
  `dni_c` varchar(10) DEFAULT '00000000X'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_cstm`
--

INSERT INTO `users_cstm` (`id_c`, `dni_c`) VALUES
('409c1178-a159-5926-566a-56cf4fe4f450', '00000000X'),
('6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', '75930078F'),
('816c0648-ca93-1d95-11f9-56cf45208e84', '11111111X'),
('b08db30f-6a25-a86a-3b1e-56cf54cc213b', '00000000X');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_feeds`
--

CREATE TABLE `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_last_import`
--

CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `import_module` varchar(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_password_link`
--

CREATE TABLE `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_signatures`
--

CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_preferences`
--

CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_preferences`
--

INSERT INTO `user_preferences` (`id`, `category`, `deleted`, `date_entered`, `date_modified`, `assigned_user_id`, `contents`) VALUES
('1607bf97-63e2-c6b6-af9b-56d043060c54', 'Prospects2_PROSPECT', 0, '2016-02-26 12:21:17', '2016-02-26 12:21:17', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('18ae4246-5ec6-a299-bd0f-56d0511a64a1', 'Employees2_EMPLOYEE', 0, '2016-02-26 13:21:02', '2016-02-26 13:21:02', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('25ac0346-d88e-3f8a-7178-56cef17eb2e8', 'ETag', 0, '2016-02-25 12:22:15', '2016-02-25 13:15:41', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6Mjt9'),
('26ab96e9-9a99-2a63-a3a5-56cef1d3bf8e', 'Calls2_CALL', 0, '2016-02-25 12:18:55', '2016-02-25 12:18:55', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('28d9442d-ff0c-b6ac-38e3-56cef067655b', 'Home', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjg6e3M6MzY6ImNjNTUwNmU2LTQ3MjMtYjAwYi1hZTc4LTU2Y2VmMGNkMDdkZCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxMzoiaUZyYW1lRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NDoiSG9tZSI7czoxMToiZm9yY2VDb2x1bW4iO2k6MDtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NTM6Im1vZHVsZXMvSG9tZS9EYXNobGV0cy9pRnJhbWVEYXNobGV0L2lGcmFtZURhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6Mzp7czoxMDoidGl0bGVMYWJlbCI7czozMDoiTEJMX0RBU0hMRVRfRElTQ09WRVJfU1VHQVJfUFJPIjtzOjM6InVybCI7czo0MjoiaHR0cHM6Ly93d3cuc3VnYXJjcm0uY29tL2NybS9wcm9kdWN0L2dvcHJvIjtzOjY6ImhlaWdodCI7aTozMTU7fX1zOjM2OiJjYzU1MDY0NS01OTIxLWUzNjktZDg5MS01NmNlZjAwYzFhMjciO2E6NDp7czo5OiJjbGFzc05hbWUiO3M6MTY6IlN1Z2FyRmVlZERhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjk6IlN1Z2FyRmVlZCI7czoxMToiZm9yY2VDb2x1bW4iO2k6MTtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NjQ6Im1vZHVsZXMvU3VnYXJGZWVkL0Rhc2hsZXRzL1N1Z2FyRmVlZERhc2hsZXQvU3VnYXJGZWVkRGFzaGxldC5waHAiO31zOjM2OiJjYzU1MDI5Zi1iODdiLTg0NWUtMTZmYy01NmNlZjAxODMwOGUiO2E6NTp7czo5OiJjbGFzc05hbWUiO3M6MTM6ImlGcmFtZURhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjQ6IkhvbWUiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjE7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjUzOiJtb2R1bGVzL0hvbWUvRGFzaGxldHMvaUZyYW1lRGFzaGxldC9pRnJhbWVEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjM6e3M6MTA6InRpdGxlTGFiZWwiO3M6MjI6IkxCTF9EQVNITEVUX1NVR0FSX05FV1MiO3M6MzoidXJsIjtzOjQxOiJodHRwczovL3d3dy5zdWdhcmNybS5jb20vY3JtL3Byb2R1Y3QvbmV3cyI7czo2OiJoZWlnaHQiO2k6MzE1O319czozNjoiZDQyNjBhOWYtNDAzOC1mNGI2LTNlNWYtNTZjZWYwNjU1ZGE2IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUNhbGxzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiQ2FsbHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0NhbGxzL0Rhc2hsZXRzL015Q2FsbHNEYXNobGV0L015Q2FsbHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiZGZkZTBiYjktMjFlNS0wMWIwLTgwZGYtNTZjZWYwMDQ2NjE1IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeU1lZXRpbmdzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiTWVldGluZ3MiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL01lZXRpbmdzL0Rhc2hsZXRzL015TWVldGluZ3NEYXNobGV0L015TWVldGluZ3NEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiZWI5NjA0Y2MtMTU2ZS1jZTg2LTg3Y2UtNTZjZWYwMDI4MDM2IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjIyOiJNeU9wcG9ydHVuaXRpZXNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czoxMzoiT3Bwb3J0dW5pdGllcyI7czoxMToiZm9yY2VDb2x1bW4iO2k6MDtzOjEyOiJmaWxlTG9jYXRpb24iO3M6ODA6Im1vZHVsZXMvT3Bwb3J0dW5pdGllcy9EYXNobGV0cy9NeU9wcG9ydHVuaXRpZXNEYXNobGV0L015T3Bwb3J0dW5pdGllc0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX1zOjM2OiJmNzRlMDIzNi1hYTBjLTJlNDQtZjBjYy01NmNlZjAzN2RkNzgiO2E6NTp7czo5OiJjbGFzc05hbWUiO3M6MTc6Ik15QWNjb3VudHNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo4OiJBY2NvdW50cyI7czoxMToiZm9yY2VDb2x1bW4iO2k6MDtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NjU6Im1vZHVsZXMvQWNjb3VudHMvRGFzaGxldHMvTXlBY2NvdW50c0Rhc2hsZXQvTXlBY2NvdW50c0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX1zOjM2OiIxMDMwNmRiOS0wMWZmLTU2NDYtODcyMy01NmNlZjA4ZmI2ODkiO2E6NTp7czo5OiJjbGFzc05hbWUiO3M6MTQ6Ik15TGVhZHNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo1OiJMZWFkcyI7czoxMToiZm9yY2VDb2x1bW4iO2k6MDtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NTY6Im1vZHVsZXMvTGVhZHMvRGFzaGxldHMvTXlMZWFkc0Rhc2hsZXQvTXlMZWFkc0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX19czo1OiJwYWdlcyI7YToxOntpOjA7YTozOntzOjc6ImNvbHVtbnMiO2E6Mjp7aTowO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI2MCUiO3M6ODoiZGFzaGxldHMiO2E6Njp7aTowO3M6MzY6ImNjNTUwNmU2LTQ3MjMtYjAwYi1hZTc4LTU2Y2VmMGNkMDdkZCI7aToxO3M6MzY6ImQ0MjYwYTlmLTQwMzgtZjRiNi0zZTVmLTU2Y2VmMDY1NWRhNiI7aToyO3M6MzY6ImRmZGUwYmI5LTIxZTUtMDFiMC04MGRmLTU2Y2VmMDA0NjYxNSI7aTozO3M6MzY6ImViOTYwNGNjLTE1NmUtY2U4Ni04N2NlLTU2Y2VmMDAyODAzNiI7aTo0O3M6MzY6ImY3NGUwMjM2LWFhMGMtMmU0NC1mMGNjLTU2Y2VmMDM3ZGQ3OCI7aTo1O3M6MzY6IjEwMzA2ZGI5LTAxZmYtNTY0Ni04NzIzLTU2Y2VmMDhmYjY4OSI7fX1pOjE7YToyOntzOjU6IndpZHRoIjtzOjM6IjQwJSI7czo4OiJkYXNobGV0cyI7YToyOntpOjA7czozNjoiY2M1NTA2NDUtNTkyMS1lMzY5LWQ4OTEtNTZjZWYwMGMxYTI3IjtpOjE7czozNjoiY2M1NTAyOWYtYjg3Yi04NDVlLTE2ZmMtNTZjZWYwMTgzMDhlIjt9fX1zOjEwOiJudW1Db2x1bW5zIjtzOjE6IjIiO3M6MTQ6InBhZ2VUaXRsZUxhYmVsIjtzOjIwOiJMQkxfSE9NRV9QQUdFXzFfTkFNRSI7fX19'),
('29d340ac-863b-a2d2-8669-56cef01a92af', 'Home2_CALL', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2a3a5254-e138-a19e-6af0-56cf50dc10ce', 'Contacts2_CONTACT', 0, '2016-02-25 19:05:35', '2016-02-25 19:05:35', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2a504f4a-9ddb-1a9c-1d78-56cef0b88524', 'Home2_MEETING', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2a8ecade-f450-2c0e-7456-56cef06fdeae', 'Home2_OPPORTUNITY', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2b0bdd5e-5f33-5d40-fada-56cef09bb7a8', 'Home2_ACCOUNT', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2b4a5476-38c5-aef4-9e5e-56cef09efdff', 'Home2_LEAD', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2c05d201-4cc5-eda0-39ec-56cef07b3fa8', 'Home2_SUGARFEED', 0, '2016-02-25 12:14:35', '2016-02-25 12:14:35', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2dd23a5e-aa39-e6de-ecd8-56cef6d595f1', 'Campaigns2_CAMPAIGN', 0, '2016-02-25 12:41:55', '2016-02-25 12:41:55', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('310cc2d4-0c8b-8e55-4b23-56d02f6ce41d', 'Campaigns2_CAMPAIGN', 0, '2016-02-26 10:54:57', '2016-02-26 10:54:57', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('3552a4b2-7480-edd7-8558-56cdf56d271c', 'ETag', 0, '2016-02-24 18:26:08', '2016-02-24 18:26:08', '1', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6MTt9'),
('35b94aee-2bc9-6140-2fd1-56cf5485a52c', 'ETag', 0, '2016-02-25 19:22:48', '2016-02-25 19:22:48', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6MTt9'),
('36fe2f78-7dfb-80b6-52c0-56d04b9cee65', 'Leads2_LEAD', 0, '2016-02-26 12:57:12', '2016-02-26 12:57:12', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('3bba8c65-5bac-859d-6104-56cef06ca021', 'Leads2_LEAD', 0, '2016-02-25 12:14:44', '2016-02-25 12:14:44', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('3f285571-c556-261a-ff78-56cee094bcc7', 'Users2_USER', 0, '2016-02-25 11:07:08', '2016-02-25 11:07:08', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('430d2504-7e44-e47f-7135-56cf4f4c24eb', 'global', 0, '2016-02-25 19:01:08', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YTozNDp7czoxMjoibWFpbG1lcmdlX29uIjtzOjI6Im9uIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxNDoibW9kdWxlX2Zhdmljb24iO3M6MDoiIjtzOjk6ImhpZGVfdGFicyI7YTowOnt9czoxMToicmVtb3ZlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6MTgwMDtzOjE5OiJlbWFpbF9yZW1pbmRlcl90aW1lIjtpOi0xO3M6ODoidGltZXpvbmUiO3M6MTM6IkV1cm9wZS9CZXJsaW4iO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjQ6ImZkb3ciO3M6MToiMCI7czo1OiJkYXRlZiI7czo1OiJkL20vWSI7czo1OiJ0aW1lZiI7czozOiJILmkiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjY6ImwsIHMgZiI7czoxNjoiZXhwb3J0X2RlbGltaXRlciI7czoxOiIsIjtzOjIyOiJkZWZhdWx0X2V4cG9ydF9jaGFyc2V0IjtzOjU6IlVURi04IjtzOjE0OiJ1c2VfcmVhbF9uYW1lcyI7czoyOiJvbiI7czoxNzoibWFpbF9zbXRwYXV0aF9yZXEiO3M6MDoiIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MDtzOjE1OiJlbWFpbF9saW5rX3R5cGUiO3M6NToic3VnYXIiO3M6MTc6ImVtYWlsX3Nob3dfY291bnRzIjtpOjA7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6IjQzMGQyZWNkLTk4ODEtNjMyMS04MjM3LTU2Y2Y0ZmU3M2I5YyI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VnYXI1IjtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjQ6IlRvZG8iO30='),
('436ae086-ca4c-de02-c4c1-56cf54355abb', 'Contacts2_CONTACT', 0, '2016-02-25 19:23:08', '2016-02-25 19:23:08', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('47e009d8-d4f9-f24a-94e2-56d045e1d16e', 'Home2_SUGARFEED', 0, '2016-02-26 12:31:45', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('4d9c4409-fde7-3208-b5c1-56cdec47cbac', 'global', 0, '2016-02-24 17:46:46', '2016-02-26 13:08:23', '1', 'YTozNTp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6IjRkOWM0NmM4LTkyMmUtMjQ0YS1lODgyLTU2Y2RlYzBhZjU5NyI7czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWdhcjUiO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6MTgwMDtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6ODoidGltZXpvbmUiO3M6MTM6IkV1cm9wZS9NYWRyaWQiO3M6MTY6InN3YXBfbGFzdF92aWV3ZWQiO3M6MDoiIjtzOjE0OiJzd2FwX3Nob3J0Y3V0cyI7czowOiIiO3M6MTk6Im5hdmlnYXRpb25fcGFyYWRpZ20iO3M6MjoiZ20iO3M6MTM6InN1YnBhbmVsX3RhYnMiO3M6MDoiIjtzOjE0OiJtb2R1bGVfZmF2aWNvbiI7czowOiIiO3M6OToiaGlkZV90YWJzIjthOjA6e31zOjExOiJyZW1vdmVfdGFicyI7YTowOnt9czo3OiJub19vcHBzIjtzOjM6Im9mZiI7czoxOToiZW1haWxfcmVtaW5kZXJfdGltZSI7aTotMTtzOjI6InV0IjtzOjE6IjEiO3M6ODoiY3VycmVuY3kiO3M6MzoiLTk5IjtzOjM1OiJkZWZhdWx0X2N1cnJlbmN5X3NpZ25pZmljYW50X2RpZ2l0cyI7czoxOiIyIjtzOjExOiJudW1fZ3JwX3NlcCI7czoxOiIsIjtzOjc6ImRlY19zZXAiO3M6MToiLiI7czo1OiJkYXRlZiI7czo1OiJkL20vWSI7czo1OiJ0aW1lZiI7czozOiJILmkiO3M6MTU6Im1haWxfc210cHNlcnZlciI7czoxNDoic210cC5nbWFpbC5jb20iO3M6MTM6Im1haWxfc210cHVzZXIiO3M6MjM6InBzM2pvc2VtaWd1ZWxAZ21haWwuY29tIjtzOjEzOiJtYWlsX3NtdHBwYXNzIjtzOjEwOiJpbmZsYW1lczE5IjtzOjI2OiJkZWZhdWx0X2xvY2FsZV9uYW1lX2Zvcm1hdCI7czo2OiJsLCBzIGYiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czowOiIiO3M6MTI6Im1haWxfc210cHNzbCI7aTowO3M6MTc6ImVtYWlsX3Nob3dfY291bnRzIjtpOjA7czoxOToidGhlbWVfY3VycmVudF9ncm91cCI7czo0OiJUb2RvIjtzOjEwOiJFbXBsb3llZXNRIjthOjM6e3M6NjoibW9kdWxlIjtzOjk6IkVtcGxveWVlcyI7czo2OiJhY3Rpb24iO3M6NToiaW5kZXgiO3M6NToicXVlcnkiO3M6NDoidHJ1ZSI7fXM6MTc6InNpZ25hdHVyZV9kZWZhdWx0IjtzOjA6IiI7czoxNzoic2lnbmF0dXJlX3ByZXBlbmQiO2I6MDtzOjM0OiJJbmJvdW5kRW1haWxfSW5ib3VuZEVtYWlsX09SREVSX0JZIjtzOjEyOiJkYXRlX2VudGVyZWQiO3M6MTI6InVzZXJQcml2R3VpZCI7czozNjoiMzc5MmRmY2UtODhmOS0xMzlkLWMwZWUtNTZkMDRlZWJkOTg4Ijt9'),
('50f9898f-8280-2d86-4c3b-56ce1b0eaa80', 'Accounts2_ACCOUNT', 0, '2016-02-24 21:08:43', '2016-02-24 21:08:43', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('5640329a-dcb1-e5c7-6a6c-56ceb8ccd677', 'Documents2_DOCUMENT', 0, '2016-02-25 08:17:41', '2016-02-25 08:17:41', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('59572620-8246-ca75-224e-56d04fc05d2d', 'Tasks2_TASK', 0, '2016-02-26 13:13:08', '2016-02-26 13:13:08', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('69260e5d-9785-9489-f91b-56d034b25646', 'Contacts2_CONTACT', 0, '2016-02-26 11:19:25', '2016-02-26 11:19:25', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('6b8e74d6-0fd2-f1b0-d31c-56d043ef9fe9', 'Accounts2_ACCOUNT', 0, '2016-02-26 12:24:18', '2016-02-26 12:24:18', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('6bc564f8-07f0-a290-18fd-56cf5c1d7dcf', 'Contacts2_CONTACT', 0, '2016-02-25 19:56:26', '2016-02-25 19:56:26', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7036e71c-e9b0-4159-fb46-56cef1c2dc72', 'global', 0, '2016-02-25 12:20:58', '2016-02-26 13:21:02', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YTozNjp7czoxMjoibWFpbG1lcmdlX29uIjtzOjI6Im9uIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxNDoibW9kdWxlX2Zhdmljb24iO3M6MDoiIjtzOjk6ImhpZGVfdGFicyI7YTowOnt9czoxMToicmVtb3ZlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6MTgwMDtzOjE5OiJlbWFpbF9yZW1pbmRlcl90aW1lIjtpOi0xO3M6ODoidGltZXpvbmUiO3M6MTM6IkV1cm9wZS9CZXJsaW4iO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjQ6ImZkb3ciO3M6MToiMCI7czo1OiJkYXRlZiI7czo1OiJkL20vWSI7czo1OiJ0aW1lZiI7czozOiJILmkiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjY6ImwsIHMgZiI7czoxNjoiZXhwb3J0X2RlbGltaXRlciI7czoxOiIsIjtzOjIyOiJkZWZhdWx0X2V4cG9ydF9jaGFyc2V0IjtzOjU6IlVURi04IjtzOjE0OiJ1c2VfcmVhbF9uYW1lcyI7czoyOiJvbiI7czoxNzoibWFpbF9zbXRwYXV0aF9yZXEiO3M6MDoiIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MDtzOjE1OiJlbWFpbF9saW5rX3R5cGUiO3M6NToic3VnYXIiO3M6MTc6ImVtYWlsX3Nob3dfY291bnRzIjtpOjA7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6IjcwMzZlZmQyLWQ1ZDktYTA1OC0yNDU0LTU2Y2VmMTNkYzBlMSI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VnYXI1IjtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjQ6IlRvZG8iO3M6MTI6InVzZXJQcml2R3VpZCI7czozNjoiYTY4ZDA1ODYtMDQ0OS05YjliLTA5NWUtNTZkMDNkOWFlMDBkIjtzOjEwOiJFbXBsb3llZXNRIjthOjM6e3M6NjoibW9kdWxlIjtzOjk6IkVtcGxveWVlcyI7czo2OiJhY3Rpb24iO3M6NToiaW5kZXgiO3M6NToicXVlcnkiO3M6NDoidHJ1ZSI7fX0='),
('7289e82c-4ba9-6881-2a28-56cf46796f91', 'ETag', 0, '2016-02-25 18:22:55', '2016-02-25 19:42:50', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6Mzt9'),
('73b17261-02ac-d80b-e666-56cf471382b1', 'Employees2_EMPLOYEE', 0, '2016-02-25 18:27:30', '2016-02-25 18:27:30', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('783fce86-1984-4af4-c015-56cdf5251329', 'Home', 0, '2016-02-24 18:26:09', '2016-02-26 13:47:32', '1', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjg6e3M6MzY6IjFjYjBmMTdiLTFmZmQtODg0YS0yYzlkLTU2Y2RmNTYxZjczYyI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjI4MmE5YTZiLTRlNTEtZDM3NC00MTEyLTU2Y2RmNTA2MmJkYiI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjJhMWVhMWEyLWU1ODUtNTY4Mi1iMDA2LTU2Y2RmNTBjNGRiMCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjJjOGZhNDY0LTcwNGQtZjBiNy05NTE2LTU2Y2RmNWQxMzQ4MSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMmVjMjNlYTgtY2M3OS05NzNjLWE2MzUtNTZjZGY1NmNiNzFkIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMzBiNjMxMjQtYjBlMi0wMTYyLWJiZDYtNTZjZGY1ZWE4YWY4IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiNmIzOWJjMmItMGM1NS0xNmZlLTY5YzYtNTZjZWY2OWU4ZDc3IjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjE5OiJUb3BDYW1wYWlnbnNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo5OiJDYW1wYWlnbnMiO3M6Nzoib3B0aW9ucyI7YTowOnt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjcwOiJtb2R1bGVzL0NhbXBhaWducy9EYXNobGV0cy9Ub3BDYW1wYWlnbnNEYXNobGV0L1RvcENhbXBhaWduc0Rhc2hsZXQucGhwIjt9czozNjoiZTQyZDQ2ZjItZDU2My1jNGY0LTM4M2YtNTZkMDRlYjhjZTAzIjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjIzOiJDYW1wYWlnblJPSUNoYXJ0RGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiQ2FtcGFpZ25zIjtzOjc6Im9wdGlvbnMiO2E6Mzp7czoxMToiY2FtcGFpZ25faWQiO2E6MTp7aTowO3M6MzY6ImRmNzgxNDMwLTZkN2UtMjc5NC01NzY0LTU2ZDAzODBjNjMzOCI7fXM6NToidGl0bGUiO3M6MTI6IkNhbXBhaWduIFJPSSI7czoxMToiYXV0b1JlZnJlc2giO3M6MjoiLTEiO31zOjEyOiJmaWxlTG9jYXRpb24iO3M6NzU6Im1vZHVsZXMvQ2hhcnRzL0Rhc2hsZXRzL0NhbXBhaWduUk9JQ2hhcnREYXNobGV0L0NhbXBhaWduUk9JQ2hhcnREYXNobGV0LnBocCI7fX1zOjU6InBhZ2VzIjthOjE6e2k6MDthOjM6e3M6NzoiY29sdW1ucyI7YToyOntpOjA7YToyOntzOjU6IndpZHRoIjtzOjM6IjYwJSI7czo4OiJkYXNobGV0cyI7YTo3OntpOjA7czozNjoiZTQyZDQ2ZjItZDU2My1jNGY0LTM4M2YtNTZkMDRlYjhjZTAzIjtpOjE7czozNjoiNmIzOWJjMmItMGM1NS0xNmZlLTY5YzYtNTZjZWY2OWU4ZDc3IjtpOjI7czozNjoiMjgyYTlhNmItNGU1MS1kMzc0LTQxMTItNTZjZGY1MDYyYmRiIjtpOjM7czozNjoiMmExZWExYTItZTU4NS01NjgyLWIwMDYtNTZjZGY1MGM0ZGIwIjtpOjQ7czozNjoiMmM4ZmE0NjQtNzA0ZC1mMGI3LTk1MTYtNTZjZGY1ZDEzNDgxIjtpOjU7czozNjoiMmVjMjNlYTgtY2M3OS05NzNjLWE2MzUtNTZjZGY1NmNiNzFkIjtpOjY7czozNjoiMzBiNjMxMjQtYjBlMi0wMTYyLWJiZDYtNTZjZGY1ZWE4YWY4Ijt9fWk6MTthOjI6e3M6NToid2lkdGgiO3M6MzoiNDAlIjtzOjg6ImRhc2hsZXRzIjthOjE6e2k6MDtzOjM2OiIxY2IwZjE3Yi0xZmZkLTg4NGEtMmM5ZC01NmNkZjU2MWY3M2MiO319fXM6MTA6Im51bUNvbHVtbnMiO3M6MToiMiI7czoxNDoicGFnZVRpdGxlTGFiZWwiO3M6MjA6IkxCTF9IT01FX1BBR0VfMV9OQU1FIjt9fX0='),
('78fb4aee-7381-a1ec-ffe4-56cdf56517d5', 'Home2_CALL', 0, '2016-02-24 18:26:09', '2016-02-24 18:26:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7939c7a4-5e5b-2a45-3163-56cdf50ce7d7', 'Home2_MEETING', 0, '2016-02-24 18:26:09', '2016-02-24 18:26:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('79a65f8a-da14-ef8e-0d15-56ce25017487', 'Emails', 0, '2016-02-24 21:51:58', '2016-02-25 10:45:26', '1', 'YToyOntzOjExOiJmb2N1c0ZvbGRlciI7czo5NzoiYToyOntzOjQ6ImllSWQiO3M6MzY6ImM1NGY3NGZlLTVmMWYtMjUzZi1kNWUzLTU2Y2UyNWQ5MTNkYiI7czo2OiJmb2xkZXIiO3M6MTU6IlNVR0FSLk1pIENvcnJlbyI7fSI7czoxMzoiZW1haWxTZXR0aW5ncyI7YTo1OntzOjE4OiJlbWFpbENoZWNrSW50ZXJ2YWwiO3M6MjoiLTEiO3M6MTg6ImFsd2F5c1NhdmVPdXRib3VuZCI7czoxOiIxIjtzOjEzOiJzZW5kUGxhaW5UZXh0IjtzOjE6IjAiO3M6MTM6InNob3dOdW1Jbkxpc3QiO3M6MjoiMjAiO3M6MjI6ImRlZmF1bHRPdXRib3VuZENoYXJzZXQiO3M6NToiVVRGLTgiO319'),
('79b6c46a-4b80-cacf-5bc8-56cdf57cb35e', 'Home2_OPPORTUNITY', 0, '2016-02-24 18:26:09', '2016-02-24 18:26:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('79f54b29-2f8f-7d00-1d01-56cdf5285595', 'Home2_ACCOUNT', 0, '2016-02-24 18:26:09', '2016-02-24 18:26:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7a33cc8f-5761-50ce-5a53-56cdf5c165dc', 'Home2_LEAD', 0, '2016-02-24 18:26:09', '2016-02-24 18:26:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7a72426f-8231-9c05-91ba-56cdf5ba8c26', 'Home2_SUGARFEED', 0, '2016-02-24 18:26:09', '2016-02-24 18:26:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7c2e0ce9-c11b-02b6-83d1-56ceb646aeef', 'Employees2_EMPLOYEE', 0, '2016-02-25 08:08:10', '2016-02-25 08:08:10', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7dc31975-d249-50e1-64e5-56d045deb667', 'ETag', 0, '2016-02-26 12:31:45', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6MTt9'),
('8897ad64-1fa2-0d3d-3d9e-56ce2a78a602', 'EmailTemplates2_EMAILTEMPLATE', 0, '2016-02-24 22:10:13', '2016-02-24 22:10:13', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('8a438e18-3f48-2a95-8b7d-56cef1b886a4', 'Home', 0, '2016-02-25 12:22:16', '2016-02-26 12:43:56', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjcxOWFhNzBjLTVjNTMtZWRlOS0zZmVlLTU2Y2VmMTNkMTY1NCI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjcyNTYyODg3LWYzNDEtNDg3ZS1mZGE0LTU2Y2VmMTFmNTU4MyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjczMTFhNjk1LTEwMDYtNTI5My1iYWYzLTU2Y2VmMThhYTU5NyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjczOGVhNDU5LThjYmMtZjJkMC1kMWU1LTU2Y2VmMWQ4NWY4NCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiNzQ0YTI4OWItMTIxMS1mNGIwLTA3OTgtNTZjZWYxZmZkM2Y1IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiNWM5Mjc5ZjUtYTA1OC1kYjYwLWRiMzctNTZkMDNkMjQ5OWE4IjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjIzOiJDYW1wYWlnblJPSUNoYXJ0RGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiQ2FtcGFpZ25zIjtzOjc6Im9wdGlvbnMiO2E6Mzp7czoxMToiY2FtcGFpZ25faWQiO2E6MTp7aTowO3M6MzY6ImRmNzgxNDMwLTZkN2UtMjc5NC01NzY0LTU2ZDAzODBjNjMzOCI7fXM6NToidGl0bGUiO3M6MTI6IkNhbXBhaWduIFJPSSI7czoxMToiYXV0b1JlZnJlc2giO3M6MjoiLTEiO31zOjEyOiJmaWxlTG9jYXRpb24iO3M6NzU6Im1vZHVsZXMvQ2hhcnRzL0Rhc2hsZXRzL0NhbXBhaWduUk9JQ2hhcnREYXNobGV0L0NhbXBhaWduUk9JQ2hhcnREYXNobGV0LnBocCI7fX1zOjU6InBhZ2VzIjthOjE6e2k6MDthOjM6e3M6NzoiY29sdW1ucyI7YToyOntpOjA7YToyOntzOjU6IndpZHRoIjtzOjM6IjYwJSI7czo4OiJkYXNobGV0cyI7YTo0OntpOjA7czozNjoiNWM5Mjc5ZjUtYTA1OC1kYjYwLWRiMzctNTZkMDNkMjQ5OWE4IjtpOjE7czozNjoiNzI1NjI4ODctZjM0MS00ODdlLWZkYTQtNTZjZWYxMWY1NTgzIjtpOjI7czozNjoiNzMxMWE2OTUtMTAwNi01MjkzLWJhZjMtNTZjZWYxOGFhNTk3IjtpOjM7czozNjoiNzQ0YTI4OWItMTIxMS1mNGIwLTA3OTgtNTZjZWYxZmZkM2Y1Ijt9fWk6MTthOjI6e3M6NToid2lkdGgiO3M6MzoiNDAlIjtzOjg6ImRhc2hsZXRzIjthOjE6e2k6MDtzOjM2OiI3MTlhYTcwYy01YzUzLWVkZTktM2ZlZS01NmNlZjEzZDE2NTQiO319fXM6MTA6Im51bUNvbHVtbnMiO3M6MToiMiI7czoxNDoicGFnZVRpdGxlTGFiZWwiO3M6MjA6IkxCTF9IT01FX1BBR0VfMV9OQU1FIjt9fX0='),
('8aff00d4-1af7-4bd1-97e8-56cef1d17981', 'Home2_CALL', 0, '2016-02-25 12:22:16', '2016-02-25 12:22:16', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('8b7c0fcb-b87b-09d9-3d7f-56cef1d114ae', 'Home2_MEETING', 0, '2016-02-25 12:22:16', '2016-02-25 12:22:16', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('8bba8e29-391a-9698-4c7b-56cef16cbc48', 'Home2_OPPORTUNITY', 0, '2016-02-25 12:22:16', '2016-02-25 12:22:16', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('8c37893c-1682-955b-3ad5-56cef1e53906', 'Home2_ACCOUNT', 0, '2016-02-25 12:22:16', '2016-02-25 12:22:16', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('8cb48de2-82a1-4708-f7d3-56cef13fff2c', 'Home2_SUGARFEED', 0, '2016-02-25 12:22:16', '2016-02-25 12:22:16', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('8fc780f5-71ad-a9e7-ab18-56ceaa51e755', 'Leads2_LEAD', 0, '2016-02-25 07:17:17', '2016-02-25 07:17:17', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('92d4df96-6838-14cc-9ffc-56cef0192e75', 'Contacts2_CONTACT', 0, '2016-02-25 12:14:42', '2016-02-25 12:14:42', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('94ddb45c-d148-44dc-7f75-56d04f962462', 'Meetings2_MEETING', 0, '2016-02-26 13:13:57', '2016-02-26 13:13:57', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('960007cc-4f42-e212-6ee5-56d0450c39fa', 'Home2_LEAD', 0, '2016-02-26 12:31:45', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('962f6555-5d95-a0e9-dfc6-56cf5668033a', 'Accounts2_ACCOUNT', 0, '2016-02-25 19:31:00', '2016-02-25 19:31:00', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('98d27540-fe63-1c6b-706e-56d04b13e9ab', 'Contacts2_CONTACT', 0, '2016-02-26 12:57:24', '2016-02-26 12:57:24', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('9a256203-b1ce-0130-7172-56cf4661a79a', 'global', 0, '2016-02-25 18:21:14', '2016-02-26 13:09:19', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YTozNjp7czoxMjoibWFpbG1lcmdlX29uIjtzOjI6Im9uIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxNDoibW9kdWxlX2Zhdmljb24iO3M6MDoiIjtzOjk6ImhpZGVfdGFicyI7YTowOnt9czoxMToicmVtb3ZlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6MTM6InJlbWluZGVyX3RpbWUiO3M6NDoiMTgwMCI7czoxOToiZW1haWxfcmVtaW5kZXJfdGltZSI7aTotMTtzOjg6InRpbWV6b25lIjtzOjEzOiJFdXJvcGUvQmVybGluIjtzOjI6InV0IjtzOjE6IjEiO3M6ODoiY3VycmVuY3kiO3M6MzoiLTk5IjtzOjM1OiJkZWZhdWx0X2N1cnJlbmN5X3NpZ25pZmljYW50X2RpZ2l0cyI7czoxOiIyIjtzOjExOiJudW1fZ3JwX3NlcCI7czoxOiIsIjtzOjc6ImRlY19zZXAiO3M6MToiLiI7czo0OiJmZG93IjtzOjE6IjAiO3M6NToiZGF0ZWYiO3M6NToiZC9tL1kiO3M6NToidGltZWYiO3M6MzoiSC5pIjtzOjI2OiJkZWZhdWx0X2xvY2FsZV9uYW1lX2Zvcm1hdCI7czo2OiJsLCBzIGYiO3M6MTY6ImV4cG9ydF9kZWxpbWl0ZXIiO3M6MToiLCI7czoyMjoiZGVmYXVsdF9leHBvcnRfY2hhcnNldCI7czo1OiJVVEYtOCI7czoxNDoidXNlX3JlYWxfbmFtZXMiO3M6Mjoib24iO3M6MTc6Im1haWxfc210cGF1dGhfcmVxIjtzOjA6IiI7czoxMjoibWFpbF9zbXRwc3NsIjtpOjA7czoxNToiZW1haWxfbGlua190eXBlIjtzOjU6InN1Z2FyIjtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6MjA6ImNhbGVuZGFyX3B1Ymxpc2hfa2V5IjtzOjA6IiI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VnYXI1IjtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjQ6IlRvZG8iO3M6MTA6IkVtcGxveWVlc1EiO2E6Mzp7czo2OiJtb2R1bGUiO3M6OToiRW1wbG95ZWVzIjtzOjY6ImFjdGlvbiI7czo1OiJpbmRleCI7czo1OiJxdWVyeSI7czo0OiJ0cnVlIjt9czoxMjoidXNlclByaXZHdWlkIjtzOjM2OiI0MDI3MmE5Yi1kNjI3LWRkNDMtNjlkOC01NmNmNWE2ZTUzYzkiO30='),
('9c6fbae0-e3fb-d098-d7f8-56cf50942c42', 'Bugs2_BUG', 0, '2016-02-25 19:05:32', '2016-02-25 19:05:32', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('9e71e815-12ea-d70a-4acd-56cee95267c6', 'ETag', 0, '2016-02-25 11:48:15', '2016-02-25 12:18:08', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6Njt9'),
('a1e25c55-8ffc-9278-274e-56cf54527258', 'Home', 0, '2016-02-25 19:22:49', '2016-02-26 13:15:36', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjM6e3M6MzY6IjgzOWJlZGM1LWRkZjctM2Y0Zi0zYzI1LTU2Y2Y1NGQ2YTAzYSI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjgzOWJlZmFlLTEzNGQtNjFiNS0xNjA3LTU2Y2Y1NGFmNTFlZSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxMzoiaUZyYW1lRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NDoiSG9tZSI7czoxMToiZm9yY2VDb2x1bW4iO2k6MTtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NTM6Im1vZHVsZXMvSG9tZS9EYXNobGV0cy9pRnJhbWVEYXNobGV0L2lGcmFtZURhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6Mzp7czoxMDoidGl0bGVMYWJlbCI7czoyMjoiTEJMX0RBU0hMRVRfU1VHQVJfTkVXUyI7czozOiJ1cmwiO3M6NDE6Imh0dHBzOi8vd3d3LnN1Z2FyY3JtLmNvbS9jcm0vcHJvZHVjdC9uZXdzIjtzOjY6ImhlaWdodCI7aTozMTU7fX1zOjM2OiI4NDU3NjI2ZS1hZjZjLWM5ZWEtNjJhZC01NmNmNTQwNTkxYmUiO2E6NTp7czo5OiJjbGFzc05hbWUiO3M6MTQ6Ik15Q2FsbHNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo1OiJDYWxscyI7czoxMToiZm9yY2VDb2x1bW4iO2k6MDtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NTY6Im1vZHVsZXMvQ2FsbHMvRGFzaGxldHMvTXlDYWxsc0Rhc2hsZXQvTXlDYWxsc0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX19czo1OiJwYWdlcyI7YToxOntpOjA7YTozOntzOjc6ImNvbHVtbnMiO2E6Mjp7aTowO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI2MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aToxO3M6MzY6Ijg0NTc2MjZlLWFmNmMtYzllYS02MmFkLTU2Y2Y1NDA1OTFiZSI7fX1pOjE7YToyOntzOjU6IndpZHRoIjtzOjM6IjQwJSI7czo4OiJkYXNobGV0cyI7YToyOntpOjA7czozNjoiODM5YmVkYzUtZGRmNy0zZjRmLTNjMjUtNTZjZjU0ZDZhMDNhIjtpOjE7czozNjoiODM5YmVmYWUtMTM0ZC02MWI1LTE2MDctNTZjZjU0YWY1MWVlIjt9fX1zOjEwOiJudW1Db2x1bW5zIjtzOjE6IjIiO3M6MTQ6InBhZ2VUaXRsZUxhYmVsIjtzOjIwOiJMQkxfSE9NRV9QQUdFXzFfTkFNRSI7fX19'),
('a2dc5919-e15c-f176-d9d6-56cf54471566', 'Home2_CALL', 0, '2016-02-25 19:22:49', '2016-02-25 19:22:49', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('a3595cda-a99f-79b7-bbcc-56cf542f8424', 'Home2_SUGARFEED', 0, '2016-02-25 19:22:49', '2016-02-25 19:22:49', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('a50ce8b6-7ffc-56bf-f173-56d045663813', 'Accounts2_ACCOUNT', 0, '2016-02-26 12:32:01', '2016-02-26 12:32:01', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('a7bf1023-59af-4cca-3c56-56d032c2dff1', 'ProspectLists2_PROSPECTLIST', 0, '2016-02-26 11:07:49', '2016-02-26 11:07:49', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('b3ba4ad5-8035-75e4-86dc-56cf54d51295', 'global', 0, '2016-02-25 19:21:58', '2016-02-25 19:22:48', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YTozNDp7czoxMjoibWFpbG1lcmdlX29uIjtzOjI6Im9uIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxNDoibW9kdWxlX2Zhdmljb24iO3M6MDoiIjtzOjk6ImhpZGVfdGFicyI7YTowOnt9czoxMToicmVtb3ZlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6MTgwMDtzOjE5OiJlbWFpbF9yZW1pbmRlcl90aW1lIjtpOi0xO3M6ODoidGltZXpvbmUiO3M6MTM6IkV1cm9wZS9CZXJsaW4iO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjQ6ImZkb3ciO3M6MToiMCI7czo1OiJkYXRlZiI7czo1OiJkL20vWSI7czo1OiJ0aW1lZiI7czozOiJILmkiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjY6ImwsIHMgZiI7czoxNjoiZXhwb3J0X2RlbGltaXRlciI7czoxOiIsIjtzOjIyOiJkZWZhdWx0X2V4cG9ydF9jaGFyc2V0IjtzOjU6IlVURi04IjtzOjE0OiJ1c2VfcmVhbF9uYW1lcyI7czoyOiJvbiI7czoxNzoibWFpbF9zbXRwYXV0aF9yZXEiO3M6MDoiIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MDtzOjE1OiJlbWFpbF9saW5rX3R5cGUiO3M6NToic3VnYXIiO3M6MTc6ImVtYWlsX3Nob3dfY291bnRzIjtpOjA7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6ImIzN2JjODU4LTUzNTctYjJjYS0xOGU0LTU2Y2Y1NDY1NThhZCI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VnYXI1IjtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjQ6IlRvZG8iO30='),
('b46fcd31-fc0f-efcc-e749-56cee0be05f7', 'ACLRoles2_ACLROLE', 0, '2016-02-25 11:07:32', '2016-02-25 11:07:32', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('b5a1b6e5-4ff2-b003-1fb6-56cee2653f51', 'global', 0, '2016-02-25 11:16:31', '2016-02-25 12:19:51', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YTozNzp7czoxMjoibWFpbG1lcmdlX29uIjtzOjI6Im9uIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjE5OiJuYXZpZ2F0aW9uX3BhcmFkaWdtIjtzOjI6ImdtIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoxNDoibW9kdWxlX2Zhdmljb24iO3M6MDoiIjtzOjk6ImhpZGVfdGFicyI7YTo5OntpOjA7czo4OiJBY2NvdW50cyI7aToxO3M6ODoiQ29udGFjdHMiO2k6MjtzOjEzOiJPcHBvcnR1bml0aWVzIjtpOjM7czo1OiJMZWFkcyI7aTo0O3M6ODoiQ2FsZW5kYXIiO2k6NTtzOjk6IkRvY3VtZW50cyI7aTo2O3M6NjoiRW1haWxzIjtpOjc7czo1OiJOb3RlcyI7aTo4O3M6NToiQ2FzZXMiO31zOjExOiJyZW1vdmVfdGFicyI7YTowOnt9czo3OiJub19vcHBzIjtzOjM6Im9mZiI7czoxMzoicmVtaW5kZXJfdGltZSI7czo0OiIxODAwIjtzOjE5OiJlbWFpbF9yZW1pbmRlcl90aW1lIjtpOi0xO3M6ODoidGltZXpvbmUiO3M6MTM6IkV1cm9wZS9NYWRyaWQiO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjQ6ImZkb3ciO3M6MToiMCI7czo1OiJkYXRlZiI7czo1OiJkL20vWSI7czo1OiJ0aW1lZiI7czozOiJILmkiO3M6MTU6Im1haWxfc210cHNlcnZlciI7czoxNDoic210cC5nbWFpbC5jb20iO3M6MTM6Im1haWxfc210cHVzZXIiO3M6MDoiIjtzOjEzOiJtYWlsX3NtdHBwYXNzIjtzOjA6IiI7czoyNjoiZGVmYXVsdF9sb2NhbGVfbmFtZV9mb3JtYXQiO3M6NjoibCwgcyBmIjtzOjE2OiJleHBvcnRfZGVsaW1pdGVyIjtzOjE6IiwiO3M6MjI6ImRlZmF1bHRfZXhwb3J0X2NoYXJzZXQiO3M6NToiVVRGLTgiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czowOiIiO3M6MTI6Im1haWxfc210cHNzbCI7aTowO3M6MTU6ImVtYWlsX2xpbmtfdHlwZSI7czo1OiJzdWdhciI7czoxNzoiZW1haWxfc2hvd19jb3VudHMiO2k6MDtzOjIwOiJjYWxlbmRhcl9wdWJsaXNoX2tleSI7czozNjoiYjVhMWJiOTgtOWNlYi03ZmMwLTc5NzAtNTZjZWUyYzc2NGY3IjtzOjE1OiJsb2dpbmV4cGlyYXRpb24iO3M6MToiMCI7czo3OiJsb2Nrb3V0IjtzOjA6IiI7czoxMToibG9naW5mYWlsZWQiO3M6MToiMCI7czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWdhcjUiO3M6MTk6InRoZW1lX2N1cnJlbnRfZ3JvdXAiO3M6NDoiVG9kbyI7fQ=='),
('be5a654e-50dc-6454-9796-56cf582a88ea', 'Users2_USER', 0, '2016-02-25 19:41:21', '2016-02-25 19:41:21', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('c156cc01-7c99-187a-dd2a-56cf6593800a', 'Bugs2_BUG', 0, '2016-02-25 20:34:27', '2016-02-25 20:34:27', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('c480500f-629a-f9f2-36d8-56cf5fcfe9b6', 'Leads2_LEAD', 0, '2016-02-25 20:10:50', '2016-02-25 20:10:50', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('ca6a154d-4246-5b1c-510c-56cef09d19a2', 'Campaigns2_CAMPAIGN', 0, '2016-02-25 12:14:46', '2016-02-25 12:14:46', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('cb09ed9a-9f87-ee76-2d8f-56cef1a410e5', 'Tasks2_TASK', 0, '2016-02-25 12:18:59', '2016-02-25 12:18:59', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('cda217be-c772-4502-17d4-56cf5d8c970b', 'Campaigns2_CAMPAIGN', 0, '2016-02-25 20:02:30', '2016-02-25 20:02:30', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('cdc14a41-7687-327d-8a31-56d0451b2adc', 'Opportunities2_OPPORTUNITY', 0, '2016-02-26 12:32:31', '2016-02-26 12:32:31', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('d9906473-b06d-0b9a-e663-56cef1591d2c', 'Meetings2_MEETING', 0, '2016-02-25 12:18:52', '2016-02-25 12:18:52', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('dd44007e-3bf0-76dd-3df8-56d0426ba3fc', 'Leads2_LEAD', 0, '2016-02-26 12:19:16', '2016-02-26 12:19:16', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('e4629ebd-7255-3a3a-b4bf-56d050df7d74', 'Bugs2_BUG', 0, '2016-02-26 13:15:54', '2016-02-26 13:15:54', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('e9afa74e-cc2e-9e36-f63f-56cf46e9339f', 'Home', 0, '2016-02-25 18:22:55', '2016-02-26 13:09:38', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjg6e3M6MzY6ImNhMzBhNGM1LTY4YWUtNjU3YS1kYTE2LTU2Y2Y0NmRiZDY3ZCI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6ImNhZWMyOGY5LWFkYmYtZTI0OS04OWFmLTU2Y2Y0NjRiZjVlNSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6ImNiZTYzMGFiLTQ2MTItM2I5OS03ODU1LTU2Y2Y0NmQwNjY0YyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6ImNjYTFiYjFlLWQ1ZTAtYTNkNC0yNTdlLTU2Y2Y0NjBjOTI4OCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiY2Q1ZDNlMGEtZjljYi1jZjJkLTJjYzEtNTZjZjQ2ZTNhMWMxIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiY2U1NzM5ZGMtZDY4MS0xMjk3LWU3NzEtNTZjZjQ2ZTYwODY3IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMTY3YjBlN2YtYTU5OS0xMTc1LTgwYTMtNTZjZjVhZTBkZjI2IjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjQxOiJPcHBvcnR1bml0aWVzQnlMZWFkU291cmNlQnlPdXRjb21lRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6Nzoib3B0aW9ucyI7YTo0OntzOjE3OiJsc2JvX2xlYWRfc291cmNlcyI7YToxOntpOjA7czoxNzoiRXhpc3RpbmcgQ3VzdG9tZXIiO31zOjg6ImxzYm9faWRzIjthOjE6e2k6MDtzOjM2OiI0MDljMTE3OC1hMTU5LTU5MjYtNTY2YS01NmNmNGZlNGY0NTAiO31zOjU6InRpdGxlIjtzOjQzOiJBbGwgT3Bwb3J0dW5pdGllcyBCeSBMZWFkIFNvdXJjZSBCeSBPdXRjb21lIjtzOjExOiJhdXRvUmVmcmVzaCI7czoyOiItMSI7fXM6MTI6ImZpbGVMb2NhdGlvbiI7czoxMTE6Im1vZHVsZXMvQ2hhcnRzL0Rhc2hsZXRzL09wcG9ydHVuaXRpZXNCeUxlYWRTb3VyY2VCeU91dGNvbWVEYXNobGV0L09wcG9ydHVuaXRpZXNCeUxlYWRTb3VyY2VCeU91dGNvbWVEYXNobGV0LnBocCI7fXM6MzY6IjkzMWRlNzZjLTdiYzUtOGNiMi01NGUwLTU2Y2Y1YTZiZjVlOCI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoyMzoiQ2FtcGFpZ25ST0lDaGFydERhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjk6IkNhbXBhaWducyI7czo3OiJvcHRpb25zIjthOjM6e3M6MTE6ImNhbXBhaWduX2lkIjthOjE6e2k6MDtzOjM2OiJkZjc4MTQzMC02ZDdlLTI3OTQtNTc2NC01NmQwMzgwYzYzMzgiO31zOjU6InRpdGxlIjtzOjEyOiJDYW1wYWlnbiBST0kiO3M6MTE6ImF1dG9SZWZyZXNoIjtzOjI6IjMwIjt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjc1OiJtb2R1bGVzL0NoYXJ0cy9EYXNobGV0cy9DYW1wYWlnblJPSUNoYXJ0RGFzaGxldC9DYW1wYWlnblJPSUNoYXJ0RGFzaGxldC5waHAiO319czo1OiJwYWdlcyI7YToxOntpOjA7YTozOntzOjc6ImNvbHVtbnMiO2E6Mjp7aTowO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI2MCUiO3M6ODoiZGFzaGxldHMiO2E6Nzp7aTowO3M6MzY6IjkzMWRlNzZjLTdiYzUtOGNiMi01NGUwLTU2Y2Y1YTZiZjVlOCI7aToyO3M6MzY6IjE2N2IwZTdmLWE1OTktMTE3NS04MGEzLTU2Y2Y1YWUwZGYyNiI7aTo0O3M6MzY6ImNhZWMyOGY5LWFkYmYtZTI0OS04OWFmLTU2Y2Y0NjRiZjVlNSI7aTo1O3M6MzY6ImNiZTYzMGFiLTQ2MTItM2I5OS03ODU1LTU2Y2Y0NmQwNjY0YyI7aTo2O3M6MzY6ImNjYTFiYjFlLWQ1ZTAtYTNkNC0yNTdlLTU2Y2Y0NjBjOTI4OCI7aTo3O3M6MzY6ImNkNWQzZTBhLWY5Y2ItY2YyZC0yY2MxLTU2Y2Y0NmUzYTFjMSI7aTo4O3M6MzY6ImNlNTczOWRjLWQ2ODEtMTI5Ny1lNzcxLTU2Y2Y0NmU2MDg2NyI7fX1pOjE7YToyOntzOjU6IndpZHRoIjtzOjM6IjQwJSI7czo4OiJkYXNobGV0cyI7YToxOntpOjA7czozNjoiY2EzMGE0YzUtNjhhZS02NTdhLWRhMTYtNTZjZjQ2ZGJkNjdkIjt9fX1zOjEwOiJudW1Db2x1bW5zIjtzOjE6IjIiO3M6MTQ6InBhZ2VUaXRsZUxhYmVsIjtzOjIwOiJMQkxfSE9NRV9QQUdFXzFfTkFNRSI7fX19'),
('eae82d84-e054-8ebf-1f0d-56cf4661554d', 'Home2_CALL', 0, '2016-02-25 18:22:55', '2016-02-25 18:22:55', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('eb26a0e1-92f6-95c2-7171-56cf46830046', 'Home2_MEETING', 0, '2016-02-25 18:22:55', '2016-02-25 18:22:55', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('eba3af64-0620-0f93-db8c-56cf46f37062', 'Home2_OPPORTUNITY', 0, '2016-02-25 18:22:55', '2016-02-25 18:22:55', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('ebe22035-9a1e-1215-c5c8-56cf46da139c', 'Home2_ACCOUNT', 0, '2016-02-25 18:22:55', '2016-02-25 18:22:55', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('ec5f200f-f1e5-c060-fe54-56cf4685dfd5', 'Home2_LEAD', 0, '2016-02-25 18:22:55', '2016-02-25 18:22:55', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('ecdc2401-a283-3743-fc18-56cf46dd54d5', 'Home2_SUGARFEED', 0, '2016-02-25 18:22:55', '2016-02-25 18:22:55', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('ee6417ae-b697-c11f-a1e9-56d04e057c09', 'Meetings2_MEETING', 0, '2016-02-26 13:10:04', '2016-02-26 13:10:04', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('eefd63cc-51f9-c1b4-9541-56cef13d6e98', 'ProspectLists2_PROSPECTLIST', 0, '2016-02-25 12:19:01', '2016-02-25 12:19:01', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('f0929512-ef74-be45-5785-56cef0b9d31a', 'Accounts2_ACCOUNT', 0, '2016-02-25 12:14:40', '2016-02-25 12:14:40', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('f0963f77-526d-0f45-4f2d-56cf5d042bf9', 'Opportunities2_OPPORTUNITY', 0, '2016-02-25 19:59:38', '2016-02-25 19:59:38', '816c0648-ca93-1d95-11f9-56cf45208e84', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ==');
INSERT INTO `user_preferences` (`id`, `category`, `deleted`, `date_entered`, `date_modified`, `assigned_user_id`, `contents`) VALUES
('f23954ab-c6c8-a926-5c17-56d045b9afd8', 'Home', 0, '2016-02-26 12:31:45', '2016-02-26 12:58:47', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjU6e3M6MzY6ImNmMTBkNGY3LTMxYTYtNDg3MS0xMjhhLTU2ZDA0NTljMTdjMSI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6ImNmY2M1OGI0LWQ4ZWMtZWU4Zi02OTYyLTU2ZDA0NTU3ODQxNSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6ImQwODdlMTIwLTM2MzAtYWY4ZS01MjE3LTU2ZDA0NTY3ZWQyMSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiZDE4MWU1ZjMtYzkxOS05Y2NlLTIwZTMtNTZkMDQ1MGEwYjE1IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiZDIzZDZjM2MtZjUxZC01ZGY0LWZmMGUtNTZkMDQ1MTYzMDkxIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjQ6e2k6MTtzOjM2OiJjZmNjNThiNC1kOGVjLWVlOGYtNjk2Mi01NmQwNDU1Nzg0MTUiO2k6MjtzOjM2OiJkMDg3ZTEyMC0zNjMwLWFmOGUtNTIxNy01NmQwNDU2N2VkMjEiO2k6MztzOjM2OiJkMTgxZTVmMy1jOTE5LTljY2UtMjBlMy01NmQwNDUwYTBiMTUiO2k6NDtzOjM2OiJkMjNkNmMzYy1mNTFkLTVkZjQtZmYwZS01NmQwNDUxNjMwOTEiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6ImNmMTBkNGY3LTMxYTYtNDg3MS0xMjhhLTU2ZDA0NTljMTdjMSI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIyIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),
('f2f4eafb-dbe0-ff96-c3ce-56d045b643cb', 'Home2_MEETING', 0, '2016-02-26 12:31:45', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('f371eda3-26c5-43d7-edb4-56d04595e372', 'Home2_OPPORTUNITY', 0, '2016-02-26 12:31:45', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('f3b06a8d-678f-5ff3-ea67-56d0455166dc', 'Home2_ACCOUNT', 0, '2016-02-26 12:31:45', '2016-02-26 12:31:45', '409c1178-a159-5926-566a-56cf4fe4f450', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('f3d16ac8-112b-a8b4-8bb4-56cef1d26677', 'Prospects2_PROSPECT', 0, '2016-02-25 12:19:00', '2016-02-25 12:19:00', 'b2752bed-800e-8997-c965-56cee29a3cdb', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vcals`
--

CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vcals`
--

INSERT INTO `vcals` (`id`, `deleted`, `date_entered`, `date_modified`, `user_id`, `type`, `source`, `content`) VALUES
('317152c0-6894-7fc3-7108-56cf5f4f02d7', 0, '2016-02-25 20:09:52', '2016-02-26 13:12:02', '816c0648-ca93-1d95-11f9-56cf45208e84', 'vfb', 'sugar', 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//SugarCRM//SugarCRM Calendar//EN\r\nBEGIN:VFREEBUSY\r\nORGANIZER;CN=Albarracín,  Teresa:VFREEBUSY\r\nDTSTART:2016-02-24 23:00:00\r\nDTEND:2016-04-24 22:00:00\r\nFREEBUSY:20160303T070000Z/20160303T091500Z\r\nFREEBUSY:20160226T080000Z/20160226T103000Z\r\nFREEBUSY:20160304T203000Z/20160304T204500Z\r\nDTSTAMP:2016-02-26 13:12:02\r\nEND:VFREEBUSY\r\nEND:VCALENDAR\r\n'),
('4198ced2-82dd-eedc-3dd3-56d04f43b981', 0, '2016-02-26 13:15:41', '2016-02-26 13:15:41', 'b08db30f-6a25-a86a-3b1e-56cf54cc213b', 'vfb', 'sugar', 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//SugarCRM//SugarCRM Calendar//EN\r\nBEGIN:VFREEBUSY\r\nORGANIZER;CN=Gallego,  Salvador:VFREEBUSY\r\nDTSTART:2016-02-24 23:00:00\r\nDTEND:2016-04-24 22:00:00\r\nFREEBUSY:20160304T203000Z/20160304T204500Z\r\nDTSTAMP:2016-02-26 13:15:41\r\nEND:VFREEBUSY\r\nEND:VCALENDAR\r\n'),
('e8860542-6a6c-9716-db6b-56d04ffb074d', 0, '2016-02-26 13:14:45', '2016-02-26 13:14:45', '6c4ed950-7e8f-f352-3ebc-56cef1afa4eb', 'vfb', 'sugar', 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//SugarCRM//SugarCRM Calendar//EN\r\nBEGIN:VFREEBUSY\r\nORGANIZER;CN=Díaz,  Jose:VFREEBUSY\r\nDTSTART:2016-02-24 23:00:00\r\nDTEND:2016-04-24 22:00:00\r\nDTSTAMP:2016-02-26 13:14:45\r\nEND:VFREEBUSY\r\nEND:VCALENDAR\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `versions`
--

CREATE TABLE `versions` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_version` varchar(255) DEFAULT NULL,
  `db_version` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `versions`
--

INSERT INTO `versions` (`id`, `deleted`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `file_version`, `db_version`) VALUES
('5a6d9c4b-5de3-c154-213e-56cdeccc6aed', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'Chart Data Cache', '3.5.1', '3.5.1'),
('5a6d9d56-e2da-c5b7-544a-56cdec5bff15', 0, '2016-02-24 17:46:46', '2016-02-24 17:46:46', '1', '1', 'htaccess', '3.5.1', '3.5.1'),
('73b7c588-1c4d-6821-dddf-56d051c7cad4', 0, '2016-02-26 13:24:13', '2016-02-26 13:24:13', '1', '1', 'Rebuild Relationships', '4.0.0', '4.0.0'),
('7a8de65c-4458-ad3b-dabf-56d05175e368', 0, '2016-02-26 13:24:13', '2016-02-26 13:24:13', '1', '1', 'Rebuild Extensions', '4.0.0', '4.0.0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_accnt_id_del` (`id`,`deleted`),
  ADD KEY `idx_accnt_name_del` (`name`,`deleted`),
  ADD KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  ADD KEY `idx_accnt_parent_id` (`parent_id`);

--
-- Indices de la tabla `accounts_audit`
--
ALTER TABLE `accounts_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_accounts_parent_id` (`parent_id`);

--
-- Indices de la tabla `accounts_bugs`
--
ALTER TABLE `accounts_bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_acc_bug_acc` (`account_id`),
  ADD KEY `idx_acc_bug_bug` (`bug_id`),
  ADD KEY `idx_account_bug` (`account_id`,`bug_id`);

--
-- Indices de la tabla `accounts_cases`
--
ALTER TABLE `accounts_cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_acc_case_acc` (`account_id`),
  ADD KEY `idx_acc_acc_case` (`case_id`);

--
-- Indices de la tabla `accounts_contacts`
--
ALTER TABLE `accounts_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account_contact` (`account_id`,`contact_id`),
  ADD KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`);

--
-- Indices de la tabla `accounts_cstm`
--
ALTER TABLE `accounts_cstm`
  ADD PRIMARY KEY (`id_c`);

--
-- Indices de la tabla `accounts_opportunities`
--
ALTER TABLE `accounts_opportunities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  ADD KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`);

--
-- Indices de la tabla `acl_actions`
--
ALTER TABLE `acl_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_aclaction_id_del` (`id`,`deleted`),
  ADD KEY `idx_category_name` (`category`,`name`);

--
-- Indices de la tabla `acl_roles`
--
ALTER TABLE `acl_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_aclrole_id_del` (`id`,`deleted`);

--
-- Indices de la tabla `acl_roles_actions`
--
ALTER TABLE `acl_roles_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_acl_role_id` (`role_id`),
  ADD KEY `idx_acl_action_id` (`action_id`),
  ADD KEY `idx_aclrole_action` (`role_id`,`action_id`);

--
-- Indices de la tabla `acl_roles_users`
--
ALTER TABLE `acl_roles_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_aclrole_id` (`role_id`),
  ADD KEY `idx_acluser_id` (`user_id`),
  ADD KEY `idx_aclrole_user` (`role_id`,`user_id`);

--
-- Indices de la tabla `address_book`
--
ALTER TABLE `address_book`
  ADD KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`);

--
-- Indices de la tabla `bugs`
--
ALTER TABLE `bugs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bugsnumk` (`bug_number`),
  ADD KEY `bug_number` (`bug_number`),
  ADD KEY `idx_bug_name` (`name`),
  ADD KEY `idx_bugs_assigned_user` (`assigned_user_id`);

--
-- Indices de la tabla `bugs_audit`
--
ALTER TABLE `bugs_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_bugs_parent_id` (`parent_id`);

--
-- Indices de la tabla `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_call_name` (`name`),
  ADD KEY `idx_status` (`status`),
  ADD KEY `idx_calls_date_start` (`date_start`),
  ADD KEY `idx_calls_par_del` (`parent_id`,`parent_type`,`deleted`),
  ADD KEY `idx_calls_assigned_del` (`deleted`,`assigned_user_id`);

--
-- Indices de la tabla `calls_contacts`
--
ALTER TABLE `calls_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_con_call_call` (`call_id`),
  ADD KEY `idx_con_call_con` (`contact_id`),
  ADD KEY `idx_call_contact` (`call_id`,`contact_id`);

--
-- Indices de la tabla `calls_leads`
--
ALTER TABLE `calls_leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_lead_call_call` (`call_id`),
  ADD KEY `idx_lead_call_lead` (`lead_id`),
  ADD KEY `idx_call_lead` (`call_id`,`lead_id`);

--
-- Indices de la tabla `calls_users`
--
ALTER TABLE `calls_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_usr_call_call` (`call_id`),
  ADD KEY `idx_usr_call_usr` (`user_id`),
  ADD KEY `idx_call_users` (`call_id`,`user_id`);

--
-- Indices de la tabla `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `camp_auto_tracker_key` (`tracker_key`),
  ADD KEY `idx_campaign_name` (`name`);

--
-- Indices de la tabla `campaigns_audit`
--
ALTER TABLE `campaigns_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_campaigns_parent_id` (`parent_id`);

--
-- Indices de la tabla `campaign_log`
--
ALTER TABLE `campaign_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_camp_tracker` (`target_tracker_key`),
  ADD KEY `idx_camp_campaign_id` (`campaign_id`),
  ADD KEY `idx_camp_more_info` (`more_information`),
  ADD KEY `idx_target_id` (`target_id`),
  ADD KEY `idx_target_id_deleted` (`target_id`,`deleted`);

--
-- Indices de la tabla `campaign_trkrs`
--
ALTER TABLE `campaign_trkrs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_tracker_key_idx` (`tracker_key`);

--
-- Indices de la tabla `cases`
--
ALTER TABLE `cases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `casesnumk` (`case_number`),
  ADD KEY `case_number` (`case_number`),
  ADD KEY `idx_case_name` (`name`),
  ADD KEY `idx_account_id` (`account_id`),
  ADD KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`);

--
-- Indices de la tabla `cases_audit`
--
ALTER TABLE `cases_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_cases_parent_id` (`parent_id`);

--
-- Indices de la tabla `cases_bugs`
--
ALTER TABLE `cases_bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_cas_bug_cas` (`case_id`),
  ADD KEY `idx_cas_bug_bug` (`bug_id`),
  ADD KEY `idx_case_bug` (`case_id`,`bug_id`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD KEY `idx_config_cat` (`category`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  ADD KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  ADD KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  ADD KEY `idx_reports_to_id` (`reports_to_id`),
  ADD KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  ADD KEY `idx_cont_assigned` (`assigned_user_id`);

--
-- Indices de la tabla `contacts_audit`
--
ALTER TABLE `contacts_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_contacts_parent_id` (`parent_id`);

--
-- Indices de la tabla `contacts_bugs`
--
ALTER TABLE `contacts_bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_con_bug_con` (`contact_id`),
  ADD KEY `idx_con_bug_bug` (`bug_id`),
  ADD KEY `idx_contact_bug` (`contact_id`,`bug_id`);

--
-- Indices de la tabla `contacts_cases`
--
ALTER TABLE `contacts_cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_con_case_con` (`contact_id`),
  ADD KEY `idx_con_case_case` (`case_id`),
  ADD KEY `idx_contacts_cases` (`contact_id`,`case_id`);

--
-- Indices de la tabla `contacts_users`
--
ALTER TABLE `contacts_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_con_users_con` (`contact_id`),
  ADD KEY `idx_con_users_user` (`user_id`),
  ADD KEY `idx_contacts_users` (`contact_id`,`user_id`);

--
-- Indices de la tabla `cron_remove_documents`
--
ALTER TABLE `cron_remove_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_cron_remove_document_bean_id` (`bean_id`),
  ADD KEY `idx_cron_remove_document_stamp` (`date_modified`);

--
-- Indices de la tabla `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_currency_name` (`name`,`deleted`);

--
-- Indices de la tabla `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD KEY `idx_beanid_set_num` (`bean_id`,`set_num`);

--
-- Indices de la tabla `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_doc_cat` (`category_id`,`subcategory_id`);

--
-- Indices de la tabla `documents_accounts`
--
ALTER TABLE `documents_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_accounts_account_id` (`account_id`,`document_id`),
  ADD KEY `documents_accounts_document_id` (`document_id`,`account_id`);

--
-- Indices de la tabla `documents_bugs`
--
ALTER TABLE `documents_bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_bugs_bug_id` (`bug_id`,`document_id`),
  ADD KEY `documents_bugs_document_id` (`document_id`,`bug_id`);

--
-- Indices de la tabla `documents_cases`
--
ALTER TABLE `documents_cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_cases_case_id` (`case_id`,`document_id`),
  ADD KEY `documents_cases_document_id` (`document_id`,`case_id`);

--
-- Indices de la tabla `documents_contacts`
--
ALTER TABLE `documents_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_contacts_contact_id` (`contact_id`,`document_id`),
  ADD KEY `documents_contacts_document_id` (`document_id`,`contact_id`);

--
-- Indices de la tabla `documents_opportunities`
--
ALTER TABLE `documents_opportunities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_docu_opps_oppo_id` (`opportunity_id`,`document_id`),
  ADD KEY `idx_docu_oppo_docu_id` (`document_id`,`opportunity_id`);

--
-- Indices de la tabla `document_revisions`
--
ALTER TABLE `document_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documentrevision_mimetype` (`file_mime_type`);

--
-- Indices de la tabla `eapm`
--
ALTER TABLE `eapm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_app_active` (`assigned_user_id`,`application`,`validated`);

--
-- Indices de la tabla `emailman`
--
ALTER TABLE `emailman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  ADD KEY `idx_eman_campaign_id` (`campaign_id`),
  ADD KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`);

--
-- Indices de la tabla `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_email_name` (`name`),
  ADD KEY `idx_message_id` (`message_id`),
  ADD KEY `idx_email_parent_id` (`parent_id`),
  ADD KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`);

--
-- Indices de la tabla `emails_beans`
--
ALTER TABLE `emails_beans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_emails_beans_bean_id` (`bean_id`),
  ADD KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`);

--
-- Indices de la tabla `emails_email_addr_rel`
--
ALTER TABLE `emails_email_addr_rel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  ADD KEY `idx_eearl_address_id` (`email_address_id`);

--
-- Indices de la tabla `emails_text`
--
ALTER TABLE `emails_text`
  ADD PRIMARY KEY (`email_id`),
  ADD KEY `emails_textfromaddr` (`from_addr`);

--
-- Indices de la tabla `email_addresses`
--
ALTER TABLE `email_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  ADD KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`);

--
-- Indices de la tabla `email_addr_bean_rel`
--
ALTER TABLE `email_addr_bean_rel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_email_address_id` (`email_address_id`),
  ADD KEY `idx_bean_id` (`bean_id`,`bean_module`);

--
-- Indices de la tabla `email_cache`
--
ALTER TABLE `email_cache`
  ADD KEY `idx_ie_id` (`ie_id`),
  ADD KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  ADD KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  ADD KEY `idx_mail_subj` (`subject`),
  ADD KEY `idx_mail_to` (`toaddr`);

--
-- Indices de la tabla `email_marketing`
--
ALTER TABLE `email_marketing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_emmkt_name` (`name`),
  ADD KEY `idx_emmkit_del` (`deleted`);

--
-- Indices de la tabla `email_marketing_prospect_lists`
--
ALTER TABLE `email_marketing_prospect_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`);

--
-- Indices de la tabla `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_email_template_name` (`name`);

--
-- Indices de la tabla `fields_meta_data`
--
ALTER TABLE `fields_meta_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_meta_id_del` (`id`,`deleted`),
  ADD KEY `idx_meta_cm_del` (`custom_module`,`deleted`);

--
-- Indices de la tabla `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_parent_folder` (`parent_folder`);

--
-- Indices de la tabla `folders_rel`
--
ALTER TABLE `folders_rel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  ADD KEY `idx_fr_id_deleted_poly` (`folder_id`,`deleted`,`polymorphic_id`);

--
-- Indices de la tabla `folders_subscriptions`
--
ALTER TABLE `folders_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`);

--
-- Indices de la tabla `import_maps`
--
ALTER TABLE `import_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`);

--
-- Indices de la tabla `inbound_email`
--
ALTER TABLE `inbound_email`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inbound_email_autoreply`
--
ALTER TABLE `inbound_email_autoreply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_ie_autoreplied_to` (`autoreplied_to`);

--
-- Indices de la tabla `inbound_email_cache_ts`
--
ALTER TABLE `inbound_email_cache_ts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `job_queue`
--
ALTER TABLE `job_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_status_scheduler` (`status`,`scheduler_id`),
  ADD KEY `idx_status_time` (`status`,`execute_time`,`date_entered`),
  ADD KEY `idx_status_entered` (`status`,`date_entered`),
  ADD KEY `idx_status_modified` (`status`,`date_modified`);

--
-- Indices de la tabla `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  ADD KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  ADD KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  ADD KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  ADD KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  ADD KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  ADD KEY `idx_lead_assigned` (`assigned_user_id`),
  ADD KEY `idx_lead_contact` (`contact_id`),
  ADD KEY `idx_reports_to` (`reports_to_id`),
  ADD KEY `idx_lead_phone_work` (`phone_work`),
  ADD KEY `idx_leads_id_del` (`id`,`deleted`);

--
-- Indices de la tabla `leads_audit`
--
ALTER TABLE `leads_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_leads_parent_id` (`parent_id`);

--
-- Indices de la tabla `linked_documents`
--
ALTER TABLE `linked_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`);

--
-- Indices de la tabla `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_mtg_name` (`name`),
  ADD KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  ADD KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`),
  ADD KEY `idx_meet_date_start` (`date_start`);

--
-- Indices de la tabla `meetings_contacts`
--
ALTER TABLE `meetings_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_con_mtg_mtg` (`meeting_id`),
  ADD KEY `idx_con_mtg_con` (`contact_id`),
  ADD KEY `idx_meeting_contact` (`meeting_id`,`contact_id`);

--
-- Indices de la tabla `meetings_leads`
--
ALTER TABLE `meetings_leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_lead_meeting_meeting` (`meeting_id`),
  ADD KEY `idx_lead_meeting_lead` (`lead_id`),
  ADD KEY `idx_meeting_lead` (`meeting_id`,`lead_id`);

--
-- Indices de la tabla `meetings_users`
--
ALTER TABLE `meetings_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_usr_mtg_mtg` (`meeting_id`),
  ADD KEY `idx_usr_mtg_usr` (`user_id`),
  ADD KEY `idx_meeting_users` (`meeting_id`,`user_id`);

--
-- Indices de la tabla `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_note_name` (`name`),
  ADD KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  ADD KEY `idx_note_contact` (`contact_id`),
  ADD KEY `idx_notes_assigned_del` (`deleted`,`assigned_user_id`);

--
-- Indices de la tabla `oauth_consumer`
--
ALTER TABLE `oauth_consumer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ckey` (`c_key`);

--
-- Indices de la tabla `oauth_nonce`
--
ALTER TABLE `oauth_nonce`
  ADD PRIMARY KEY (`conskey`,`nonce`),
  ADD KEY `oauth_nonce_keyts` (`conskey`,`nonce_ts`);

--
-- Indices de la tabla `oauth_tokens`
--
ALTER TABLE `oauth_tokens`
  ADD PRIMARY KEY (`id`,`deleted`),
  ADD KEY `oauth_state_ts` (`tstate`,`token_ts`),
  ADD KEY `constoken_key` (`consumer`);

--
-- Indices de la tabla `opportunities`
--
ALTER TABLE `opportunities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_opp_name` (`name`),
  ADD KEY `idx_opp_assigned` (`assigned_user_id`),
  ADD KEY `idx_opp_id_deleted` (`id`,`deleted`);

--
-- Indices de la tabla `opportunities_audit`
--
ALTER TABLE `opportunities_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_opportunities_parent_id` (`parent_id`);

--
-- Indices de la tabla `opportunities_contacts`
--
ALTER TABLE `opportunities_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_con_opp_con` (`contact_id`),
  ADD KEY `idx_con_opp_opp` (`opportunity_id`),
  ADD KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`);

--
-- Indices de la tabla `outbound_email`
--
ALTER TABLE `outbound_email`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oe_user_id_idx` (`id`,`user_id`);

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `projects_accounts`
--
ALTER TABLE `projects_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_proj_acct_proj` (`project_id`),
  ADD KEY `idx_proj_acct_acct` (`account_id`),
  ADD KEY `projects_accounts_alt` (`project_id`,`account_id`);

--
-- Indices de la tabla `projects_bugs`
--
ALTER TABLE `projects_bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_proj_bug_proj` (`project_id`),
  ADD KEY `idx_proj_bug_bug` (`bug_id`),
  ADD KEY `projects_bugs_alt` (`project_id`,`bug_id`);

--
-- Indices de la tabla `projects_cases`
--
ALTER TABLE `projects_cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_proj_case_proj` (`project_id`),
  ADD KEY `idx_proj_case_case` (`case_id`),
  ADD KEY `projects_cases_alt` (`project_id`,`case_id`);

--
-- Indices de la tabla `projects_contacts`
--
ALTER TABLE `projects_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_proj_con_proj` (`project_id`),
  ADD KEY `idx_proj_con_con` (`contact_id`),
  ADD KEY `projects_contacts_alt` (`project_id`,`contact_id`);

--
-- Indices de la tabla `projects_opportunities`
--
ALTER TABLE `projects_opportunities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_proj_opp_proj` (`project_id`),
  ADD KEY `idx_proj_opp_opp` (`opportunity_id`),
  ADD KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`);

--
-- Indices de la tabla `projects_products`
--
ALTER TABLE `projects_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_proj_prod_project` (`project_id`),
  ADD KEY `idx_proj_prod_product` (`product_id`),
  ADD KEY `projects_products_alt` (`project_id`,`product_id`);

--
-- Indices de la tabla `project_task`
--
ALTER TABLE `project_task`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `project_task_audit`
--
ALTER TABLE `project_task_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_project_task_parent_id` (`parent_id`);

--
-- Indices de la tabla `prospects`
--
ALTER TABLE `prospects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prospect_auto_tracker_key` (`tracker_key`),
  ADD KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  ADD KEY `idx_prospecs_del_last` (`last_name`,`deleted`),
  ADD KEY `idx_prospects_id_del` (`id`,`deleted`),
  ADD KEY `idx_prospects_assigned` (`assigned_user_id`);

--
-- Indices de la tabla `prospect_lists`
--
ALTER TABLE `prospect_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_prospect_list_name` (`name`);

--
-- Indices de la tabla `prospect_lists_prospects`
--
ALTER TABLE `prospect_lists_prospects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_plp_pro_id` (`prospect_list_id`),
  ADD KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`);

--
-- Indices de la tabla `prospect_list_campaigns`
--
ALTER TABLE `prospect_list_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_pro_id` (`prospect_list_id`),
  ADD KEY `idx_cam_id` (`campaign_id`),
  ADD KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`);

--
-- Indices de la tabla `relationships`
--
ALTER TABLE `relationships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_rel_name` (`relationship_name`);

--
-- Indices de la tabla `releases`
--
ALTER TABLE `releases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_releases` (`name`,`deleted`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_role_id_del` (`id`,`deleted`);

--
-- Indices de la tabla `roles_modules`
--
ALTER TABLE `roles_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_role_id` (`role_id`),
  ADD KEY `idx_module_id` (`module_id`);

--
-- Indices de la tabla `roles_users`
--
ALTER TABLE `roles_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_ru_role_id` (`role_id`),
  ADD KEY `idx_ru_user_id` (`user_id`);

--
-- Indices de la tabla `saved_search`
--
ALTER TABLE `saved_search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_desc` (`name`,`deleted`);

--
-- Indices de la tabla `schedulers`
--
ALTER TABLE `schedulers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_schedule` (`date_time_start`,`deleted`);

--
-- Indices de la tabla `sugarfeed`
--
ALTER TABLE `sugarfeed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sgrfeed_date` (`date_entered`,`deleted`),
  ADD KEY `idx_sgrfeed_rmod_rid_date` (`related_module`,`related_id`,`date_entered`);

--
-- Indices de la tabla `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_tsk_name` (`name`),
  ADD KEY `idx_task_con_del` (`contact_id`,`deleted`),
  ADD KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  ADD KEY `idx_task_assigned` (`assigned_user_id`),
  ADD KEY `idx_task_status` (`status`);

--
-- Indices de la tabla `tracker`
--
ALTER TABLE `tracker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_tracker_iid` (`item_id`),
  ADD KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  ADD KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  ADD KEY `idx_tracker_monitor_id` (`monitor_id`),
  ADD KEY `idx_tracker_date_modified` (`date_modified`);

--
-- Indices de la tabla `upgrade_history`
--
ALTER TABLE `upgrade_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`);

--
-- Indices de la tabla `users_cstm`
--
ALTER TABLE `users_cstm`
  ADD PRIMARY KEY (`id_c`);

--
-- Indices de la tabla `users_feeds`
--
ALTER TABLE `users_feeds`
  ADD KEY `idx_ud_user_id` (`user_id`,`feed_id`);

--
-- Indices de la tabla `users_last_import`
--
ALTER TABLE `users_last_import`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`assigned_user_id`);

--
-- Indices de la tabla `users_password_link`
--
ALTER TABLE `users_password_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_username` (`username`);

--
-- Indices de la tabla `users_signatures`
--
ALTER TABLE `users_signatures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_usersig_uid` (`user_id`);

--
-- Indices de la tabla `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_userprefnamecat` (`assigned_user_id`,`category`);

--
-- Indices de la tabla `vcals`
--
ALTER TABLE `vcals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_vcal` (`type`,`user_id`);

--
-- Indices de la tabla `versions`
--
ALTER TABLE `versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_version` (`name`,`deleted`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bugs`
--
ALTER TABLE `bugs`
  MODIFY `bug_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `tracker_key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `campaign_trkrs`
--
ALTER TABLE `campaign_trkrs`
  MODIFY `tracker_key` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cases`
--
ALTER TABLE `cases`
  MODIFY `case_number` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `emailman`
--
ALTER TABLE `emailman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prospects`
--
ALTER TABLE `prospects`
  MODIFY `tracker_key` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tracker`
--
ALTER TABLE `tracker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
